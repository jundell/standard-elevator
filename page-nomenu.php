<?php 

// Template Name: No Menu Page

get_header(); ?>

<?php

$image = get_field('sub_header_image');

/*if( !empty($image) ):*/ ?>

<section class="header-image-new">

  <div class="wOuter">

    <div class="wInner">

      <h1><?php the_title(); ?></h1>

    </div>

  </div>

</section>



<section class="content">

  <div class="container">

    <div class="row">

      <div class="col-sm-12">

        <?php

          // Start the loop.

          while ( have_posts() ) : the_post();

          the_content();

          // End the loop.

          endwhile;

        ?>

      </div>

    </div>

  </div>

</section>

<?php get_footer(); ?>