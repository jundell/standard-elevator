<?php get_header(); ?>

<section class="header-image-new">
  <div class="wOuter">
    <div class="wInner">
      <h1>Search</h1>
    </div>
  </div>
</section>

<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <ul class="search-list">
            <?php if ( have_posts() ) : ?>

              <div class="search-page-header">
                <p><?php printf( __( 'Search Results for: %s', 'wpdocs_my_search_form' ), get_search_query() ); ?></p>
              </div>
              <?php
              // Start the Loop.
              while ( have_posts() ) : the_post();
              ?>
              <li>
                <h3><a href="<?php get_the_permalink() ?>"><?php the_title(); ?></a></h3>
                <?php the_post_thumbnail('medium') ?>
                <p><?php echo excerpt(200); ?></p>
                <div class="h-readmore"> 
                    <a href="<?php the_permalink(); ?>">Read More</a>
                </div>
                <?php
                endwhile;
                else : ?>
              </li>
            <p style="text-align: center;"><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentysixteen' ); ?></p>
            <?php //get_search_form(); ?>

            <?php endif; ?>      
        </ul>    
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
