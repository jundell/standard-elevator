<?php get_header(); ?>

<section class="header-image" style="background-image:url('http://torindrive.ripecustomdesign.com/wp-content/uploads/2016/11/hdr-img-wide-v1-icecondo.jpg')">
  <div class="wOuter">
    <div class="wInner">
      <h1>404</h1>
    </div>
  </div>
</section>

<section class="content text-center">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
   		<h2>Oops! That page can&rsquo;t be found.</h2>
      <p><?php esc_html_e( 'It looks like nothing was found at this location.', 'dfs' ); ?></p>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
