<?php  

/**
 * PHP Libraries
 */
require get_template_directory() . '/vendor/autoload.php';

/**
 * Register Modules
 */
require get_template_directory() . '/modules/loader.php';


function torindriveintl_scripts(){
	// bootstrap
	wp_enqueue_style('torindriveintl-bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '20161013');

	wp_enqueue_style('torindriveintl-fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), '20161013');
	// Owl Carousel
	wp_enqueue_style('torindriveintl-owlcarousel-style', get_template_directory_uri() . '/css/owl.carousel.css', array(), '20161013');
	// Owl Carousel - Theme
	wp_enqueue_style('torindriveintl-owlcarousel-theme-style', get_template_directory_uri() . '/css/owl.theme.default.min.css', array(), '20161013');
	// main css
	wp_enqueue_style('torindriveintl-main-css-style', get_template_directory_uri() . '/css/main.css', array(), '20161013');
	// breakpoints css
	wp_enqueue_style('torindriveintl-breakpoints-style', get_template_directory_uri() . '/css/breakpoints.css', array(), '20161013');
	// main style
	wp_enqueue_style('torindriveintl-main-style', get_stylesheet_uri() );
	
	// nice select
	wp_enqueue_style('torindriveintl-select-style', get_template_directory_uri() . '/css/nice-select.css', array(), '20161013');
	
	wp_enqueue_style('torindriveintl-dashboard', get_template_directory_uri().'/css/dashboard.css' );
	
	// main js
	wp_enqueue_script( 'torindriveintl-main-scripts', get_template_directory_uri() . '/js/jquery.min.js', array(), '20161013', true );
	// cookie js
	wp_enqueue_script( 'torindriveintl-cookie-scripts', get_template_directory_uri() . '/js/js.cookie.js', array(), '20161013', true );
	// fast-click js
	wp_enqueue_script( 'torindriveintl-fastclick-scripts', get_template_directory_uri() . '/js/fastclick.js', array(), '20161013', true );
	// bootstrap js
	wp_enqueue_script( 'torindriveintl-bootstrap-scripts', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '20161013', true );
	// owlcarousel js
	wp_enqueue_script( 'torindriveintl-owlcarousel-scripts', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '20161013', true );

	// matchheight js
	wp_enqueue_script( 'torindriveintl-matchheight-scripts', get_template_directory_uri() . '/js/jquery.matchHeight.js', array(), '20161013', true );
	
	wp_enqueue_script( 'torindriveintl-inputmask-scripts', get_template_directory_uri() . '/js/jquery.inputmask.bundle.min.js', array(), '20161013', true );
	// page js
	wp_enqueue_script( 'torindriveintl-page-scripts', get_template_directory_uri() . '/js/page.js', array(), '20161013', true );
	
	// select to ul li
	wp_enqueue_script( 'torindriveintl-select', get_template_directory_uri() . '/js/jquery.nice-select.js', array(), '20161013', true );
}
add_action( 'wp_enqueue_scripts', 'torindriveintl_scripts' );
function torindriveintl_theme_setup(){
	add_theme_support('menus');
	register_nav_menu('primary','primary navigation menu');
}
add_action('init','torindriveintl_theme_setup');
function torindriveintl_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Footer Copyright', 'torindriveintl' ),
		'id'            => 'copy',
		'description'   => __( 'copyright', 'torindriveintl' ),
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Address', 'torindriveintl' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'address', 'torindriveintl' ),
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Contact Info', 'torindriveintl' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'contact', 'torindriveintl' ),
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Email and Schedule', 'torindriveintl' ),
		'id'            => 'sidebar-4',
		'description'   => __( 'email', 'torindriveintl' ),
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Social Media', 'torindriveintl' ),
		'id'            => 'sidebar-5',
		'description'   => __( 'social media', 'torindriveintl' ),
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Our Brands', 'torindriveintl' ),
		'id'            => 'sidebar-6',
		'description'   => __( 'Our Brands', 'torindriveintl' ),
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
	) );
	register_sidebar( array(
		'name'          => __( 'Welcome Page', 'torindriveintl' ),
		'id'            => 'sidebar-7',
		'description'   => __( 'Welcome Page', 'torindriveintl' ),
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>'
	) );
}
add_action( 'widgets_init', 'torindriveintl_widgets_init' );
// custom search
function wpdocs_my_search_form( $form ) {
    $form =	'<form role="search" method="get" class="navbar-form" action="' . home_url( '/' ) . '">
				<div class="search-wrapper">
				 	<div class="form-group">
				    	<input type="text" class="form-control" placeholder="Search" value="' . get_search_query() . '" name="s">
				  	</div>
				  	<button type="submit" id="searchsubmit" class="btn btn-default" value="'. esc_attr__( 'Search' ) .'"><i class="fa fa-search"></i></button>
				</div>
			</form>';
    return $form;
}
add_filter( 'get_search_form', 'wpdocs_my_search_form' );
if ( function_exists( 'add_theme_support' ) ) { 
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 150, 150, true ); // default Post Thumbnail dimensions (cropped)
    // additional image sizes
    // delete the next line if you do not need additional image sizes
	add_image_size( 'category-thumb', 300, 9999 ); //300 pixels wide (and unlimited height)
	add_image_size('map-thumb', 150, 160); // map thumbnail
}
function new_excerpt_more( $more ) {
 return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}


add_action('admin_footer', 'account_status_change');
function account_status_change() {
	echo "<script type='text/javascript'>jQuery('#swpm-profile-page #account_status_change').attr('checked','checked');</script>";
}


function my_kses_post( $value ) {
	
	// is array
	if( is_array($value) ) {
	
		return array_map('my_kses_post', $value);
	
	}
	
	
	// return
	return wp_kses_post( $value );

}

add_filter('acf/update_value', 'my_kses_post', 10, 1);

function my_acf_save_post( $post_id ) {
		// bail early if no ACF data
		if( empty($_POST['acf']) ) {
			return;
		}
		
		// bail early if editing in admin
		if( is_admin() ) {
			return;
		}

		global $wpdb;

		$current_user 	= wp_get_current_user();
		$userID 		= $current_user->ID;
		$username 		= $current_user->user_login;
		
		$company_logo 			= @$_POST['acf']['field_5923f05996281'];

		$company_name 			= @$_POST['acf']['field_58ff07de2e77a'];
		$user_firstname 		= @$_POST['acf']['field_58ff07ee2e77b'];
		$user_lastname 			= @$_POST['acf']['field_58ff0bce2e77c'];
		$user_email 			= @$_POST['acf']['field_58ff0bde2e77d'];
		$contact_phone_number 	= @$_POST['acf']['field_58ff0beb2e77e'];

		$profile_ext    = @$_POST['acf']['field_59081f1d733c7'];
		$profile_title  = @$_POST['acf']['field_59081e9d733c6'];
		$cellphone      = @$_POST['acf']['field_59081fbf733c8'];
		$profile_fax    = @$_POST['acf']['field_593a6a0c19a70'];

		$street         = @$_POST['acf']['field_59081fe1733c9'];
		$city           = @$_POST['acf']['field_59082002733ca'];
		$state          = @$_POST['acf']['field_59082013733cb'];
		$zip            = @$_POST['acf']['field_5908201e733cc'];

		$contact_physical_address = @$_POST['acf']['field_58ff0c012e77f'];

		if ( $company_name && $user_firstname && $user_lastname && $user_email ) {
			update_user_meta($userID, 'company_name', $company_name);			

			// update_user_meta($userID, 'company_logo', $company_logo);
			
			update_user_meta($userID, 'first_name', $user_firstname);
			update_user_meta($userID, 'contact_name_firstname', $user_firstname);

			update_user_meta($userID, 'last_name', $user_lastname);
			update_user_meta($userID, 'contact_name_lastname', $user_lastname);

			update_user_meta($userID, 'profile_ext', $profile_ext);
			update_user_meta($userID, 'profile_title', $profile_title);
			update_user_meta($userID, 'profile_cellphone', $cellphone);
			update_user_meta($userID, 'profile_fax', $profile_fax);

			update_user_meta($userID, 'profile_street', $street);
			update_user_meta($userID, 'profile_city', $city);
			update_user_meta($userID, 'profile_state', $state);
			update_user_meta($userID, 'profile_zip', $zip);

			update_user_meta($userID, 'contact_email', $user_email);
			update_user_meta($userID, 'contact_phone_number', $contact_phone_number);

			$args = array(
				'ID'         => $userID,
				'user_email' => esc_attr( $user_email )
			);            
			wp_update_user( $args );
		}
		
		// return the ID
		return $post_id;
	}
	//add_action('acf/save_post', 'my_acf_save_post', 20);

function my_project_updated_send_email( $post_id, $post ) {

	// If this is just a revision, don't send the email.
	if ( wp_is_post_revision( $post_id ) )
		return;

	if ( get_post_status( $post_id ) != 'publish' )
		return;

	if ( get_post_type( $post_id ) != 'post' )
		return;

 	$emailed = get_post_meta($post_id, '_emailed', true);
 	if ( !$emailed ) {
		$post_title = get_the_title( $post_id );
		$post_url = get_permalink( $post_id );

		$headers[] = 'From: Torin <no-reply@torindriveintl.com>';
		$headers[] = 'Bcc: torindriveintlchecker@yopmail.com';
		$headers[] = 'Content-Type: text/html; charset=UTF-8';

		$subject = get_field('subject', 'option');

		$blogusers = get_users( 'blog_id=1&orderby=nicename&role=customer' );
		foreach ( $blogusers as $user ) {
			$message =  str_replace(array('[name]','[title]','[url]'), array($user->display_name, $post_title, $post_url), get_field('body', 'option'));
			wp_mail( $user->user_email, $subject, $message, $headers );
		}

		// Send email to admin.
  		update_post_meta($post_id, '_emailed', 'yes');
 	}
}
//add_action( 'save_post', 'my_project_updated_send_email', 10, 2 ); 
// COMMENTED HERE ---------------------------------------------------------------


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Options',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Manage Survey Questions',
		'menu_title'	=> 'Survey Form',
		'parent_slug'	=> 'theme-options',
	));
	
}

function custom_repeater_data( $repeater_name = null, $fields, $id = null, $option=null ) {
	$data = array();
	$counter = 1;

	if ( ! $repeater_name ) { return false; }

	if( have_rows( $repeater_name, $option ) ) {
		while ( have_rows( $repeater_name , $option) ) : the_row();
			if( $fields ) {
				foreach( $fields as $field_name ) {
					if( $id ) {
						$data[$counter][$field_name] = get_sub_field( $field_name, $id );
					}else {
						$data[$counter][$field_name] = get_sub_field( $field_name );
					}
				}
			}
		$counter++;
		endwhile;
	}
	return $data;
}

function subpagelist_func( $atts ){

	$args = array(
	    'post_type'      => 'page',
	    'posts_per_page' => -1,
	    'post_parent'    => $atts['postid'],
	    'order'          => 'ASC',
	    'orderby'        => 'menu_order'
	 );

	ob_start();

	$parent = new WP_Query( $args );
	if ( $parent->have_posts() ) : ?>
	<ul class="faqs">
	    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
	        <li>
	        	<a href="#catfaq<?php the_ID(); ?>"><?php the_title(); ?></a>
	        	<div id="faq<?php the_ID(); ?>"><?php the_content(); ?></div>
	        </li>
	    <?php endwhile; ?>
	</ul>
	<div class="back-to-top"><a class="js-back-to-top">Back to Top</a></div>
	<?php endif; wp_reset_query();

	$out = ob_get_clean();

	return $out;
}
add_shortcode( 'subpagelist', 'subpagelist_func' );

function initdb() {
	global $wpdb;
	$has_ext = $wpdb->get_row("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '".$wpdb->dbname."' AND TABLE_NAME = '".$wpdb->prefix."swpm_members_tbl' AND COLUMN_NAME = 'ext'");
	if ( !$has_ext ) {
		$wpdb->query('ALTER TABLE `'.$wpdb->prefix.'swpm_members_tbl` ADD `ext` VARCHAR(100) NULL');
		$wpdb->query('ALTER TABLE `'.$wpdb->prefix.'swpm_members_tbl` ADD `title` VARCHAR(100) NULL');
		$wpdb->query('ALTER TABLE `'.$wpdb->prefix.'swpm_members_tbl` ADD `cellphone` VARCHAR(100) NULL');
		$wpdb->query('ALTER TABLE `'.$wpdb->prefix.'swpm_members_tbl` ADD `company_logo` VARCHAR(100) NULL');
	}
} initdb();

// Get customer Company Logo
function get_user_company_logo() {
	$current_user = wp_get_current_user();
	
	$company_logo = '';
	$logo_id = get_user_meta($current_user->ID, 'company_logo', true);
	$company_name = get_user_meta($current_user->ID, 'company_name', true);

	if( $logo_id ) {
		$company_logo   = wp_get_attachment_url( $logo_id );
	}

	$logo = $company_logo ? '<img class="img-responsive" src="'.$company_logo.'">' : $company_name;
	if( $logo ) {
		$logo = '<h3 class="no-margin">'.$logo.'</h3>';
	}else {
		//$logo = '<img style="margin-bottom:20px;" src="'.get_template_directory_uri().'/images/torin-logo-placeholder.jpg">';
	}
	
	return $logo;
}

// Get Customers List
function customers_assign_pm( $pm_id = null ) {
	$userInfo = wp_get_current_user();
	$user_data = '';
	
	$args = array(
		'role'			=>	'customer',
		'meta_key'		=>	'assigned_pm',
		'meta_value'	=>	$userInfo->ID
	);
	
	if( $userInfo->roles[0] == 'project-manager') {
		$user_data = get_users( $args );
	}
	
	return $user_data;
}

// Get user address
function get_customer_address() {
	$user_info = wp_get_current_user();
	$country = strtolower(get_field('countries', 'user_'.$user_info->ID));
	$state = get_field('profile_state', 'user_'.$user_info->ID);
	
	if( $country == 'canada' ) {	
		$state = get_field('profile_state_canada', 'user_'.$user_info->ID);
	}
	
	return array( 'country' => $country, 'state' => $state );
}

add_filter( 'upload_size_limit', 'override_filter_site_upload_size_limit', 20 );

function override_filter_site_upload_size_limit() {
    return 60000000;
}

// Load acf json when updating acf

add_filter('acf/settings/load_json', 'my_acf_json_load_point');

function my_acf_json_load_point( $paths ) {
    
    // remove original path (optional)
    unset($paths[0]);    
    // append path
    $paths[] = get_stylesheet_directory() . '/acf-json';
    // return
    return $paths;
    
}