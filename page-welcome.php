<?php 
/* Template Name: Welcome Page */
get_header();
?>

<section class="header-image-new">

  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>
<section class="content">
  <div class="container"> 
	
	<?php 
		if( is_user_logged_in() ) {
	?>
	  
	<div class="bg-side"></div>    
	<div class="row">
	
	 <div class="clear"></div>	 
	  
		<div class="col-md-3 sidebar_wrap">			
			<?php 
				get_sidebar('menu'); 
			?>
		</div>
		
		<div class="col-md-9 col-xs-12 content_wrap welcome-right">
		
			<div class="dashboard_content">
				<p>Welcome to the TDI Customer Portal. here you’ll find quicklinks to customer resources, helpful information and company updates.Continue to check back for new features and tools as well as the latest news Torin Drive International.</p>
				
				<hr>
				
				<!-- Search Section-->
				<div class="search_wrapper">
					<form action="" method="POST">
						<div class="form-group has-feedback">							
							<input type="text" id="search-input" name="Search" placeholder="Search" class="form-control"/>
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</div>
						<div class="clear"></div>
					</form>
				</div>
				
				<!-- Dashboard Filter-->
				<div class="filter-wrapper">
					<form method="POST" action="">
						<div class="filter-container">
							<div class="filter-item">
								<select>
									<option>Quote #: All</option>
								</select>
							</div>
							<div class="filter-item">
								<select>
									<option>Date Submitted #: All</option>
								</select>
							</div>
							<div class="filter-item">
								<select>
									<option>Job Name #: All</option>
								</select>
							</div>
							<div class="filter-item">
								<select>
									<option>Consultant #: All</option>
								</select>
							</div>
							<div class="filter-item">
								<select>
									<option>Location City #: All</option>
								</select>
							</div>
							<div class="filter-item">
								<select>
									<option>Status #: All</option>
								</select>
							</div>
							<div class="filter-item">
								<select>
									<option>Amount #: All</option>
								</select>
							</div>
						</div>
					</form>
				</div>
				
				<!-- Table Content-->
				<div class="table-wrapper">
					<table class="table">
						<thead class="thead-inverse">
							<tr>
								<th>Quote #</th>
								<th>Date Submitted</th>
								<th>Job Name</th>
								<th>Contact</th>
								<th>Consultant</th>
								<th>Location</th>
								<th>Amount</th>
							</tr>							
						</thead>
						<tbody>
							<tr>								
								<td>TRD17-0001</td>
								<td>04-18-2017</td>
								<td>Lorem Ipsum Dolor Sit Amet</td>
								<td>Branden Hopkins</td>
								<td>Tom Smith</td>
								<td>Salt Lake City,Utah</td>
								<td>$ 10, 000.00</td>							
							</tr>
							<tr>
								<td colspan="7">
									<a href="#" class="closed btn btn-primary">closed</a>
									<span class="link-action">
										<a href="">Request Final</a>
										<a href="">Remove Inquiry</a>
										<a href="">PDF Download</a>
									</span>	
								</td>
							</tr>
							<tr>								
								<td>TRD17-0001</td>
								<td>04-18-2017</td>
								<td>Lorem Ipsum Dolor Sit Amet</td>
								<td>Branden Hopkins</td>
								<td>Tom Smith</td>
								<td>Salt Lake City,Utah</td>
								<td>$ 10, 000.00</td>							
							</tr>
							<tr>
								<td colspan="7">
									<a href="#" class="initial btn btn-primary">closed</a>
									<span class="link-action">
										<a href="">Request Final</a>
										<a href="">Remove Inquiry</a>
										<a href="">PDF Download</a>
									</span>	
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				
			</div>
		</div>	  
    </div>
	  
    <?php } else {  ?>
	
	<?php get_template_part('restricted-error'); ?>
	
	<?php } ?>
	
  </div>
</section>
<?php get_footer(); ?>