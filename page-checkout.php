<?php 
/* Template Name: Checkout Page */
	get_header();
?>
<style>
.order-summary {
	
border: 2px solid #446084;
    padding: 15px 30px 30px;
}
.order-summary h3 {

    color: #555;
    width: 100%;
    margin-top: 0;
    margin-bottom: .5em;
    text-rendering: optimizeSpeed;
   text-transform : uppercase;
}
table {
    width: 100%;
    margin-bottom: 1em;
    border-color: #ececec;
    border-spacing: 0;
}
thead {
    display: table-header-group;
    vertical-align: middle;
    border-color: inherit;
}
tr {
    display: table-row;
    vertical-align: inherit;
    border-color: inherit;
}
th, td {
    padding: .5em;
    text-align: left;
    border-bottom: 1px solid #ececec;
    line-height: 1.3;
    font-size: .9em;
}
.shop_table .cart_item td {
    padding-top: 15px;
    padding-bottom: 15px;
}
td.product-name {
    word-break: break-word;
    text-overflow: ellipsis;
}
th:first-child, td:first-child {
    padding-left: 0;
}
td {
    color: #666;
}
.woocommerce-checkout-payment ul {
list-style:none !important;
}
.woocommerce-billing-fields {
    padding-top: 15px;
    border-top: 2px solid #ddd;
}
.message-wrapper {
    margin: 0;
    padding-bottom: .5em;
}
form.checkout h3 {
    font-size: 1.1em;
    overflow: hidden;
    padding-top: 10px;
    font-weight: bolder;
    text-transform: uppercase;
}
.wc_payment_methods li {
  list-style:none  !important;
}
#place_order{
padding: 10px;
    border: #446084 1.5px solid;
    color: #446084;
    text-transform: uppercase;
    font-style: bold;
    font-weight: bold;
   background: #fff;
}
#place_order:hover {
  color : #fff; 
  background: #446084;

}
.card .card-body {
	padding:20px;
}
.card  {
    background-color: #fff;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    border: 1px solid #999;
    border: 1px solid rgba(0,0,0,.2);
    border-radius: 6px;
    outline: 0;
    -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
    box-shadow: 0 3px 9px rgba(0,0,0,.5);
}
.card .card-header {
     background: #333;
  
    color: #fff;
    font-size: 19px;
    padding: 24px 20px;
    text-shadow: 1px 1px 3px #000;
    text-align: center;
}
input[type='email'], input[type='date'], input[type='search'], input[type='number'], input[type='text'], input[type='tel'], input[type='url'], input[type='password'], textarea, select, .select-resize-ghost, .select2-container .select2-choice, .select2-container .select2-selection {
    box-sizing: border-box;
    border: 1px solid #ddd;
    padding: 0 .75em;
    height: 2.507em;
    font-size: .97em;
    border-radius: 0;
    max-width: 100%;
    width: 100%;
    vertical-align: middle;
    background-color: #fff;
    color: #333;
    box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
    transition: color .3s, border .3s, background .3s, opacity .3s;
}
p.form-row-wide {
    clear: both;
}
.woocommerce-billing-fields p {
    margin-bottom: .5em;
}
form p {
    margin-bottom: .5em;
}

p {
    margin-top: 0;
}
.woocommerce-billing-fields > h3 {
 
}
.woocommerce-additional-fields > h3 {

display:none;
}
.input-text {
display: block;
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 12px !important;
    line-height: 1.42857143;
    color: #555;
   }
input[type='select'] {
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 12px !important;
    line-height: 1.42857143;
 
}
.select2 {
  font-size: 12px !important;}
.select {
 font-size: 12px !important;
}
.woocommerce-shipping-fields > p > label {
display :none;
}
.order-summary {
	display: none;

}
.woocommerce-checkout-payment {
	display: none;
}
</style>
<section class="header-image-new">
  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">
	<?php 
		if( is_user_logged_in() ):
	?>	  
		<div class="bg-side"></div>
		<div class="row">
		 
		  <div class="col-md-3 sidebar_wrap welcome-left">       
			<?php 
				get_sidebar('menu'); 
			?>
		  </div>
		  <div class="col-md-9 content_wrap welcome-right">

											<?php

$checkout = WC()->checkout();


// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

?>
<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data"  id="myForm">
 <div class="row">
	<div class="col-md-12">
	<div class="card">
	<div class="card-header">
		<h3 class="card-title">CHECK OUT - STEP <span id="step"></span>/3</h3>
		<?php
  $all_meta_for_user = get_user_meta(get_current_user_id() );
 
?>
	</div>
	<div class="card-body">
		<div class="purchase-step space-2 shown" id="step1">
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Delivery Date:</div>
                                    <div class="col-md-9"><input type="date" name="delivery_date" class="form-control" id="delivery_date" required=""></div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Ship To:</div>
                                    <div class="col-md-9">
                                        <div class="row row-xs">
                                            <div class="col-md-8 mb4"><input type="text" name="shipping_address" placeholder="* Address" id="shipping_address" class="form-control " value="<?php  echo $all_meta_for_user['billing_address_1'][0];?>" required=""></div>
                                            <div class="col-md-4 mb4"><input type="text" name="shipping_city" placeholder="* City" id="shipping_city" class="form-control"  value="<?php  echo $all_meta_for_user['billing_city'][0];?>" required=""></div>
                                            <div class="col-md-4"><input type="text" name="shipping_state" id="shipping_state" placeholder="* State/Province" class="form-control " required=""   value="<?php  echo $all_meta_for_user['billing_state'][0];?>"></div>
                                            <div class="col-md-4"><input type="number" name="shipping_zip" placeholder="* Zip" id="shipping_zip" class="form-control " required="" value="<?php  echo $all_meta_for_user['billing_postcode'][0];?>"></div>
                                            <div class="col-md-4"><input type="text" name="shipping_country" placeholder="* Country" id="shipping_country" class="form-control " required="" value="<?php  echo $all_meta_for_user['billing_country'][0];?>"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Bill To :</div>
                                    <div class="col-md-9">
                                        <div class="row row-xs">
                                            <div class="col-md-8 mb4"><input type="text" name="billing_address" placeholder="* Address" id="billing_address" class="form-control" required="" value="<?php  echo $all_meta_for_user['billing_wooccm20'][0];?>"></div>
                                            <div class="col-md-4 mb4"><input type="text" name="billing_city" placeholder="* City" id="billing_citys"  class="form-control" required=""  value="<?php  echo $all_meta_for_user['billing_wooccm23'][0];?>"></div>
                                            <div class="col-md-4"><input type="text" name="billing_state" placeholder="* State/Province" id="billing_states"  class="form-control" required="" value="<?php  echo $all_meta_for_user['billing_wooccm22'][0];?>"></div>
                                            <div class="col-md-4"><input type="number" name="billing_zip" placeholder="* Zip" id="billing_zip" class="form-control" required="" value="<?php  echo $all_meta_for_user['billing_wooccm24'][0];?>"></div>
                                            <div class="col-md-4"><input type="text" name="billing_country" placeholder="* Country" id="billing_countrys" class="form-control" required="" value="<?php  echo $all_meta_for_user['billing_wooccm21'][0];?>"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Shipping Type:</div>
                                    <div class="col-md-9">
                                        <select name="shipping_type" class="form-control " id="shipping_type" required="">
                                            <option value="0">Customer Account</option>
                                            <option  value="1">Customer Pickup</option>
                                            <option  value="4">TDI Prepay - 24Hr Lift Gate</option>
                                            <option value="5">TDI Prepay - Regular</option>
                                            <option value="2">TDI Pre - Express On</option>
                                             <option value="3">TDI Pre - Fed Ground</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Customer Carrier:</div>
                                    <div class="col-md-9"><input type="text" name="customer_carrier" id="customer_carrier" class="form-control js-sci" required=""></div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Cus. Shipping Acct#:</div>
                                    <div class="col-md-9"><input type="text" name="customer_shipping_account" id="customer_shipping_account" class="form-control js-sci" required=""></div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Shipping Contact Name:</div>
                                    <div class="col-md-9"><input type="text" name="shipping_contact_name" id="shipping_contact_name" class="form-control js-xsci" required="" value="<?php  echo $all_meta_for_user['billing_wooccm18'][0];?>"></div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Shipping Contact Phone:</div>
                                    <div class="col-md-9"><input type="text" name="shipping_contact_phone" id="shipping_contact_phone" class="form-control js-xsci" required="" min="11" value="<?php  echo $all_meta_for_user['billing_phone'][0];?>"></div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Shipping Contact Email:</div>
                                    <div class="col-md-9"><input type="email" name="shipping_contact_email" id="shipping_contact_email" class="form-control js-xsci" required="" value="<?php  echo $all_meta_for_user['billing_email'][0];?>"></div>
                                </div>
                            </div>

		<div class="purchase-step " id="step2">
                                <div class="js-pt">
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right">Payment Type</div>
                                        <div class="col-md-8">
                                            <select name="payment_type" id="payment_type" class="form-control js-payment_type" required="">
                                                <option value="po" selected="">PO</option>
                                                <option value="credit-card">Credit Card</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="js-pt-po">
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> PO Number:</div>
                                        <div class="col-md-8"><input type="text" name="po_number" class="form-control js-po" id="po_number" required=""></div>
                                    </div>
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> PO File <small>(PDF only)</small>:</div>
                                        <div class="col-md-8"><input type="file" name="billing_wooccm19" class="form-control js-po" accept="application/pdf" id="billing_wooccm19_file" required=""></div>
                                    </div>
                                </div>
                                <div class="js-pt-cc">
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> Card Type:</div>
                                        <div class="col-md-8">
                                            <select name="card_type" class="form-control js-card_type js-cc" required="">
                                                <option value="visa" selected="">Visa</option>
                                                <option value="american-express">American Express</option>
                                                <option value="master-card">Master Card</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> Card Number:</div>
                                        <div class="col-md-8"><input type="text" name="card_number" id="card_number" class="form-control js-cc ccFormatMonitor" maxlength="19" placeholder="4###-####-####-####" required=""></div>
                                    </div>
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> CVV:</div>
                                        <div class="col-md-2"><input type="number" name="card_cvv" id="card_cvv" class="form-control js-cc" maxlength="3" required=""></div>
                                    </div>
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> Expiry Date:</div>
                                        <div class="col-md-8">
                                            <div class="row row-xs">
                                                <div class="col-md-6">
                                                    <select name="card_expiry_month" class="form-control js-cc" id="month" required="">
                                                                                                                    <option value="01">January</option>
                                                                                                                    <option value="02">February</option>
                                                                                                                    <option value="03">March</option>
                                                                                                                    <option value="04">April</option>
                                                                                                                    <option value="05">May</option>
                                                                                                                    <option value="06">June</option>
                                                                                                                    <option value="07">July</option>
                                                                                                                    <option value="08">August</option>
                                                                                                                    <option value="09">September</option>
                                                                                                                    <option value="10">October</option>
                                                                                                                    <option value="11">November</option>
                                                                                                                    <option value="12">December</option>
                                                                                                            </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <select name="card_expiry_year" class="form-control js-cc" id="year" required="">
                                                                                                                    <option>2019</option>
                                                                                                                    <option>2020</option>
                                                                                                                    <option>2021</option>
                                                                                                                    <option>2022</option>
                                                                                                                    <option>2023</option>
                                                                                                                    <option>2024</option>
                                                                                                                    <option>2025</option>
                                                                                                                    <option>2026</option>
                                                                                                                    <option>2027</option>
                                                                                                                    <option>2028</option>
                                                                                                                    <option>2029</option>
                                                                                                                    <option>2030</option>
                                                                                                                    <option>2031</option>
                                                                                                                    <option>2032</option>
                                                                                                                    <option>2033</option>
                                                                                                                    <option>2034</option>
                                                                                                                    <option>2035</option>
                                                                                                                    <option>2036</option>
                                                                                                                    <option>2037</option>
                                                                                                                    <option>2038</option>
                                                                                                                    <option>2039</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   <!--  <div style="padding: 10px 10px 0px;">
                                        <small>Note: there is 3.4% fee if you choose Credit Card.</small>
                                    </div> -->
                                </div>
                            </div>


                         <div class="purchase-step " id="step3">
                            					<?php do_action( 'woocommerce_checkout_order_review' ); ?>
                            </div>
	<?php if ( $checkout->get_checkout_fields() ) : ?>
		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div id="customer_details" style="display:none;">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
				
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>
		<div class="order-summary">
	
			<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
	
			<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>
	
			<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

			<div id="order_review" class="woocommerce-checkout-review-order">
				<?php do_action( 'woocommerce_checkout_order_review' ); ?>
			</div>
		<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
	</div>
		<div class="row">
			<div class="col-md-12">
				<span class="pull-right">	
				<button type="button" class="btn btn-default" id="back">Back</button>
				<button type="button" class="btn btn-primary" id="next">Next</button>	
				<button type="submit" class="btn btn-primary" id="confirm">Confirm</button>	
			</span>
			</div>
			
		</div>
	</div>
	</div>
	</div>

</div>
</form>

<!-- do_action( 'woocommerce_before_checkout_form', $checkout ); -->
<br><br>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>

		  </div>		
		 
	<?php else : ?>
		
		<?php get_template_part('restricted-error'); ?>
		
	<?php endif; ?>
  </div>
</section>
<?php get_footer(); ?>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="https://rawgit.com/lopezton/jquery-creditcard-formatter/master/ccFormat.js"></script>

<script>
	$(function() {
	$('#confirm').hide();
	 $('[name=payment_method]').val('cod');
	$('#payment_type').val('po');
	var step = 1 ;
	$('#step').html(step);
	if(step ==1) {
		$('#back').hide();
		$('#step1').show();
		$('#step2').hide();
		$('#step3').hide();
	}
	 $('#payment_type').val('po');

	var pay_type = $('#payment_type').val();
	if(pay_type =='po') {
	 $('.js-pt-po').show();
	 $('.js-pt-cc').hide();
	}
$('#payment_type').on('change',function(){
	pay_type = $(this).val();
	if(pay_type =='po') {
	 $('.js-pt-po').show();
	 $('.js-pt-cc').hide();
	 $('[name=payment_method]').val('cod');

	}
	else {
		 $('.js-pt-po').hide();
	 	 $('.js-pt-cc').show();
	 	  $('[name=payment_method]').val('authnet');
	
	}

});
	$('#next').on('click',function(){


		
		$('#authnet-card-number').val($('#card_number').val());
		$('#authnet-card-expiry').val($('#month').val() +'/'+ $('#year').val() );
		$('#authnet-card-cvc').val($('#card_cvv').val());
		//SHIP TO
		$('#billing_wooccm12').val($('#delivery_date').val());
		$('#billing_wooccm20').val($('#shipping_address').val());
		$('#billing_wooccm23').val($('#shipping_city').val());
		$('#billing_wooccm21').val($('#shipping_country').val());
		$('#billing_wooccm22').val($('#shipping_state').val());
		$('#billing_wooccm24').val($('#shipping_zip').val());

		//BILL TO
		$('#billing_address_1').val($('#billing_address').val());
		$('#billing_city').val($('#billing_citys').val());
		$('#billing_country').val($('#billing_countrys').val());
		$('#billing_state').val($('#billing_states').val());
		$('#billing_postcode').val($('#billing_zip').val());

		//SHIPPING TYPE
		$('#billing_wooccm15').val($('#shipping_type').val());

		// $('#billing_wooccm16').val($('#customer_carrier').val());
		// $('#billing_wooccm17').val($('#customer_shipping_account').val());

		$('#authnet-card-number').val($('#card_number').val());
		$('#authnet-card-expiry').val($('#month').val() +'/'+ $('#year').val() );
		$('#authnet-card-cvc').val($('#card_cvv').val());
		$('#billing_wooccm18').val($('#shipping_contact_name').val());
		//shipping_contact_email
		$('#billing_phone').val($('#shipping_contact_phone').val());
		$('#billing_email').val($('#shipping_contact_email').val());
		$('#authnet-card-cvc').val($('#card_cvv').val());
		$('#step').html(step);
 	  	var pay_type = $('#payment_type').val();
 	  	var ship_type =$('#ship_type').val();
		if(pay_type =='po') {
			$('#billing_wooccm11').val($('#po_number').val());
		} 
		if(ship_type > 0) {

				$('#billing_wooccm16').val('N/A');
				$('#billing_wooccm17').val('N/A');
		}
		var validator = $("#myForm").validate();

		
		if(step == 1) {
			$('#step2').hide();
			$('#back').hide();
			$('#step1').show();
			$('#step3').hide();

			validator.element("#delivery_date" );
			validator.element("#shipping_address" );
			validator.element("#shipping_city" );
			validator.element("#shipping_state" );
			validator.element("#shipping_zip" );
			validator.element("#shipping_country" );
			validator.element("#billing_address" );
			validator.element("#billing_citys" );
			validator.element("#billing_states" );
			validator.element("#billing_zip" );
			validator.element("#billing_countrys" );
			validator.element("#shipping_type" );
			validator.element("#customer_carrier" );
			validator.element("#customer_shipping_account");
			validator.element("#shipping_contact_name");
			validator.element("#shipping_contact_phone");
			validator.element("#shipping_contact_email");


			var isValidDate 				= validator.invalid.delivery_date;
			var isValidShippingAddress 		= validator.invalid.shipping_address;
			var isValidshipping_state 		= validator.invalid.shipping_state;
			var isValidshipping_city 		= validator.invalid.shipping_city;
			var isValidshipping_zip 		= validator.invalid.shipping_zip;
			var isValidshipping_country		= validator.invalid.shipping_country;
			var isValidbilling_address		= validator.invalid.billing_address;
			var isValidbilling_citys		= validator.invalid.billing_citys;
			var billing_states		= validator.invalid.billing_states;
			var billing_zip		= validator.invalid.billing_zip;
			var billing_countrys		= validator.invalid.billing_countrys;
			var shipping_type		= validator.invalid.shipping_type;
			var customer_carrier		= validator.invalid.customer_carrier;
			var customer_shipping_account		= validator.invalid.customer_shipping_account;
			var shipping_contact_name		= validator.invalid.shipping_contact_name;
			var shipping_contact_phone		= validator.invalid.shipping_contact_phone;
			var shipping_contact_email		= validator.invalid.shipping_contact_email;
				
			function isValid(element, index, array) {

 				 return element  != true;
			
			}
			var fields = [isValidDate,isValidShippingAddress,isValidshipping_state,isValidshipping_city,isValidshipping_zip,isValidshipping_country,isValidbilling_address,isValidshipping_country,isValidbilling_address,isValidbilling_citys,billing_states,billing_zip,billing_countrys,shipping_type,customer_carrier,customer_shipping_account,shipping_contact_name,shipping_contact_phone,shipping_contact_email] ;
			console.log(fields);
			console.log(fields.indexOf("true"));
			var foundFieldsInfo = fields.find(function(element) { 
 				 return element ==true; 
				}); 
				if(foundFieldsInfo !=true) {
						step ++;
							$('#step').html(step);
						console.log(step)
				}

		}
		if(step==2) {
			$('#step1').hide();
			$('#step2').show();
			$('#step3').hide();
			$('#back').show();
							
					
			validator.element("#po_number");
			validator.element("#billing_wooccm19_file");
			validator.element("#card_number");
			validator.element("#card_cvv");
			validator.element("#month");
			validator.element("#year");

			var po_number = validator.invalid.po_number;
			var file = validator.invalid.file;
			var card_number = validator.invalid.card_number;
			var card_cvv = validator.invalid.card_cvv;
			var month = validator.invalid.month;
			var year = validator.invalid.year;

			var fields2 = [po_number,file,card_number,card_cvv,month,year];
				var foundFieldsInfo2 = fields2.find(function(element) { 
 				 return element ==true; 
				}); 	

			$('#po_number').attr('required',true);
			$('#billing_wooccm19_file').attr('required',true);

			var pay_method = $('#payment_type').val();
			console.log(pay_method);
			if(pay_method == 'po') {
				$('#po_number').attr('required',true);
				$('#billing_wooccm19_file').attr('required',true);
				$('#card_number').attr('required',false);
				$('#card_cvv').attr('required',false);
				$('#month').attr('required',false);
				$('#year').attr('required',false);


			}
			else {
				$('#po_number').attr('required',false);
				$('#billing_wooccm19_file').attr('required',false);
				$('#card_number').attr('required',true);
				$('#card_cvv').attr('required',true);
				$('#month').attr('required',true);
				$('#year').attr('required',true);

			}
			if(foundFieldsInfo2 !=true) {
						step  ++;
						
						$('#step').html(step);
				}
			

		


		}
		if(step==3) {
			$('#authnet-card-number').val($('#card_number').val());
			$('#authnet-card-expiry').val($('#month').val() +'/'+ $('#year').val() );
			$('#authnet-card-cvc').val($('#card_cvv').val());
			
			$('#step1').hide();
			$('#step2').hide();
			$('#step3').show();
			$('#next').hide();
			$('#back').show();
			$('#confirm').show();

		}

	

	});
	$('#back').on('click',function(){
		step --;
		$('#step').html(step);
		
		if(step == 1) {
		$('#step2').hide();
		
			$('#back').hide();
			$('#step1').show();
			$('#step3').hide();

		}
		if(step==2) {
			$('#step1').hide();
			$('#step2').show();
			$('#step3').hide();

		}
		if(step < 3) {
			$('#next').show();
			$('#step3').hide();
			$('#confirm').hide();
		}

	});

	$('#ship-to-different-address  span').html('Ship To :');
	$('.woocommerce-billing-fields h3').html('<bold>BILL TO :</bold>');
	$('.order-summary').hide();
	$('#billing_wooccm11_field').hide();
	$('#billing_wooccm19_field').hide();

	$('#shipping_type').on('change', function(){
			var ship_types = $(this).val();

			if( ship_types > 0  ){

				$('#customer_shipping_account').attr('disabled',true);
				$('#customer_carrier').attr('disabled',true);
				$('#customer_shipping_account').attr('required',false);
				$('#customer_carrier').attr('required',false);
				$('#billing_wooccm16').val('N/A');
				$('#billing_wooccm17').val('N/A');
			}
			else if(ship_types == 0 ) {	
				
				$('#customer_shipping_account').attr('disabled',false);
				$('#customer_carrier').attr('disabled',false);
				$('#customer_shipping_account').attr('required',true);
				$('#customer_carrier').attr('required',true);
				$('#billing_wooccm16').val($('#customer_carrier').val());
				
				$('#billing_wooccm17').val($('#customer_shipping_account').val());
			}
	});
	
});
</script>