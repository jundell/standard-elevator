<?php 

// Template Name: Home Page v2

get_header(); 
$post_id = get_option( 'page_on_front' );
?>
<?php
$brasil = array_shift((explode('.', $_SERVER['HTTP_HOST'])));
//$brasil = array_shift((explode('.', 'dev.torindrive-india')));
if( $brasil == 'brasil' ){

?>
<style>
  .banner-btn-extra-class{
      width:100%;
    }
  @media (min-width: 768px){
    .banner-buttons-links {
      margin: 0 25px;
    }
    .banner-btn-extra-class{
      width:180px;
    }
  }
</style>
<?php } ?>

<!-- slider -->
<section id="hero-slider">

  <?php if(count(get_field('image_slider')) > 1): ?>

    <div id="slider-wrapper" class="">
        <?php if( have_rows('image_slider', $post_id) ): ?>
        <div id="slider">
        <?php while( have_rows('image_slider', $post_id ) ): the_row(); 
            // vars
            $image = get_sub_field('image_slide');
            $title = get_sub_field('image_title');
            $sub_title = get_sub_field('sub_title');
        ?>
        <div class="item" style="background-image:url('<?php echo $image['url']; ?>');">
            <?php if( !empty( $title) || !empty( $sub_title ) ): ?>
            <div class="outer">
                <div class="inner">
                    <h2>
                    <?php echo $title; ?><span class="slider-lowercase">
                    <?php echo $sub_title; ?></span>
                    </h2>
                </div>
            </div>
            <?php endif; ?>
        </div>

        <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>

  <?php else: ?>

        <?php if( have_rows('image_slider', $post_id) ): ?>
        <?php while( have_rows('image_slider', $post_id ) ): the_row(); 
            // vars
            $image = get_sub_field('image_slide');
            $title = get_sub_field('image_title');
            $sub_title = get_sub_field('sub_title');
        ?>
        <div id="slider-wrapper" class="single-slide-bg--" style="background-image:url('<?php echo $image['url']; ?>');">
            <?php if( !empty( $title) || !empty( $sub_title ) ): ?>
            <div class="outer">
                <div class="inner">
                    <h2>
                    <?php echo $title; ?><span class="slider-lowercase">
                    <?php echo $sub_title; ?></span>
                    </h2>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
        <?php endif; ?>
    </div>

  <?php endif; ?>

  <!-- panels -->
  <div id="panel-info">
    <div class="container">
      <?php if( have_rows('panels', $post_id) ): ?>
      <div class="banner-buttons">
        <?php while( have_rows('panels', $post_id) ): the_row(); 
          // vars
          $title = get_sub_field('title');
          $content = get_sub_field('content');
          $link = get_sub_field('link');
        ?>

        <div class="banner-buttons-links">
          <?php if( $link ): ?>
          <span class="home-ref-button"><a href="<?php echo $link; ?>" class="banner-btn-extra-class">
          <?php endif; ?>
          <?php echo $title; ?>
          <?php if( $link ): ?>
          </a></span>
          <?php endif; ?>
        </div>

        <?php endwhile; ?>
      </div>
      <?php endif; ?>
    </div>
  </div>
</section>
<?php get_footer(); ?>