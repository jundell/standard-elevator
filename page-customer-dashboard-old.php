<?php 
	/* Template Name: Customer Dashboard v1.0 */
	get_header();
?>

<section class="header-image-new">

  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">
    <div class="bg-side"></div>
    <div class="row">
      <div class="col-sm-12">
        <?php
          // Start the loop.
          while ( have_posts() ) : the_post();
          // the_content();
          // End the loop.
          endwhile;
        ?>
      </div>
      <div class="clear"></div>
     
      <div class="col-md-3 sidebar_wrap welcome-left">
		<?php get_sidebar('menu'); ?>
      </div>
      <div class="col-md-9 col-xs-12 content_wrap welcome-right">       
          
          <?php the_content(); $header = ''; $last_date = '' ?>
            <?php
              global $post;
              $showall = @$_GET['show'] == 'all';
              $per_page = ( $showall ) ? -1 : 5;
              $args = array( 'posts_per_page' => $per_page, 'post_status' => 'publish' );
              $myposts = get_posts( $args );
              foreach ( $myposts as $post ) : 
                setup_postdata( $post );
                $this_date = get_the_date('Y');
            ?>
              <?php if ( $showall && !$header ) : $header = true; $last_date = $this_date; ?>
                <h4><?php echo $this_date; ?></h4>
                <ul class="alink">
              <?php elseif ( $showall && $this_date != $last_date ) : $last_date = $this_date; ?>
                </ul>
                <h4><?php echo $last_date; ?></h4>
                <ul class="alink">
              <?php elseif ( !$showall ) : ?>
                <ul class="alink">
              <?php endif; ?>

              <li><a href="<?php the_field('pdf_file'); ?>" target="_blank"><?php the_title(); ?></a></li>
            <?php endforeach;
            wp_reset_postdata(); ?>
          </ul>
          <?php if ( !$showall ) : ?>
            <a class="btn btn-link view-all-rako" href="?show=all">View All Bulletins &raquo;</a>
          <?php endif; ?>
          <?php
            $requests = custom_repeater_data('requests',array('button_text','button_page'));
          ?>
          <hr>
          <div class="box-wrapper">
            <div class="box-wrap">
              <div class="action-box">
                <div class="box-content">
                  <i class="fa fa-calculator"></i>
                  <h3><?php echo $requests[1]['button_text'] ?></h3>
                  <p>for bidding purpose only</p>
                  <a class="btn-box" href="<?php echo $requests[1]['button_page'] ?>">Start</a>
                </div>
              </div>
              <div class="action-box box-sep">
                <div class="box-content">
                  <span class="glyphicon glyphicon-arrow-right"></span>
                </div>
              </div>
              <div class="action-box box-green">
                <div class="box-content">
                  <i class="fa fa-group"></i>
                  <h3><?php echo $requests[2]['button_text'] ?></h3>
                  <p>once you have the job</p>
                  <a class="btn-box" href="<?php echo $requests[2]['button_page'] ?>">Obtain</a>
                </div>
              </div>
            </div>
          </div>
      </div>
      
    </div>
  </div>
</section>
<?php get_footer(); ?>