<?php
/* Template Name: Cart Page */
	get_header();
?>
<style>
.cart-collaterals {
    margin-bottom: 20px;
}
.wc-proceed-to-checkout a {
	padding:10px;
	border:#446084 1.5px solid;
	color : #446084 ;
	text-transform : uppercase;
	font-style:bold;
	font-weight:bold;
	font-size:14px !important;
}
.wc-proceed-to-checkout a:hover{
	background-color:#446084;
	color : #ffffff;
}
.coupon button:hover {
	background-color:#446084;
	color : #ffffff;
}
.coupon button {

	padding:10px;
	border:#446084 1.5px solid;
	color : #446084 ;
	text-transform : uppercase;
	font-style:bold;
	font-weight:bold;
	background : transparent;

}
button:hover {
	background-color:#446084;
	color : #ffffff;

}
body {
padding-right: 0px !important;
}
button {
	padding:10px;
	border:#446084 1.5px solid;
	color : #446084 ;
	text-transform : uppercase;
	font-style:bold;
	font-weight:bold;
	background : transparent;

}
.shop-link  {

	margin-top : 40px;
	padding:10px;
	border:#446084 1.5px solid;
	color : #446084 ;
	text-transform : uppercase;
	font-style:bold;
	font-weight:bold;
	background : transparent;


}
.shop-link:hover  {
    background-color:#446084;
   color : #ffffff;

}


.shop_table tr td{
	padding:10px;
}
.shop_table {
    background-color :transparent;
}
.shop_table thead {
	/* background:black;
	color : #fff; */
}
#coupon_code{
	    width: auto;
    	padding: 10px;
}
.order-wrapper > .shop_table > tr >  td {
    background-color: transparent;
}
.order-wrapper > .shop_table > tr > .product-total {
    width:25%;
}
.cart-subtotal > td {
    background:transparent !important;
}
.order-wrapper  > table > tr > .product-name {
    background-color: transparent !important;
}

.shop_table tr td {
    background-color: transparent !important;
}
.shop_table > .cart_item {
    background-color: none;
}
.quantity > input {
    width: auto;
    padding: 10px;
}
.wc-proceed-to-checkout a {
    padding: 10px;
    border: #00a72e 1.5px solid;
    color: #fff;
    text-transform: uppercase;
    font-style: bold;
    font-weight: bold;
    background: #00a72e !important
	;
}
.wc-proceed-to-checkout a:hover {
    background-color: #00a72e;
    color: #ffffff;
}
.order-wrapper {

    border : 1px solid #176fa5;
    margin :10px;
}
.order-wrapper table > thead > tr:first-child>th {
    width: 0px;
    padding: 0px 6px !important;
    height: 48px;
    vertical-align: middle;
    background-color: transparent !important;
    color: #333;
    border:1px solid #ddd;
}
.order-wrapper >table {
    background : transparent !important;
    width : 100%;
}



</style>

<style>
    hr {
        margin-top: 40px;
        margin-bottom: 40px;
        border: 0;
        border-top: 1px solid #7b7b7b;
    }
    .torin-calc .form-horizontal .form-group{
        margin-bottom: 10px;
        margin-right: 0;
        margin-left: 0;
    }
    .torin-calc .btn-custom-dropdown {
        background-color: hsl(12, 4%, 80%) !important;
        background-repeat: repeat-x;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#12f133134", endColorstr="#cecac9");
        background-image: -khtml-gradient(linear, left top, left bottom, from(#12f133134), to(#cecac9));
        background-image: -moz-linear-gradient(top, #12f133134, #cecac9);
        background-image: -ms-linear-gradient(top, #12f133134, #cecac9);
        background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #12f133134), color-stop(100%, #cecac9));
        background-image: -webkit-linear-gradient(top, #12f133134, #cecac9);
        background-image: -o-linear-gradient(top, #12f133134, #cecac9);
        background-image: linear-gradient(#12f133134, #cecac9);
        border-color: #cecac9 #cecac9 hsl(12, 4%, 70%);
        color: #333 !important;
        text-shadow: 0 1px 1px rgba(255, 255, 255, 0.66);
        -webkit-font-smoothing: antialiased;
    }
    .button_example{
		width:100%;
        text-transform: uppercase;
        line-height: 28px;
        height: 30px;
        border-radius: 3px;
        border: 1px solid #bbbaba;
        font-size: 9px;
        font-family: arial, helvetica, sans-serif;
        padding: 0px 22px 0px 13px;
        text-decoration: none;
        display: inline-block;
        color: #000;
        background: rgba(253,253,253,1);
        background: -moz-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(253,253,253,1)), color-stop(12%, rgba(250,250,250,1)), color-stop(28%, rgba(247,247,247,1)), color-stop(49%, rgba(234,232,232,1)), color-stop(76%, rgba(219,217,217,1)), color-stop(100%, rgba(213,210,210,1)));
        background: -webkit-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
        background: -o-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
        background: -ms-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
        background: linear-gradient(to bottom, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
	}

    .button_example:hover{
        border: 1px solid #bbbaba;
        background: rgba(232,232,232,1);
        background: -moz-linear-gradient(top, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(232,232,232,1)), color-stop(19%, rgba(230,230,230,1)), color-stop(36%, rgba(230,230,230,1)), color-stop(60%, rgba(230,230,230,1)), color-stop(81%, rgba(230,230,230,1)), color-stop(100%, rgba(213,210,210,1)));
        background: -webkit-linear-gradient(top, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        background: -o-linear-gradient(top, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        background: -ms-linear-gradient(top, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        background: linear-gradient(to bottom, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e8e8e8', endColorstr='#d5d2d2', GradientType=0 );
    }
    .filter-wrapper .filter-container .filter-item select {
        font-size: 9px;
        text-transform: uppercase;
    }
    .torin-calc .final{
        background-color: #ffd700;
    }
    .torin-calc .request-final{
        color:#B52C3E;
        font-weight: bold;
        text-transform: uppercase;
        text-decoration: underline;
        letter-spacing: 0.1px;
		font-size: 11px;
    }
    .torin-calc .underline{
        text-decoration: underline;
        text-transform: uppercase;
        color: #000;
		font-size: 11px;
    }
    .torin-calc .cust-scrollbar{
        overflow-x: hidden;
        max-height: 558px;
    }

    .force-overflow-cust
    {
        min-width: 847px;
    }
    .force-overflow-cust table{
        margin-left: -15px;
        height: 550px;
    }

    #style-2::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 1px rgba(0,0.3,0,0);
        /*-webkit-box-shadow: inset 0 0 1px rgba(0,0,0,0.3);*/
        background-color: #F5F5F5;
    }


    .quotation .table thead tr{
        background-color: #eee;
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#f2f2f2), to(#fafafa));
        background-image: -webkit-linear-gradient(top, #f2f2f2 0, #fafafa 100%);
        background-image: -moz-linear-gradient(top, #f2f2f2 0, #fafafa 100%);
        background-image: -ms-linear-gradient(top, #f2f2f2 0, #fafafa 100%);
        background-image: -o-linear-gradient(top, #f2f2f2 0, #fafafa 100%);
        font-size: 12px
    }
    .table-bordered {
        border: 1px solid #ddd;
    }
    .torin-calc table > thead > tr > th, .table > thead > tr > th, table.table>thead:first-child>tr:first-child>th{
        border:none;
		text-align: center;
    }
    .torin-calc .table thead tr{
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#f2f2f2), to(#fafafa));
    }
    .torin-calc .table>thead>tr>th{
        vertical-align: bottom;
        border-bottom: 2px solid #ddd;
        padding: 8px;
        line-height: 1.428571429;
    }
    .torin-calc .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        padding: 8px;
        line-height: 1.428571429;
        vertical-align: top;
        border:none;
    }
    .table tr{
        border: 1px solid #ddd;
    }

    .list-btns {
        margin: 0;
        padding: 0;
        list-style: none !important;
        list-style-type: none !important;
    }
    .list-btns li{
        margin-top: 10px;
    }
    .bg-color-red {
        background-color: rgb(237, 26, 36) !important;
        border-radius: 0;
    }
    .list-btns .txt-color-white{
        color: #fff;
        text-transform: uppercase;
        font-size: 12px;
        font-weight: bold;
    }
    .btn-block{
        padding: 5px 16px;
        display: block;
        width: 100%;
    }
    .img-logo{
        border: 2px solid #ccc;
        float: left;
        width: 150px;
        height: 100px;
        position: relative;
        margin-bottom: 10px;
    }
    .img-logo .img-responsive{
        height: 100%;
        width: 100%;
    }
   .quotation-quote .button {
        background-color: transparent;
        text-align: center;
        display: inline-block;
        text-decoration: none;
        border: 1px solid;
        -webkit-transition: all 0.2s ease-in;
        -moz-transition: all 0.2s ease-in;
        -o-transition: all 0.2s ease-in;
        transition: all 0.2s ease-in;
        padding: 10px 30px 8px;
        font-size: 16px;
        padding: 5px 8px 5px;

        font-weight: 300;
        letter-spacing: 0.2rem;
        line-height: 1;
        text-transform: uppercase;
        border-radius: 0;
    }
    .quotation-quote .button:hover{
        text-decoration: none;
        cursor: pointer;
        color: #000;
    }
    .quotation-quote .no-left-margin{
        margin-left: 0;
    }
    .quotation-quote .button-neutral{
        color: #000;
        border-color: #000;
        font-size: 11px;
    }
    .quotation-quote .button-neutral:hover{
        color: #fff;
        background-color: rgb(237, 26, 36);
        border-color:rgb(237, 26, 36);
    }
    .quotation-quote button.button.button-neutral.btn-xs {
        margin-bottom: 10px;
    }

/*


    .center-td{
        text-align: center;
    }
    .border-tbl{
        border: 1px solid #ccc;
    }
    .search-input{
        width: 25%;
        float: right;
        margin-bottom: 10px;
    }
*/

    .stylish-input-group .input-group-addon{
        background: white !important;
    }
    .stylish-input-group .form-control{
        border-right:0;
        box-shadow:0 0 0;
        border-color:#ccc;
    }
    .stylish-input-group button{
        border:0;
        background:transparent;
    }
    .page-template .sidebar_wrap h3 {
        color: #151515;
        margin: 15px 15px 15px 0;
        background: #FFF;
        box-shadow: 1px 1px 1px #929292;
    }
    .page-template .sidebar_wrap h3 img {
        max-width: 100%;
    }
    .torin-calc .nav-tabs>li>a{
        color:#000;
        border-radius: 1px;
    }

    .scrollbar /*gale*/
    {
        float: left;
        width: 847px;
        background: #F5F5F5;
        overflow-y: scroll;
        margin-bottom: 25px;
    }

    .force-overflow
    {
        min-width: 847px;
    }
    .force-overflow table{
        margin-left: -15px;
        width: 847px;
    }

    #style-3::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 1px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }

    #style-3::-webkit-scrollbar
    {
        width: 1px;
        background-color: #F5F5F5;
    }

    .csv-tbl .input-sm{
          border-radius: 0;
      }
    .csv-tbl input[type="text"]{
        /*width: 100px;*/
        font-size: 12px;
        padding: 5px;
    }
    .btn-save-csv {
        margin-bottom: 50px;
    }
    .csv-tbl >tbody>tr>td{
        padding:0;
    }
    .csv-tbl .tbl-header{
        font-size: 11px;
    }
    .black-th{
        background-color: #333333;
        color: #fff;
    }
  /*  .black-th.action{
        font-size: 11px;
    }*/
    table .glyphicon-remove{
        text-align: center;
        color: red;
    }
    .glyphicon-remove{
        cursor: pointer;
    }
    label.btn.btn-default.semiBold {
        background-color: #333333;
        color: #fff;
    }
    .torin-calc .nav>li>a{
        padding:15px 15px;
        color: #000;
        font-size: 14px;
        font-weight: bold;
    }
    .torin-calc .nav-tabs>li>a:hover, .torin-calc .nav-tabs>li.active>a{
        color:#fff;
        background-color: #333333;
    }
    .package-table tbody tr:nth-child(odd){background:#f5f4f4;}
    /*.package-table td{border-top:0px !important; border-right:1px solid #fff !important;}*/
    .package-table tbody tr:nth-child(even){background:#ececec; }
    .package-table tbody > tr td {
        font-size: 15px !important;
        font-weight: 600 !important;
        color: #333333;
        padding: 20px 0px 20px 31px !important;
    }
    .package-table > tbody > tr:first-child td {font-weight: normal;}
    .package-table span.glyphicon.glyphicon-edit{
        color:#000;
        font-size: 15px;
    }
    .package-table td, .package-table tr{border: none!important; padding: 5px 10px;}
    .package-table > tbody > tr > td{
        padding: 10px 10px 10px;
        height: 60px;
        vertical-align: middle;
    }
    .torin-calc .view-survey{
        color: #000;
    }

	.torin-calc .view-survey:hover,
    .toric-calc .view-survey:focus {
        text-decoration: none !important;
    }

    table .glyphicon-remove{
        padding-top: 7px;
    }
    .torin-calc .panel{
        border-radius: 0;
    }
    .torin-calc .panel-body {
        padding: 10px;
    }

    .torin-calc .panel-body p{
        margin-bottom: 0 !important;
        font-size: 13px;
    }
    .torin-calc .filter-wrapper .filter-container .filter-item select{
        height: auto;
    }
    .torin-calc .new-quote{
        border-radius: 0;
        min-width: 108px;
        padding: 3px;
        border: 0;
        background-color:#000;
        margin-top: 15px;
        margin-bottom: 15px;
        color:#fff;
    }
    .torin-calc .new-quote:hover{
        color:#fff;
    }
    .torin-calc .asc-btn{
        padding: 0;
        font-size: 13px;
        border-radius: 1px;
        color: #fff;
        border: 0;
        background-color: #000;
        outline:0;
    }
    .torin-calc .asc-btn:focus{
        outline: 0;
    }
    p.table-top-h1{
        text-transform: uppercase;
        font-weight: bold;
        font-size: 11px !important;
        margin-bottom: 3px !important;
        margin-left: 9px;
    }
    select.button_example.form-control{
        border-radius:0;
    }
    p.table-top-h1{
        text-transform: uppercase;
        font-weight: bold;
        font-size: 15px !important;
        margin-bottom: 3px !important;
        margin-left: 9px;
    }
    /*
    table.tablesorter thead tr .header{
        background-image: url(../torin/public/img/bg.png);
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }

    th.header.headerSortUp{
        background-image: url(../torin/public/img/trans-asc.png) !important;
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }
    th.header.headerSortDown{
        background-image: url(../torin/public/img/trans-desc.png) !important;
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }
    */
    th.center{
        text-align: center!important;
    }
    a.final.btn.btn-primary.csv-data:hover,a.initial.btn.btn-primary.csv-data:hover{
        cursor: pointer !important;
    }

    table.tablesorter thead tr .header {
        position: relative;
    }

    table.tablesorter thead tr .header:after {
        background-image: url(../torin/public/img/bg.png);
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
        display: block;
        height: 10px;
        width: 18px;
        content: '';
        position: absolute;
        right: -12px;
        top: 18px;
        /*top: 50%;*/
        /*transform: translateY(-50%);*/
        z-index: 1;
    }
    table.tablesorter thead tr .headerSortUp:after {
        background-image: url(../torin/public/img/trans-asc.png) !important;
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }
    table.tablesorter thead tr .headerSortDown:after {
        background-image: url(../torin/public/img/trans-desc.png) !important;
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }
    table.tablesorter thead tr .header:last-child:after {
        right: 0px;
    }
     table.tablesorter thead tr .header:first-child:after {
        right: -5px;
     }

     table.tablesorter.customer-tbl thead tr .header:first-child:after  {
        right: 0px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(2):after  {
        right: -8px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(3):after  {
        right: -4px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(4):after  {
        right: 0px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(5):after  {
        right: -11px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(6):after  {
        right: 10px;
    }
     table.tablesorter.customer-tbl thead tr .header:nth-child(7):after  {
        right: 3px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(8):after  {
        right: -6px;
     }
     table.tablesorter.customer-tbl thead tr .header:last-child:after  {
       right: 3px;
     }

     /* ADMIN */

     table.tablesorter.admin-tbl thead tr .header:first-child:after  {
        right: -5px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(2):after  {
        right: -10px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(3):after  {
        right: -9px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(4):after  {
        right: -5px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(5):after  {
        right: -4px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(6):after  {
        right: -14px;
    }
     table.tablesorter.admin-tbl thead tr .header:nth-child(7):after  {
        right: 4px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(8):after  {
        right: -3px;
     }
      table.tablesorter.admin-tbl thead tr .header:nth-child(9):after  {
        right: -10px;
     }
     table.tablesorter.admin-tbl thead tr .header:last-child:after  {
       right: -2px;
     }

    /** search input box **/
    .reset-btn{
        width: 8%;
        height:31px;
        float: right;
        display: inline-block;
        margin-right: 36px;
    }
    #search-input {
        height:31px;
        display: inline-block;
    }
    .search_wrapper .form-group{
        width: 350px;
    }
    span.glyphicon.glyphicon-search.form-control-feedback{
        right: 2px;
    }
    .filter-reset-btn{
        margin-left: 7px;
        display: block;
        float: left;
        margin-bottom: 5px;
        line-height: 34px;
        text-align: center;
    }
    .filter-reset-btn a{
        font-size: 12px;
    }
    .filter-reset-btn a:hover{
        color:#000;
        text-decoration: underline;
    }
    #purchased input{
        border-radius: 0px;
        padding: 5px;
        height: 25px;
    }
    #purchased .row{
        margin-bottom: 5px;
    }

    #purchased .btn-primary{
        border-radius: 0;
        min-width: 108px;
        padding: 3px;
        border: 0;
        background-color: #000;
        margin-top: 15px;
        margin-bottom: 15px;
        color: #fff;
        text-transform: uppercase !important;
        padding: 9px 20px !important;
        font-size: 15px;
        font-weight: bold;
        letter-spacing: -0.5px !important;
    }

    .po-error, .so-error{
        /*text-align: center;*/
        margin-top: 4px;
        font-size: 11px !important;
    }
    span#purchase-error{
        color: #FF0004;
    }
    .torin-calc .purchased {
        background-color: #00A82D;
    }
    a.purchased.btn.btn-primary.csv-data:hover{
        cursor: default !important;
    }
    .torin-calc .purchased:hover,
    .toric-calc .purchased:focus {
        text-decoration: none !important;
        background-color: #00A82D;
    }
    .custom {
        background-color: #fb6d29;
    }

    #csv-table-form .table-scrollbar{
        background-color:#fff;
        overflow-y: hidden;
    }
    table#csvtbl{
        width: auto !important;
    }

    #style-3::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 1px rgba(0,0.3,0,0);
        background-color: #F5F5F5;
    }

    #style-3::-webkit-scrollbar
    {
        width: 15px;
        background-color: #F5F5F5;
        height: 15px;
    }
    #style-3::-webkit-scrollbar-thumb{
        background-color: #000;
    }


.extra-actions {

	display: flex;
    align-items: center;
    /* margin: auto; */
    font-size: 10px;
}
.extra-actions > button {
margin:5px;
}

.dashboard_content .table-wrapper table tr.row-details {
    text-decoration: none;
}
.dashboard_content .table-wrapper table tr.row-details {
    text-transform: uppercase;
    text-decoration: underline;
    border-top: 1px solid #f5f5f5 !important;
}
.dashboard_content .table-wrapper table tr {
    background: #e4e4e4 !important;
    border-bottom:1px solid #ffffff !important;
}
.form-group {
    margin-bottom :5px;
}

#update_cart {
    background-color: #ff812b !important;
    padding: 10px;
    border: #ff812b 1.5px solid;
    color: #ffff;
    text-transform: uppercase;
    font-style: bold;
    font-weight: bold;
    background: transparent;
}
.coupon button {
    background-color: #00a72e !important;
    padding: 10px;
    border: #00a72e 1.5px solid;
    color: #ffff;
    text-transform: uppercase;
    font-style: bold;
    font-weight: bold;
    background: transparent;
}

.dashboard_content .table-wrapper table tr:nth-child(odd) {
	background-color: #fff!important;
	}
	section.content h2 {
    font-size: 16px;
    margin-bottom: 15px;
    font-weight: bold;
    color: #fff;
    text-transform: uppercase;
    background-color: #333333;
    text-align: center;
    padding: 10px;
}
.shop_table tr td {

	font-size : 14px;
}
.cart-subtotal td {
	font-size : 14px;
}
 table > tbody > tr:first-child td {
    font-size: 14px !important;
    font-weight: bold;
}
.shop_table tr td {
	background-color:#e4e4e4 !important;
}
.calculated_shipping {
	background-color: #e4e4e4 !important;

}
.wc-proceed-to-checkout {
	margin-bottom :0px;
	padding-bottom:0px;
	padding: 0px!important;
}
.wc-proceed-to-checkout > a {
	margin-bottom:0px !important;
}
</style>

 <style>
        @keyframes  add_border {
            0% {
                background-color: none;
                color: #000000;
            }
            50% {
                background-color: #ffd700;
                color: #FFFFFF;
            }
            100% {
                background-color: none;
                color: #000000;
            }
        }
        .purchase-step .input-wrap { padding: 2px 10px; }
        .purchase-step input[type="checkbox"] { width: 17px; height: 17px; }
        .status-button-wrap { white-space:nowrap; }
        .status-button-wrap li { list-style:none; padding-left:10px; display:inline-block; }
        .status-button-wrap li a { cursor:pointer; }
        #pdf-preview { width: 100%; height: 400px; height: calc(100vh - 250px); max-height: 687px; }
        #checkOutModal .modal-dialog { width: 940px; max-width: calc(100% - 20px) }
        .esign-wrap .btn { display: block; width: 548px; margin: -4px 0 0; border-radius: 0px; }
        #cust-table-form .btn-clickable { cursor: pointer; pointer-events: visible; }
        #cust-table-form .btn-clickable span {
            display: inline-block;
            background: #459e45;
            padding: 2px 7px;
            width: 29px;
            margin: -7px 2px -7px -8px;
            font-size: 17px;
            line-height: 26px;
        }
        .tdm_table { width: 100%; }
        .tdm_table th, .tdm_table td { padding: 6px 10px; }
        .tdm_include { width: 32px; text-align: center; line-height: 18px; }
        .tdm_include input { width: 17px; height: 17px; }
        .tdm_name { width: 260px; }
        .tdm_qty { width: 52px; text-align: center; }
        .tdm_subhead { font-weight: bold; }
        .tdm_subhead .tdm_item { text-align: right; }
        .tdm_price { width: 140px; }
        td.tdm_price { text-align: right; }
        .tdm_price i { font-style: normal; float: left; }
        .row.row-xs { margin: 0 -2px; }
        .row-xs [class^="col-"] { padding-left: 2px; padding-right: 2px; }
        .row .mb4 { padding-bottom: 4px; }

        #checkOutModal .modal-header {
    background: #333;
    padding: 14px 20px;
}
.modal-header {
    background: #CCC;
    color: #fff;
    font-size: 19px;
    padding: 24px 20px;
    text-shadow: 1px 1px 3px #000;
    text-align: center;
}
.wc_payment_methods {

    display:none;
}
.place-order {
    display:none;
}
#payment {

    display:none;
}


.shop_table tr td {
    background-color: transparent !important;

}


.shop_table tr .product-total {
  text-align: right !important;

}

.shop_table tfoot tr > td {
		text-align: right !important;
}
</style>
<link href="https://torindriveintl.com/torin/public/css/bootstrap-datepicker3.css" rel="stylesheet">
<section class="header-image-new">
  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">

		<div class="bg-side"></div>
		<div class="row">

		  <div class="col-md-3 sidebar_wrap welcome-left">
			<?php
				get_sidebar('menu');
			?>
		  </div>
		  <div class="col-md-9 content_wrap welcome-right">
          <?php 
                            if( !is_user_logged_in() ):
                        ?>
                        <div class="row">
                                <div class="col-md-12">
                                <div class="alert alert-danger">You are on a guest account, please login if you want to use your customer account.</div>
                                </div>
                        </div>
                    <?php endif; ?>

						<a href="/spare-parts" class="btn btn-success" style="background:#00a72e !important;color:#ffffff;"><i class="fa fa-arrow-left"> </i> Continue Shopping</a>
							<hr>

					<?php do_action( 'woocommerce_before_cart' ); ?>

<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>
	<div class="dashboard_content">
                    <div class="js-wrap table-wrapper cust-scrollbar" id="style-2">

	<table class="table tablesorter customer-tbl">
                    <thead class="thead-inverse">

			<tr>

				<th class="product-name" style="width:10%"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
				<th class="product-price" style="width:10%"><?php esc_html_e( 'Price', 'woocommerce' ); ?></th>
				<th class="product-quantity" style="width:10%"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></th>
				<th class="product-subtotal" style="text-align:right;"><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?>&nbsp;&nbsp;</th>
				<th class="product-remove" style="width:10%">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
					<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">



						<td class="product-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>" style="width:10%;text-align:center;">
						<a href="<?=get_permalink( $_product->get_id() );?>" style="color:#333;text-decoration:underline;">	<?php echo $_product->get_name(); ?></a>
						<?php


						do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

						// Meta data.
						echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

						// Backorder notification.
						if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
						}
						?>
						</td>

						<td class="product-price" data-title="<?php esc_attr_e( 'Price', 'woocommerce' ); ?>" style="width:10%;text-align:center;">
							<?php
								global $wpdb;
								$user_id = get_current_user_id();
								$key = 'price_level';
								$single = true;
								$price_level = get_user_meta( $user_id, $key, $single );
								$name = $cart_item['data']->name;
								$new_price = $wpdb->get_row("SELECT * FROM wp_pricing_table WHERE machines = '$name'") ;
        						$price = 0;
								/*if($price_level == 'b4' || $price_level=='B4 Price') {
									$price = $new_price->b4_price;
								}
								else */if($price_level=='otis' || $price_level =='Otis' || $price_level == 'Otis Price') {
									$price = $new_price->otis_price;
								}
								else {
										$price = $new_price->general_price;
								}
									echo  number_format($price,2) . ' US Dollars';
							?>

						</td>

						<td class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'woocommerce' ); ?>" style="width:10%;text-align:center;">
						<?php
						if ( $_product->is_sold_individually() ) {
							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
						} else {
							$product_quantity = woocommerce_quantity_input(
								array(
									'input_name'   => "cart[{$cart_item_key}][qty]",
									'input_value'  => $cart_item['quantity'],
									'max_value'    => $_product->get_max_purchase_quantity(),
									'min_value'    => '0',
									'product_name' => $_product->get_name(),
								),
								$_product,
								false
							);
						}

						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
						?>
						</td>

						<td class="product-subtotal" data-title="<?php esc_attr_e( 'Subtotal', 'woocommerce' ); ?>" style="text-align:right;">
							<?php
									echo  number_format($cart_item['quantity'] * $price,2) . ' US Dollars';
										//echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
							?>
						</td>
						<td class="product-remove" style="width:10%;">
							<?php
								echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
									'woocommerce_cart_item_remove_link',
									sprintf(
										'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s" style="color:#000;text-decoration:underline;">REMOVE</a>',
										esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
										esc_html__( 'Remove this item', 'woocommerce' ),
										esc_attr( $product_id ),
										esc_attr( $_product->get_sku() )
									),
									$cart_item_key
								);
							?>
						</td>
					</tr>
					<?php
				}
			}
			?>


		</tbody>
	</table>
			<div class="extra-actions">
			<?php do_action( 'woocommerce_cart_contents' ); ?>




<?php if ( wc_coupons_enabled() ) { ?>
	<div class="coupon">
		<input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Service Code', 'woocommerce' ); ?>" /> <button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>" id="apply_coupon;"><?php esc_attr_e( 'Apply Code', 'woocommerce' ); ?></button>
		<?php do_action( 'woocommerce_cart_coupon' ); ?>
	</div>
<?php } ?>

<button type="submit" class="" id="update_cart" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

<?php do_action( 'woocommerce_cart_actions' ); ?>

<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>



<?php do_action( 'woocommerce_after_cart_contents' ); ?>
			</div>
	</div>
	</div>
	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>

<?php do_action( 'woocommerce_before_cart_collaterals' ); ?>

<div class="cart-collaterals">
	<?php
		/**
		 * Cart collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
		do_action( 'woocommerce_cart_collaterals' );
	?>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>

		  </div>
		 			</div>

  </div>

<?php

$checkout = WC()->checkout();
 ?>
</section>
<div class="modal fade js-purchase-wrap in" id="checkOutModal" role="dialog" style="padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">CHECKOUT - STEP <span class="js-step">1</span>/4</h4>
            </div>
            <div class="modal-body">
                <form class="form-search form-horizontal" role="form" action="#" method="POST" id="purchase-form" enctype="multipart/form-data">

                    <div class="container">
                        <div class="row">
                            <div class="purchase-step" data-step="1" >
                                <small class="purchase-note shown"> Fields with <span class="required-indicator">*</span> is required to be filled.</small>
                            </div>
                            <div class="purchase-step space-2 shown" id="step1">
                            <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> E-Signature:</div>
                                    <div class="col-md-9 esign-wrap">
                                        <input type="hidden" name="esign" class="js-purchase-esign required">
                                        <input type="hidden" name="esignature">
                                        <iframe src="/esign/" id="esign"></iframe>
                                        <a href="javascript:document.getElementById('esign').contentDocument.location.reload(true);" class="btn btn-danger btn-sm" id="clearSign">CLEAR</a>
                                    </div>
                                
                            </div>
                            <div class="purchase-step space-2 " data-step="4" id="step2">
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Delivery Date:</div>
                                    <div class="col-md-9"><input type="text" name="delivery_date" class="form-control required datepicker"   autocomplete="off" required>

																					<span class="text-danger" id="dev"></span>
																					</div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Ship To:</div>
                                    <div class="col-md-9">
                                        <div class="row row-xs">
                                            <div class="col-md-8 mb4"><input type="text" name="shipping_address" placeholder="* Address" class="form-control required" maxlength="60"  required></div>
                                            <div class="col-md-4 mb4"><input type="text" name="shipping_city" placeholder="* City" class="form-control required"  required></div>
                                            <div class="col-md-4"><input type="text" name="shipping_state" placeholder="* State/Province" class="form-control required"  required></div>
                                            <div class="col-md-4"><input type="text" name="shipping_zip" placeholder="* Zip" class="form-control required"  required></div>
                                            <div class="col-md-4"><input type="text" name="shipping_country" placeholder="* Country" class="form-control required"  required></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Bill To :</div>
                                    <div class="col-md-9">
                                        <div class="row row-xs">
                                            <div class="col-md-8 mb4"><input type="text" name="billing_address" placeholder="* Address" class="form-control required" maxlength="60"  required></div>
                                            <div class="col-md-4 mb4"><input type="text" name="billing_city" placeholder="* City" class="form-control required"  required></div>
                                            <div class="col-md-4"><input type="text" name="billing_state" placeholder="* State/Province" class="form-control required"  required></div>
                                            <div class="col-md-4"><input type="text" name="billing_zip" placeholder="* Zip" class="form-control required"  required></div>
                                            <div class="col-md-4"><input type="text" name="billing_country" placeholder="* Country" class="form-control required"  required></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Shipping Type:</div>
                                    <div class="col-md-9">
                                        <select name="shipping_type" class="form-control required" required>
                                            <option >Customer Account</option>
                                            <option >Customer Pickup</option>
                                            <option data-stype="tdi">TDI Prepay - 24Hr Lift Gate</option>
                                            <option data-stype="tdi">TDI Prepay - Regular</option>
                                            <option data-stype="tdi">TDI Pre - Express ON</option>
                                            <option data-stype="tdi">TDI Pre - Fed Ground</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Customer Carrier:</div>
                                    <div class="col-md-9"><input type="text" name="customer_carrier" class="form-control required js-sci"  ></div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Cus. Shipping Acct#:</div>
                                    <div class="col-md-9"><input type="text" name="customer_shipping_account" class="form-control required js-sci" ></div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Shipping Contact Name:</div>
                                    <div class="col-md-9"><input type="text" name="shipping_contact_name" class="form-control required js-xsci"  required></div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Shipping Contact Phone:</div>
                                    <div class="col-md-9"><input type="text" name="shipping_contact_phone" id="shipphone" class="form-control required js-xsci"  maxlength="14"  required></div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Shipping Contact Email:</div>
                                    <div class="col-md-9"><input type="text" name="shipping_contact_email" class="form-control required js-xsci"  required></div>
                                </div>
                            </div>
                            <div class="purchase-step" id="step3">
                                <div class="js-pt">
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right">Payment Type</div>
                                        <div class="col-md-8">
                                            <select name="payment_type" id="payment_type" class="form-control required js-payment_type">
                                                <option value="po" selected="">PO</option>
                                                <option value="creditcard">Credit Card</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="js-pt-po" >
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> PO Number:</div>
                                        <div class="col-md-8"><input type="text" name="po_number" class="form-control required js-po" ></div>
                                    </div>
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> PO File <small>(PDF only)</small>:</div>
                                        <div class="col-md-8"><input type="file" name="po_file" class="form-control required js-po" accept="application/pdf"></div>
                                    </div>
                                </div>
                                <div class="js-pt-cc" style="display: none;">
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> Card Type:</div>
                                        <div class="col-md-8">
                                            <select name="card_type" class="form-control required js-card_type js-cc">
                                                <option value="visa" selected="">Visa</option>
                                                <option value="american-express">American Express</option>
                                                <option value="master-card">Master Card</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> Card Number:</div>
                                        <div class="col-md-8">
                                            <input type="text" name="card_number" id="card_number" class="form-control required js-cc" maxlength="19">
                                            <input type="text" name="wc_card_number" class="hidden ccFormatMonitor wc-credit-card-form-card-number" maxlength="19" placeholder="4###-####-####-####">
                                        </div>
                                    </div>
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> CVV:</div>
                                        <div class="col-md-2">
                                            <input type="text" name="card_cvv" id="card_cvv" class="form-control required js-cc" maxlength="3">
                                            <input type="text" name="wc_card_cvv" class="hidden wc-credit-card-form-card-cvc" maxlength="3">
                                        </div>
                                    </div>
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> Expiry Date:</div>
                                        <div class="col-md-8">
                                            <div class="row row-xs">
                                                <div class="col-md-6">
                                                    <select name="card_expiry_month" class="form-control required js-cc">
                                                                                                                    <option value="01">January</option>
                                                                                                                    <option value="02">February</option>
                                                                                                                    <option value="03">March</option>
                                                                                                                    <option value="04">April</option>
                                                                                                                    <option value="05">May</option>
                                                                                                                    <option value="06">June</option>
                                                                                                                    <option value="07">July</option>
                                                                                                                    <option value="08">August</option>
                                                                                                                    <option value="09">September</option>
                                                                                                                    <option value="10">October</option>
                                                                                                                    <option value="11">November</option>
                                                                                                                    <option value="12">December</option>
                                                                                                            </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <select name="card_expiry_year" class="form-control required js-cc">
                                                                                                                    <option>2020</option>
                                                                                                                    <option>2021</option>
                                                                                                                    <option>2022</option>
                                                                                                                    <option>2023</option>
                                                                                                                    <option>2024</option>
                                                                                                                    <option>2025</option>
                                                                                                                    <option>2026</option>
                                                                                                                    <option>2027</option>
                                                                                                                    <option>2028</option>
                                                                                                                    <option>2029</option>
                                                                                                                    <option>2030</option>
                                                                                                                    <option>2031</option>
                                                                                                                    <option>2032</option>
                                                                                                                    <option>2033</option>
                                                                                                                    <option>2034</option>
                                                                                                                    <option>2035</option>
                                                                                                                    <option>2036</option>
                                                                                                                    <option>2037</option>
                                                                                                                    <option>2038</option>
                                                                                                                    <option>2039</option>
                                                                                                            </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
																		<div class="row input-wrap">
																				<div class="col-md-4 plabel text-right"><span class="required-indicator">*</span>  Reference No. :</div>
																				<div class="col-md-8"><input type="text" name="customer_reference_no" id="referrence_no" class="form-control required" ></div>
																		</div>
                                    <div style="padding: 10px 10px 0px;">
                                        <small>Note: there is 3.4% fee if you choose Credit Card.</small>
                                    </div>
                                </div>
                            </div>
                            <div class="purchase-step"  id="step4">
                                    <div class="order-wrapper">
                                                <p class="order-title">YOUR ORDERS</p>
                                            <?php do_action( 'woocommerce_checkout_order_review' ); ?>
                                    </div>
                            </div>
                        </div>
                    <input type="hidden" name="step" value="1">

		 	 <input type="hidden" name="customer_id" value="<?=$user_id?>">
			  <input type="hidden" name="customer_name">
			 <input type="hidden" name="customer_code">
			 <input type="hidden" name="customer_phone">
			 <input type="hidden" name="customer_email">
		<input type="hidden" name="cc_fees" value="0" >
		<input type="hidden" name="manager_email" value="<?=the_field('manager_email', 'options')?>">
		<?php	global $woocommerce;
		$count = $woocommerce->cart->cart_contents_count;
		 ?>


					<input type="hidden" name="cart_found_total" value="<?=$count?>" >


                    </div>
                </form>
                <div style="padding: 10px 10px 0px; 10px;">
                                      &nbsp;  <small class="error" style="color:red;"></small>
                                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" type="button" data-dismiss="modal" style="float:left;">Cancel</button>
                <button class="btn btn-default purchase-button js-purchase-prev" type="button">Back</button>
                <button class="btn btn-primary purchase-button js-purchase-step shown" type="button">Next</button>
                <button class="btn btn-primary purchase-button js-purchase-confirm" type="button">Confirm</button>

            </div>
        </div>
    </div>
</div>
<div id="purchaseSuccess" class="modal fade">
    <div class="modal-dialog modal-confirm modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box"><i class="fa fa-check"></i></div>
            </div>
            <div class="modal-body text-center">
                <h4>Great!</h4>
                <p>Your Order has been received.</p>
                <a class="btn btn-success"  href="https://torindriveintl.com/spare-parts/?clear-cart"><span>OK</a>
            </div>
        </div>
    </div>
</div>


<div id="checkoutFailed" class="modal fade">
    <div class="modal-dialog modal-confirm modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box text-danger"><i class="fa fa-exclamation-triangle"></i></div>
            </div>
            <div class="modal-body text-center">
                <h4>Oops!</h4>
                <p>Your cart is empty. Cannot proceed to checkout.</p>
                <a class="btn btn-danger"  href="https://torindriveintl.com/spare-parts/"><span>OK</a>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php echo site_url(); ?>/torin/public/js/jquery.mask.min.js?v=3"></script>
<script> var $j3 = jQuery.noConflict(); </script>
<?php get_footer(); ?>
<script type='text/javascript' src='https://torindriveintl.com/wp-content/themes/torindriveintl/js/bootstrap.min.js'></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="https://github.com/lopezton/jquery-creditcard-formatter/blob/master/ccFormat.js"></script>
<script src="https://torindriveintl.com/torin/public/js/bootstrap-datepicker.min.js"></script>
<!-- <script type="text/javascript">

$(function() {
	$('.woocommerce-Price-currencySymbol').remove();

	$('.woocommerce-Price-amount').append('<span class="woocommerce-Price-currencySymbol">   US Dollars</span>');
});

    var step = 1 ;
    $('.js-step').html(step);
    $('.js-purchase-step').click(function(e) {
        var formData =  $('#purchase-form').serialize();
        $('.error').html('') ;
            console.log(step);
            $(this).html('<span><i class="fa fa-spin fa-spinner"></i></span>') ;
        if(step == 1) {
            $('[name=step]').val(step);
                                                            $('#step2').hide();
		                                            	    $('.js-purchase-prev').hide();
			                                                 $('#step1').show();
			                                                $('#step3').hide();
                                                             $('.js-purchase-confirm').hide();
                                                            $('.js-purchase-step').show();




            $.ajax({

	   						type: "post",

	    									    url : "https://torindriveintl.com/spareparts/api/check-data",
	             		 						data : formData,
	    										success: function(response){
	        									console.log(response);

                                                    if(response.success ==true) {
                                                        step =2 ;
                                                        $('[name=step]').val(step);
                                                        $('.js-step').html(step);
                                                        $('#step2').show();
                                                        $('.js-purchase-prev').show();
                                                        $('#step1').hide();
                                                        $('#step3').hide();
                                                        $('.js-purchase-confirm').hide();
                                                        $('.js-purchase-step').show();

                                                    }
                                                    else {
                                                        $('.error').html(response) ;
                                                    }

                                                    $('.js-purchase-step').html('<span>NEXT</span>') ;
												}
								});


        }
        else if(step == 2) {

            var formData =  new FormData( $('#purchase-form')[0]);
            $.ajax({
                type: "post",
                 url : "https://torindriveintl.com/spareparts/api/check-data",
                   data : formData,

                    enctype: 'multipart/form-data',
                    processData: false,
                    cache: false,
                     contentType: false,
                     success: function(response){
                        console.log(response);
                             if(response.success ==true) {
                                                        step =3 ;
                                                        $('.js-step').html(step);
                                                        $('[name-step]').val(step);
			                                            $('#step2').hide();
                                                         $('.js-purchase-prev').show();
			                                            $('.js-purchase-step').hide();
			                                            $('#step1').hide();
			                                            $('#step3').show();
																									$('[name=cc_fees]').val(response.cc_fee);
																												console.log(	$('[name=cc_fees]').val());
																											var tt = $('[name=total_order]').val();
																											var fees = $('[name=cc_fees]').val();
																											var ff = tt * (fees /100);
																											var final_total = parseFloat(tt) + parseFloat(ff);
																												$('#new_total').html( '<span> ' + parseFloat(final_total).toFixed(2).toLocaleString()  + ' US Dollars</span>');
																												var html_fee = '<tr><th style="text-align:right;padding-right:10px;">CC Fee</th><td>'+parseFloat(ff).toFixed(2) +' US Dollars</td></tr>';
																												$('.cart-subtotal').after(html_fee);
																												$('.js-purchase-confirm').show();
																												$('.js-purchase-step').html('<span>NEXT</span>') ;
                                                    }
                                                    else {
                                                        $('.error').html(response) ;
                                                    }
                                                    $('.js-purchase-step').html('<span>NEXT</span>') ;
							}


					});

        }
        else if(step == 3 ){
            $('[name-step]').val(step);
						$('#step2').hide();
			      $('.js-purchase-prev').show();
						$('.js-purchase-step').hide();
						$('#step1').hide();
						$('#step3').show();
            $('.js-purchase-confirm').show();
        }
        $('.js-step').html(step);
    }) ;

    $('.js-purchase-confirm').click(function(){

        var formDatas =  new FormData( $('#purchase-form')[0]);
        $(this).html('<span><i class="fa fa-spin fa-spinner"></i></span>') ;
        $.ajax({
                type: "post",
                url : "https://torindriveintl.com/spareparts/api/checkout",
                 data : formDatas,
                enctype: 'multipart/form-data',
                processData: false,
                cache: false,
                contentType: false,
                success: function(response){
                     if(response.success ==true) {
                        $('#purchaseSuccess').modal('show')
                        $('#checkOutModal').modal('hide')  ;
                        step =1  ;
                        $('[name-step]').val(step);
                     }
                     else {
                        $('.error').html(response) ;
                    }
                     $('.js-purchase-confirm').html('<span>CONFIRM</span>') ;
            },
            error: function(response){
                var parent = $(response.responseText);
                $('.error').html('Server Error, please report to your Project Manager.<br><small>'+$('.trace-message', parent).text()+'</small>');
                $('.js-purchase-confirm').html('<span>CONFIRM</span>') ;
            }
 });

    });
    $('.js-purchase-prev').click(function() {
            step -- ;
            $('[name=step]').val(step);
            console.log(step);
        if(step == 1) {
			$('#step2').hide();
			$('.js-purchase-prev').hide();
			$('#step1').show();
			$('#step3').hide();
            $('.js-purchase-confirm').hide();
            $('.js-purchase-step').show();

        }
        else if(step == 2) {
            $('[name-step]').val(step);
			$('#step2').show();
			$('.js-purchase-prev').show();
			$('#step1').hide();
			$('#step3').hide();
            $('.js-purchase-confirm').hide();
            $('.js-purchase-step').show();
        }
        else if(step == 3 ){
            $('[name-step]').val(step);
			$('#step2').hide();
			$('.js-purchase-prev').show();
			$('#step1').hide();
			$('#step3').show();
            $('.js-purchase-confirm').show();
            $('.js-purchase-step').hide();

        }
        $('.js-step').html(step);

    }) ;
    $('#payment_type').on('change',function () {
        var pay_type = $(this).val();
            if(pay_type == 'po') {
                $('.js-pt-cc').hide();
                $('.js-pt-po').show();
            }
            else if(pay_type =='creditcard') {
                $('.js-pt-cc').show();
                $('.js-pt-po').hide();
            }
    });

	$(function() {

					$('.checkout-button').attr('href','#');
					$('.checkout-button').attr('data-toggle','modal');
					$('.checkout-button').attr('data-target','#checkOutModal');


                        var carrier = "" ;
                        var account_no = "" ;
					$('.checkout-button').click(function() {

									$.ajax({
	   										type: "post",
	    			 							dataType: "json",
	    										url : "<?php echo admin_url('admin-ajax.php'); ?>",
	             		 						data : {action: "get_all_user_meta" },
	    										success: function(response){
	        									console.log(response);

                                                    $('[name=billing_address]').val(response.billing_address);
                                                    $('[name=billing_city]').val(response.billing_city);
                                                    $('[name=billing_state]').val(response.billing_state[0] ? response.billing_state : 'Tennessee');
                                                    $('[name=billing_country]').val(response.billing_country[0] ? response.billing_country : 'USA');
                                                    $('[name=billing_zip]').val(response.billing_zip[0] ? response.billing_zip : response.profile_zip);

                                                    $('[name=shipping_address]').val(response.shipping_address);
                                                    $('[name=shipping_city]').val(response.shipping_city);
                                                    $('[name=shipping_state]').val(response.shipping_state[0] ? response.shipping_state : 'Tennessee');
                                                    $('[name=shipping_country]').val(response.shipping_country[0] ? response.shipping_country : 'USA');
                                                    $('[name=shipping_zip]').val(response.shipping_zip[0] ? response.shipping_zip : response.profile_zip);

                                                    $('[name=shipping_contact_name]').val(response.shipping_contact_name);
                                                    $('[name=shipping_contact_phone]').val(response.shipping_contact_phone);
                                                    $('[name=shipping_contact_email]').val(response.shipping_contact_email);
                                                    formatPhone('shipphone');
                                                    account_no = response.customer_shipping_account;
                                                    carrier = response.customer_carrier;
                                                    $('[name=customer_carrier]').val(carrier);
                                                    $('[name=customer_shipping_account]').val(account_no);

						 $('[name=customer_name]').val(response.first_name + ' ' + response.last_name);
						 $('[name=customer_code]').val(response.customer_code);
						 $('[name=customer_phone]').val(response.contact_phone_number);
						 $('[name=customer_email]').val(response.user_email);
                                                                    if(response.cc_only == 1) {

																																									$('#payment_type').val('creditcard');
																																									$('#payment_type option:selected').val('creditcard');
																																									$('#payment_type').attr('readonly',true);
																																									$('#payment_type').find('[value="po"]').remove();
																																									$('.js-pt-cc').show();
																																									$('.js-pt-po').hide();
                                                                        $('[data-stype="tdi"]').remove();
                                                                    }

												}
											});

					});

                    $('[name=shipping_type]').on('change', function(){
                            var ship_type  = $(this).val();
                            console.log(ship_type);
                            if(ship_type !='Customer Account') {
                                $('[name=customer_carrier]').attr('disabled',true);
                                $('[name=customer_shipping_account]').attr('disabled',true);
                                $('[name=customer_carrier]').val('');
                                $('[name=customer_shipping_account]').val('');
                            }
                            else {
                                $('[name=customer_carrier]').attr('disabled',false);
                                $('[name=customer_shipping_account]').attr('disabled',false);
                                $('[name=customer_carrier]').val(carrier);
                                $('[name=customer_shipping_account]').val(account_no);
                            }
                    });


	});

	$(function() {
	$('[name=delivery_date]').on('change',function() {
			var fullDate = new Date();
			var twoDigitMonth = ((fullDate.getMonth().length+1) === 1) ? (fullDate.getMonth()+1) : '' + (fullDate.getMonth()+1);

			var present_date =  new Date(twoDigitMonth + "/" + fullDate.getDate() + "/" + fullDate.getFullYear());
			var nextDate = new Date($(this).val()) ;
			console.log(present_date);
				console.log(nextDate);
				var diff =  nextDate.getTime() - present_date.getTime();
				var Difference_In_Days = diff / (1000 * 3600 * 24);
				if(Difference_In_Days < 7) {
								$('#dev').html('<label>Requested lead times shorter than one week may be delayed. Please contact TDI for more information.</label>') ;
				}
				else {
						$('#dev').html('') ;
}
	});

	$('.qty').attr('max',100);
var current_total = 	$('[name=total_order]').val();
		$('.woocommerce-remove-coupon').click(function() {
				console.log('ok');
					var coupon_total = $('[name=coupon_amount]').val();
					var total_order = $('[name=total_order]').val();
					 var new_total =   $('[name=sub_total_order]').val();
				 	$('[name=total_order]').val(new_total);
					current_total =  	$('[name=total_order]').val();
						$('[name=coupon_amount]').val(0);
					$('.cart-discount').remove();
					// $('#new_total').html( '<span>$ ' + current_total.toFixed(2) + '</span>');
					var tt = $('[name=total_order]').val();
					var fees = $('[name=cc_fees]').val();
					var ff = tt * (fees /100);
					var final_total = parseFloat(current_total) + parseFloat(ff);
						$('#new_total').html( '<span>$ ' + parseFloat(final_total).toFixed(2)  + ' ( <i> $ ' +  parseFloat(ff).toFixed(2) + ' cc fee<i>)</span>');

		});
				// $('#new_total').html( '<span>$ ' +current_total.toFixed(2) + '</span>');
})

$(function() {
var cart_count = $('[name=cart_found_total]').val();

if(cart_count == 0 ) {
	$('.checkout-button ').attr('data-target','#checkoutFailed');
}

});


$(function() {

	$.ajax({
		type: "post",
dataType: "json",
url : "<?php echo admin_url('admin-ajax.php'); ?>",
	data : {action: "check_backorder" },
success: function(response){
		console.log(response);
		if(response.back_order == true) {
			 var tt = new Date();
			var date = new Date(tt);
 			var newdate = new Date(date);

 			newdate.setDate(newdate.getDate() + 83);

 			var dd = newdate.getDate();
 			var mm = newdate.getMonth() + 1;
 		var y = newdate.getFullYear();

 				var someFormattedDate = mm + '/' + dd + '/' + y;
					console.log(someFormattedDate );
					var now = new Date();
			now.setDate(now.getDate()+84);
		console.log(now)
			 $('.datepicker').datepicker({ startDate: now });
            // $('#dev').html('<label style="padding-top:10px;">Some or all of your order is on backorder. The soonest available lead time is 12 weeks. Please contact the parts manager for more details.</label>')
			$('#dev').html('<label style="padding-top:10px;">Purchased quantity exceeds stock availability, please allow 12 weeks for delivery.</label>');
   }
		if(response.back_order == false) {
			 $('.datepicker').datepicker({ startDate: "today" });
		}


	}
});
});
</script> -->


<script type="text/javascript">

$(function() {
	$('.woocommerce-Price-currencySymbol').remove();

	$('.woocommerce-Price-amount').append('<span class="woocommerce-Price-currencySymbol">   US Dollars</span>');
});

    var step = 1 ;
    $('.js-step').html(step);
    $('.js-purchase-step').click(function(e) {


        

      
        $('.error').html('') ;
            console.log(step);
            $(this).html('<span><i class="fa fa-spin fa-spinner"></i></span>') ;
     


        var esign = $("#esign").contents().find(".pad")[0].toDataURL();
                    console.log($('.js-purchase-esign').val(esign));
                   
            var empty_sign =  $("#esign").contents().find('input[name="output-3"]').val() ;
            $('[name=esignature]').val(empty_sign);
            console.log(empty_sign);

            if(empty_sign == '' || empty_sign == ' ') {
                console.log('agay');
                $('[name=esignature]').val('empty');
            }

            var formData =  $('#purchase-form').serialize();
        if(step == 1) {
            $('#step1').show();
		                                            	    $('.js-purchase-prev').hide();
			                                                 $('#step2').hide();
			                                                $('#step3').hide();
                                                            $('#step4').hide();
                                                             $('.js-purchase-confirm').hide();
                                                            $('.js-purchase-step').show();
                    var esign = $("#esign").contents().find(".pad")[0].toDataURL();
                    console.log($('.js-purchase-esign').val(esign));

                    $.ajax({ type: "post",

                 url : "https://torindriveintl.com/spareparts/api/check-data",
                   data : formData,
                 success: function(response){
                 console.log(response);

                     if(response.success ==true) {
                         step =2 ;
                         $('[name=step]').val(step);
                         $('.js-step').html(step);
                         $('#step2').show();
                         $('.js-purchase-prev').show();
                         $('#step1').hide();
                         $('#step2').show();
                         $('#step3').hide();
                         $('#step4').hide();
                         $('.js-purchase-confirm').hide();
                         $('.js-purchase-step').show();

                     }
                     else {
                         $('.error').html(response) ;
                     }

                     $('.js-purchase-step').html('<span>NEXT</span>') ;
                 }
 });

        }
        else if (step == 2) {
            $('[name=step]').val(step);
                                                            $('#step1').hide();
		                                            	    $('.js-purchase-prev').hide();
			                                                 $('#step2').show();
			                                                $('#step3').hide();
                                                            $('#step4').hide();
                                                             $('.js-purchase-confirm').hide();
                                                            $('.js-purchase-step').show();




            $.ajax({

	   						type: "post",

	    									    url : "https://torindriveintl.com/spareparts/api/check-data",
	             		 						data : formData,
	    										success: function(response){
	        									console.log(response);

                                                    if(response.success ==true) {
                                                        step =3 ;
                                                        $('[name=step]').val(step);
                                                        $('.js-step').html(step);
                                                        $('#step2').show();
                                                        $('.js-purchase-prev').show();
                                                        $('#step1').hide();
                                                        $('#step3').show();
                                                        $('#step4').hide();
                                                        $('#step2').hide();
                                                        $('.js-purchase-confirm').hide();
                                                        $('.js-purchase-step').show();

                                                    }
                                                    else {
                                                        $('.error').html(response) ;
                                                    }

                                                    $('.js-purchase-step').html('<span>NEXT</span>') ;
												}
								});


        }
        else if(step == 3) {

            var formData =  new FormData( $('#purchase-form')[0]);
            $.ajax({
                type: "post",
                 url : "https://torindriveintl.com/spareparts/api/check-data",
                   data : formData,

                    enctype: 'multipart/form-data',
                    processData: false,
                    cache: false,
                     contentType: false,
                     success: function(response){

                             if(response.success ==true) {
                                                        step =4 ;
                                                        $('.js-step').html(step);
                                                        $('[name-step]').val(step);
			                                            $('#step2').hide();
                                                         $('.js-purchase-prev').show();
			                                            $('.js-purchase-step').hide();
			                                            $('#step1').hide();
                                                        $('#step3').hide();
			                                            $('#step4').show();
																									$('[name=cc_fees]').val(response.cc_fee);

																											var tt = $('[name=total_order]').val();
																											var fees = $('[name=cc_fees]').val();
																											var ff = tt * (fees /100);
																											var final_total = parseFloat(tt) + parseFloat(ff);
																												$('#new_total').html( '<span> ' + parseFloat(final_total).toFixed(2).toLocaleString()  + ' US Dollars</span>');
																											var html_fee = '<tr><th style="text-align:right;padding-right:10px;">CC Fee</th><td>'+parseFloat(ff).toFixed(2) +' US Dollars</td></tr>';
																											$('.cart-subtotal').after(html_fee);
                                                        $('.js-purchase-confirm').show();
                                                        $('.js-purchase-step').html('<span>NEXT</span>') ;
                                                    }
                                                    else {
                                                        $('.error').html(response) ;
                                                    }
                                                    $('.js-purchase-step').html('<span>NEXT</span>') ;
							}


					});

        }
        else if(step == 4 ){
            $('[name-step]').val(step);
			$('#step2').hide();
            $('.js-purchase-prev').show();
			$('.js-purchase-step').hide();
			$('#step1').hide();
            $('#step3').hide();
			$('#step4').show();
            $('.js-purchase-confirm').show();

        }
        $('.js-step').html(step);
    }) ;

    $('.js-purchase-confirm').click(function(){

        var formDatas =  new FormData( $('#purchase-form')[0]);
        $(this).html('<span><i class="fa fa-spin fa-spinner"></i></span>') ;
        $.ajax({
                type: "post",
                url : "https://torindriveintl.com/spareparts/api/checkout",
                 data : formDatas,
                enctype: 'multipart/form-data',
                processData: false,
                cache: false,
                contentType: false,
                success: function(response){
                     if(response.success ==true) {
                        $('#purchaseSuccess').modal('show')
                        $('#checkOutModal').modal('hide')  ;
                        step =1  ;
                        $('[name-step]').val(step);
                     }
                     else {
                        $('.error').html(response) ;
                    }
                     $('.js-purchase-confirm').html('<span>CONFIRM</span>') ;
            },
            error: function(response){
                var parent = $(response.responseText);
                $('.error').html('Server Error, please report to your Project Manager.<br><small>'+$('.trace-message', parent).text()+'</small>');
                $('.js-purchase-confirm').html('<span>CONFIRM</span>') ;
            }
 });

    });
    $('.js-purchase-prev').click(function() {
            step -- ;
            $('[name=step]').val(step);
            console.log(step);
        if(step == 1) {
			$('#step2').hide();
			$('.js-purchase-prev').hide();
			$('#step1').show();
			$('#step3').hide();
            $('.js-purchase-confirm').hide();
            $('.js-purchase-step').show();

        }
        else if(step == 2) {
            $('[name-step]').val(step);
			$('#step2').show();
			$('.js-purchase-prev').show();
			$('#step1').hide();
			$('#step3').hide();
            $('.js-purchase-confirm').hide();
            $('.js-purchase-step').show();
        }
        else if(step == 3 ){
            $('[name-step]').val(step);
			$('#step2').hide();
			$('.js-purchase-prev').show();
			$('#step1').hide();
			$('#step3').show();
            $('.js-purchase-confirm').show();
            $('.js-purchase-step').hide();

        }
        $('.js-step').html(step);

    }) ;
    $('#payment_type').on('change',function () {
        var pay_type = $(this).val();
            if(pay_type == 'po') {
                $('.js-pt-cc').hide();
                $('.js-pt-po').show();
            }
            else if(pay_type =='creditcard') {
                fee = 3.4;
                $('.js-pt-cc').show();
                $('.js-pt-po').hide();
            }
    });

	$(function() {

					$('.checkout-button').attr('href','#');
					$('.checkout-button').attr('data-toggle','modal');
					$('.checkout-button').attr('data-target','#checkOutModal');


                        var carrier = "" ;
                        var account_no = "" ;
					$('.checkout-button').click(function() {

									$.ajax({
	   										type: "post",
	    			 							dataType: "json",
	    										url : "<?php echo admin_url('admin-ajax.php'); ?>",
	             		 						data : {action: "get_all_user_meta" },
	    										success: function(response){
	        									console.log(response);

	        										// $('[name=billing_address]').val(response.billing_address_1);
	        										// $('[name=billing_city]').val(response.billing_city);
	        										// $('[name=billing_state]').val(response.billing_state);
	        										// $('[name=billing_country]').val(response.billing_country);
	        										// $('[name=billing_zip]').val(response.billing_postcode);

	        										// $('[name=shipping_address]').val(response.billing_wooccm20);
	        										// $('[name=shipping_city]').val(response.billing_wooccm23);
	        										// $('[name=shipping_state]').val(response.billing_wooccm22);
	        										// $('[name=shipping_country]').val(response.billing_wooccm21);
	        										// $('[name=shipping_zip]').val(response.billing_wooccm24);

                                                    // $('[name=shipping_contact_name]').val(response.shipping_contact_name);
                                                    // $('[name=shipping_contact_phone]').val(response.shipping_contact_phone);
                                                    // $('[name=shipping_contact_email]').val(response.shipping_contact_email);

                                                    $('[name=billing_address]').val(response.billing_address);
                                                    $('[name=billing_city]').val(response.billing_city);
                                                    $('[name=billing_state]').val(response.billing_state);
                                                    $('[name=billing_country]').val(response.billing_country);
                                                    if (typeof(response.billing_zip) !== 'undefined') {
                                                        $('[name=billing_zip]').val(response.billing_zip[0] ? response.billing_zip : response.profile_zip);
                                                    }
                                                    $('[name=billing_zip]').val(response.billing_postcode);
                                                    $('[name=shipping_address]').val(response.shipping_address);
                                                    $('[name=shipping_city]').val(response.shipping_city);
                                                    $('[name=shipping_state]').val(response.shipping_state);
                                                    $('[name=shipping_country]').val(response.shipping_country);
                                                   
                                                    if (typeof(response.shipping_zip) !== 'undefined') {
                                                         $('[name=shipping_zip]').val(response.shipping_zip[0] ? response.shipping_zip : response.profile_zip);
                                                    }
                                                    $('[name=shipping_contact_name]').val(response.shipping_contact_name);
                                                    $('[name=shipping_contact_phone]').val(response.shipping_contact_phone);
                                                    $('[name=shipping_contact_email]').val(response.shipping_contact_email);
                                                    formatPhone('shipphone');
                                                    account_no = response.customer_shipping_account;
                                                    carrier = response.customer_carrier;

                                                    $('[name=customer_carrier]').val(carrier);
                                                    $('[name=customer_shipping_account]').val(account_no);

						 $('[name=customer_name]').val(response.first_name + ' ' + response.last_name);
						 $('[name=customer_code]').val(response.customer_code);
						 $('[name=customer_phone]').val(response.contact_phone_number);
						 $('[name=customer_email]').val(response.user_email);
                                                                    if(response.cc_only == 1) {

																																									$('#payment_type').val('creditcard');
																																									$('#payment_type option:selected').val('creditcard');
																																									$('#payment_type').attr('readonly',true);
                                                                                                                                                                    $('#payment_type').find('[value="po"]').remove();
                                                                            $('.js-pt-cc').show();
                                                                            $('.js-pt-po').hide();																					$('#payment_type').find('[value="po"]').remove();
																																									$('.js-pt-cc').show();
																																									$('.js-pt-po').hide();
                                                                        $('[data-stype="tdi"]').remove();
                                                                    }

												}
											});

					});

                    $('[name=shipping_type]').on('change', function(){
                            var ship_type  = $(this).val();
                            console.log(ship_type);
                            if(ship_type !='Customer Account') {
                                $('[name=customer_carrier]').attr('disabled',true);
                                $('[name=customer_shipping_account]').attr('disabled',true);
                                $('[name=customer_carrier]').val('');
                                $('[name=customer_shipping_account]').val('');
                            }
                            else {
                                $('[name=customer_carrier]').attr('disabled',false);
                                $('[name=customer_shipping_account]').attr('disabled',false);
                                $('[name=customer_carrier]').val(carrier);
                                $('[name=customer_shipping_account]').val(account_no);
                            }
                            $('[name="shipping_type_code"]').val($('option:selected',this).data('code'));
                    }).change();


	});

	$(function() {
	$('[name=delivery_date]').on('change',function() {
			var fullDate = new Date();
			var twoDigitMonth = ((fullDate.getMonth().length+1) === 1) ? (fullDate.getMonth()+1) : '' + (fullDate.getMonth()+1);

			var present_date =  new Date(twoDigitMonth + "/" + fullDate.getDate() + "/" + fullDate.getFullYear());
			var nextDate = new Date($(this).val()) ;
			console.log(present_date);
				console.log(nextDate);
				var diff =  nextDate.getTime() - present_date.getTime();
				var Difference_In_Days = diff / (1000 * 3600 * 24);
				if(Difference_In_Days < 7) {
								$('#dev').html('<label>Requested lead times shorter than one week may be delayed. Please contact TDI for more information.</label>') ;
				}
				else {
						$('#dev').html('') ;
}
	});

	$('.qty').attr('max',100);
var current_total = 	$('[name=total_order]').val();
		$('.woocommerce-remove-coupon').click(function() {
				console.log('ok');
					var coupon_total = $('[name=coupon_amount]').val();
					var total_order = $('[name=total_order]').val();
					 var new_total =   $('[name=sub_total_order]').val();
				 	$('[name=total_order]').val(new_total);
					current_total =  	$('[name=total_order]').val();
						$('[name=coupon_amount]').val(0);
					$('.cart-discount').remove();
					// $('#new_total').html( '<span>$ ' + current_total.toFixed(2) + '</span>');
					var tt = $('[name=total_order]').val();
					var fees = $('[name=cc_fees]').val();
					var ff = tt * (fees /100);
					var final_total = parseFloat(current_total) + parseFloat(ff);
						$('#new_total').html( '<span>$ ' + parseFloat(final_total).toFixed(2)  + ' ( <i> $ ' +  parseFloat(ff).toFixed(2) + ' cc fee<i>)</span>');

		});
				// $('#new_total').html( '<span>$ ' +current_total.toFixed(2) + '</span>');
})

$(function() {
var cart_count = $('[name=cart_found_total]').val();

if(cart_count == 0 ) {
	$('.checkout-button ').attr('data-target','#checkoutFailed');
}

});


$(function() {

    $('#clearSign').click(function() {
            $('[name=esignature]').val('empty');
    });

	$.ajax({
		type: "post",
dataType: "json",
url : "<?php echo admin_url('admin-ajax.php'); ?>",
	data : {action: "check_backorder" },
success: function(response){
		console.log(response);
		if(response.back_order == true) {
			 var tt = new Date();
			var date = new Date(tt);
 			var newdate = new Date(date);

 			newdate.setDate(newdate.getDate() + 83);

 			var dd = newdate.getDate();
 			var mm = newdate.getMonth() + 1;
 		var y = newdate.getFullYear();

 				var someFormattedDate = mm + '/' + dd + '/' + y;
					console.log(someFormattedDate );
					var now = new Date();
			now.setDate(now.getDate()+84);
		console.log(now)
			 $('.datepicker').datepicker({ startDate: now });
			$('#dev').html('<label style="padding-top:10px;">Purchased quantity exceeds stock availability, please allow 12 weeks for delivery.</label>')
   }
		if(response.back_order == false) {
			 $('.datepicker').datepicker({ startDate: "today" });
		}


	}
});


});
</script>


<script>
    var purchaseForm = $j3('#purchase-form'),
        masks = {
            'visa' : {
                limit: 19,
                cvv: 3,
                format: '4###-####-####-####'
            },
            'master-card' : {
                limit: 19,
                cvv: 3,
                format: '5###-####-####-####'
            },
            'american-express' : {
                limit: 17,
                cvv: 4,
                format: '3###-######-#####'
            },
            'other' : {
                limit: 19,
                cvv: '3',
                format: '####-####-####-####'
            }
        };
    function formatPhone(id){
        var phone_regex_sc10 = /\d{10}/g,
            phone_regex_sc7 = /\d{7}/g,
            cphone = $('#'+id).val();

        if ( cphone.length!=0 && !cphone.match(/\([\d]{3}\) [\d]{3}\-[\d]{4}/g) ) {
            cphone = cphone.replace(/\D/g,'');
        }
        if ( cphone.match(phone_regex_sc10) ) {
            $('#'+id).val('('+cphone.substring(0,3)+') '+cphone.substring(2,5)+'-'+cphone.substring(5,9));
        } else if ( cphone.length == 7 ) {
            $('#'+id).val('(901) '+cphone.substr(0,3)+'-'+cphone.substr(3,4));
        } else {
            // $('#'+id).val('');
        }
    }
    $j3(document).ready(function () {
        $j3('.js-card_type',purchaseForm).change(function(){
            var val = $j3(this).val();
            $j3('#card_number',purchaseForm)
                .mask(masks[val].format)
                .attr('maxlength',masks[val].limit)
                .attr('placeholder',masks[val].format)
                .val($j3('#card_number').val().substr(0,masks[val].limit))
                // .val('')
                .focus();
            $j3('#card_cvv',purchaseForm)
                .mask('####')
                .attr('maxlength',masks[val].cvv)
                .val($j3('#card_cvv').val().substr(0,masks[val].cvv));
                // .val('');
        }).change();
    });
    $(document).ready(function () {
        $('#shipphone')
        .keydown(function (e) {
            var key = e.which || e.charCode || e.keyCode || 0,
                charstr = '';
            $phone = $(this);

            // Don't let them remove the starting '('
            if ($phone.val().length === 1 && (key === 8 || key === 46)) {
                $phone.val('(');
                return false;
            }
            // Reset if they highlight and type over first char.
            else if ($phone.val().charAt(0) !== '(') {
                charstr = String.fromCharCode(e.keyCode);
                $phone.val('('+charstr.replace(/\D/g,'').charAt(0)+''); 
                if ( !isNaN(charstr) ) {
                    return false;
                }
            }

            // Auto-format- do not expose the mask as the user begins to type
            if (key !== 8 && key !== 9) {
                if ($phone.val().length === 4) {
                    $phone.val($phone.val() + ')');
                }
                if ($phone.val().length === 5) {
                    $phone.val($phone.val() + ' ');
                }           
                if ($phone.val().length === 9) {
                    $phone.val($phone.val() + '-');
                }
            }

            // Allow numeric (and tab, backspace, delete) keys only
            return (key == 8 || 
                    key == 9 ||
                    key == 46 ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105)); 
        })
        .keyup(function (e) {
            var key = e.which || e.charCode || e.keyCode || 0,
                charstr = '';
            $phone = $(this);
            
            if ($phone.val().charAt(0) !== '(') {
                $phone.val('('+$phone.val()); 
            }

            if ($phone.val().charAt(2) === '(') {
                $phone.val($phone.val().substring(0,($phone.val().length-1)));
            }
        })
        .bind('focus click', function () {
            $phone = $(this);
            
            /*if ($phone.val().length === 0) {
                $phone.val('(');
            }
            else {
                var val = $phone.val();
                $phone.val('').val(val); // Ensure cursor remains at the end
            }*/

            // var $phone = $('#fieldName');
            var fldLength= $phone.val().length;
            if ( fldLength ) {
                $phone[0].setSelectionRange(fldLength, fldLength);
            } else {
                $phone.val('(');
                $phone[0].setSelectionRange(1, 1);
            }

        })
        .blur(function () {
            $phone = $(this);
            
            if ($phone.val() === '(') {
                $phone.val('');
            } else {
                formatPhone('shipphone');
            }
        });
    });
</script>