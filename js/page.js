$(document).ready(function(){

    if ( $('.sidebar_wrap').length ) {

      var toggle_element = '<button id="hamburger-toggle" class="hamburger-toggle">' +
                  '<span class="sr-only">Toggle navigation</span>' +
                  '<span class="icon-bar"></span>' +
                  '<span class="icon-bar"></span>' +
                  '<span class="icon-bar"></span>' +
                  '</button>';

      $('.dashboard_menu').wrap('<div class="toggle_wrap"></div>');
      //$('h3.no-margin').wrap('<div class="toggle_wrap"></div>');

      function add_toggle_element(){
        $( ".toggle_wrap" ).prepend( toggle_element);
        $('#hamburger-toggle').click(function(s){
          $('.dashboard_menu').slideToggle();
        });
      }

      if( $(window)[0].innerWidth < 992 ) {
        add_toggle_element();
        $('.dashboard_menu').hide();
      } else {
        $('.dashboard_menu').show();
      }

     $(window).on('load', function() {
        var window_size = $(this)[0].innerWidth;
        if( window_size > 992 ) {
          $('.toggle_wrap #hamburger-toggle').remove();
          $('.dashboard_menu').show();
        } else {
          !$('.toggle_wrap #hamburger-toggle').length && add_toggle_element();
          $('.dashboard_menu').hide();
        }
      });
      
    }

  $(function() {
    var matchClass = $('.match-height');
    var matchClassRow = $('.match-height-row');
    if (matchClass.length) {
      matchClass.matchHeight({
        byRow: false
      });
    }
    if (matchClassRow.length) {
      matchClassRow.matchHeight();
    }
  });
  
  $(function() {
      FastClick.attach(document.body);
  });

  function show_shipping_warranty_notice(message){
      var myModal = $('#myModal');
      $('.modal-title', myModal).text('Update Notice!');
      $('.modal-body', myModal).html(message);
      $('a', myModal).unbind( "click").click(function(){
        var link = $(this).attr('href');
        $('iframe', myModal).remove();
        $('.modal-body', myModal).append('<iframe src="'+link+'?display=content" style="width:100%;height:50vh;"/>');
        return false;
      });
      $('.close', myModal).remove();
      $('.btn-primary', myModal).text('Confirm').click(close_shipping_warranty_notice);
      myModal.addClass('show').removeClass('fade');
      $('<div class="modal-backdrop show"/>').css('background','#0000007a').appendTo('body');
  }

  function close_shipping_warranty_notice(){
      $('#myModal').removeClass('show').addClass('fade');
      $('.modal-backdrop').remove();
      $.get( "/torin/shipping-warranty-notice/confirmed", function( data ) {
        console.log(data);
      });
  }

  function shipping_warranty_notice() {
      $.get( "/torin/shipping-warranty-notice", function( data ) {
        data = $.parseJSON(data);
          if ( data.status == 'show_notice' ) {
              show_shipping_warranty_notice(data.message);
          }
      });
  }
  shipping_warranty_notice();

  function init_InputMask() {
    if( typeof ($.fn.inputmask) === 'undefined' && !$(".inputmaskphone").length ) { return; }
      $('.inputmaskphone input').each(function(){
        $(this).inputmask('(999) 999-9999');
      });
  }; init_InputMask();

  $('table.table-pdf, #faq1189 table, #faq1191 table').wrap('<div class="overflow-scroll"></div>');
 
  $('.filter-item select').niceSelect();

  //cookies - location select
  //var locsel = Cookies.get('location') || "";
  //if( locsel != ""){
    //console.log(locsel);
    //$('#location-select').addClass('hidden');
    //$('body').addClass('locselHide');
  //}

  //Cookies.set('location', '1');

  // location select menu
  // $('.panel-menu .item:first-child').on('click', function(e){
  //     e.preventDefault();
  //     $('#location-select').fadeOut('fast');
  //     $('body').css('overflow-y','scroll');
  // });

  // browser window scroll (in pixels) after which the "back to top" link is shown
  var offset = 300,
    //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
    offset_opacity = 1200,
    //duration of the top scrolling animation (in ms)
    scroll_top_duration = 700,
    //grab the "back to top" link
    $back_to_top = $('.cd-top');

  //hide or show the "back to top" link
  $(window).scroll(function(){
    ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
    if( $(this).scrollTop() > offset_opacity ) { 
      $back_to_top.addClass('cd-fade-out');
    }

    if ( $('.js-back-to-top').length ) {
      var d = $(document),
          w = $(window),
          wh = w.height(),
          dh = d.height(),
          fh = $('footer').height(),
          ds = d.scrollTop(),
          sd = dh - ( ds + wh );
      if ( ds > 100 ) {
        $('.back-to-top').fadeIn('slow');
      } else { 
        $('.back-to-top').fadeOut('slow');
      }
      if ( sd - 60 < fh  ) {
        // $('.back-to-top').css({'bottom': ( fh - sd + 80 ) + 'px'});
      } else {
        // $('.back-to-top').removeAttr('style');
      }
    }
  });

  //smooth scroll to top
  $back_to_top.on('click', function(event){
    event.preventDefault();
    $('body,html').animate({
      scrollTop: 0 ,
      }, scroll_top_duration
    );
  });

  // header carousel
	$('#slider').owlCarousel({
	  animateOut: 'fadeOut',
      autoplay: true,
      loop:true,
      slideSpeed : 300,
      items:1,  
      nav:false,
      dots: false,
      smartSpeed:450
	});

  $('.search-ico').on('click',function(){
    $('.search-mobile .navbar-form').slideToggle('fast');
  })

  // $('a[href="#"]').on('click', function(e){
  //   e.preventDefault();
  // });

  $('[name^="acf[field_"]').each(function(){
    $(this).val( $('[data-acffield="'+$(this).attr('name')+'"]').val() );
  });

  $('.acf-field-image').each(function(){
    var img = $('[data-acffieldimg="acf['+$(this).attr('data-key')+']"]').val();
    if ( img ) {
      $('img',this).attr('src', img);
    } else {
      $('.acf-icon-cancel',this).click();
    }
  });

  $('.dashboard_menu ul li a[href="'+window.location.href+'"]').addClass('active');

  if ( $('.faqs, .menuslide').length ) {
    $('.faqs > li > a, .menuslide > li > a').click(function(){
      var tp = $($(this).attr('href')).offset().top;
      console.log(tp);
      $("html, body").animate({ scrollTop: tp }, "slow");
      return false;
    });

    if ( $('.faqs').length ) {
      $('.faqs').after('<div id="faqlist"></div>');
      $('.faqs > li').each(function(){
        var list = $(this).clone();
        $('> div',this).remove();
        $('> a',list).replaceWith($('<h4 id="cat'+$('> div',list).attr('id')+'">' + $('> a',list).text() + '</h4>'));
        $('#faqlist').append(list.html());
      });
    }
  }

  $('#post.acf-form').submit(function(){
    var haserror = false;
    $('#message, .acf-error-message').remove();
    if ( $('.inputmaskphone input',this).val().includes('_') ) {
      $('.inputmaskphone .acf-input .acf-error-message').remove();
      $('<div class="acf-error-message"><p>'+$('.inputmaskphone label',this).text().replace('*','')+' is invalid</p></div>').prependTo('.inputmaskphone .acf-input');
      haserror = true;
    }
    if ( $('.js-min5 input',this).val().length < 5 || !$.isNumeric( $('.js-min5 input',this).val() ) ) {
      $('.js-min5 .acf-input .acf-error-message').remove();
      $('<div class="acf-error-message"><p>'+$('.js-min5 label',this).text().replace('*','')+' must have 5 minimum number</p></div>').prependTo('.js-min5 .acf-input');
      haserror = true;
    }
    if ( haserror ) return false;
  });

  if ( $('.back-to-top').length ) {
    $('.back-to-top').appendTo('body');

    $(document).on('click','.js-back-to-top', function(){
      $('html, body').stop().animate({'scrollTop':0},'slow');
      return false;
    });
  }

  // onload show

    $('.welcome-right.torin-calc').css('display','block');

  // auto resize inputbox
  function resizeInput() {
    $(this).attr('size', $(this).val().length);
}

$('.main-quote-wrap input[type="text"]')
    // event handler
    .keyup(resizeInput)
    // resize on page load
    .each(resizeInput);

});