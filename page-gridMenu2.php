<?php 
// Template Name: Page Grid Menufmenu-grid-wrapper v2
get_header(); 
$content = $post->post_content;
$content_tab_repeater = get_field('content_tab_repeater');
	$num_tab_repeater = $content_tab_repeater ? count($content_tab_repeater) : 0;
	$tab_repeater_count = '';

	if($num_tab_repeater >= 5) {
		$tab_repeater_count = 'multiple';
	} else {
		$tab_repeater_count = 'single';
  }
  
  $page_class_attribute = get_field('page_class_attribute');
  $page_class = $page_class_attribute ? ' ' . $page_class_attribute : '';
  
?>

<section class="header-image-new">

  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">
    <div class="menu-grid-wrapper">
      <?php if( have_rows('menu_group') ): ?>
      <div class="component__list component__list--column">
        <?php while( have_rows('menu_group') ): the_row(); 
          // vars
          $image = get_sub_field('image');
          $title = get_sub_field('title');
          $link = get_sub_field('link');
        ?>
        <div class="component__item-list">
            <div class="component__item <?php echo empty($image) ? 'no-image' : ''; ?>">
            <?php if( $link ): ?>
              <a target="<?php if($tab == 'true'){echo '_blank';} ?>" href="<?php echo $link; ?>">
            <?php endif; ?>
              
              <div class="component__item-image <?php echo empty($image) ? 'no-image' : ''; ?>" style="background-image:url('<?php echo $image['url']; ?>');" data-mh="component__item-image">
                <?php if(!empty($image)) { ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>">
                <?php } else { ?>
                <img src="<?php echo get_template_directory_uri();?>/images/default-no-image.jpg" alt="Standard Elevator">

                <?php } ?>
              </div>
              <div class="component__item-header">
                <h4 class="component__item-title"><?php echo $title; ?></h4>
              </div>
              <?php if( $link ): ?>
              </a>
              <?php endif; ?>
            </div>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
      </div>
    </div>
    <?php if(!empty($content)) { ?>
    <div class="section__tab table-responsive">
      <?php the_content(); ?>
    </div>
    <?php } ?>
    <?php if(is_array($content_tab_repeater)) { ?>
  
  <div class="container<?php echo $page_class; ?>">
    <div class="section__tab">
        <ul class="nav nav-tabs <?php echo $tab_repeater_count; ?>">
        <?php 
        $i_ctr = 0; foreach ($content_tab_repeater as $tab_title) {
            if($i_ctr == 0){
            $active = 'active';
            }else{
            $active = '';
            }
        ?>
        <li class="<?php echo $active; ?>"><a href="#tab-<?php echo $i_ctr++; ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $tab_title['heading'] ?></a></li>
        <?php $i_ctr++;  }  ?>
        </ul>
        <div class="tab-content table-responsive">
            <?php $i_ctr = 0; foreach ($content_tab_repeater as $tab_content) { 
            if($i_ctr == 0){
            $conactive = 'in active';
            }else{
            $conactive = '';
            }
            ?>
            <div role="tabpanel" class="tab-pane fade <?php echo $conactive; ?>" id="tab-<?php echo $i_ctr++; ?>" class="video_cat">
            <h3><?php echo $tab_content['heading'] ?></h3>
            <div class="tab-desc">
                <?php echo $tab_content['content']; ?>
            </div>
            </div>
            <?php $i_ctr++; } ?>
        </div>
        </div>
  </div>

    </div>
  <?php } ?>
  </div>
</section>
<!-- <a href="#0" class="cd-top">Top</a> -->
<?php get_footer(); ?>