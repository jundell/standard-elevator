<?php ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="format-detection" content="telephone=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <title><?php bloginfo('name') ?> - <?php bloginfo('description') ?></title>
    <?php wp_head(); ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->
    <link rel="shortcut icon" href="/wp-content/themes/torindriveintl/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/wp-content/themes/torindriveintl/favicon.ico" type="image/x-icon">
    
</head>
<?php
  $user = wp_get_current_user();
?>
<body <?php body_class('page-template'); ?> class="no-scroll">

  <header>
    <nav class="navbar navbar-default">
      <div class="container">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="<?php echo bloginfo('name'); ?>" class="img-responsive"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-navbar-collapse">         
          <?php
            $defaults = array(
                'theme_location'  => 'primary',
                'menu_class'      => 'nav navbar-nav',
                'fallback_cb'     => 'wp_page_menu',
            );
              wp_nav_menu($defaults);
          ?>
          <!-- search form -->
          <div class="visible-xs">
            <?php get_search_form(); ?>
          </div>
		  <?php /*
          <a href="#" class="login-btn">Login</a>*/?>
          <!-- <div class="search-ico hidden-xs"><i class="glyphicon glyphicon-search"></i></div> -->
          <div class="search-ico hidden-xs"><img src="<?=get_template_directory_uri()?>/images/icon-search.png" alt="Torin Drive International"></div>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
  </header>
  <div class="search-mobile hidden-xs">
    <div class="container">
      <div class="row">
        <?php get_search_form(); ?>
      </div>
    </div>
  </div>
