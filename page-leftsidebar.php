<?php 
/* Template Name: Left Sidebar Page */
	get_header();

	$content_tab_repeater = get_field('content_tab_repeater');
	$num_tab_repeater = $content_tab_repeater ? count($content_tab_repeater) : [];
	$tab_repeater_count = '';

	if($num_tab_repeater >= 5) {
		$tab_repeater_count = 'multiple';
	} else {
		$tab_repeater_count = 'single';
	}

?>
<section class="header-image-new">
  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">
	<?php 
		//if( is_user_logged_in() || is_page('operation-manual') ):
	?>	  
		<div class="bg-side"></div>
		<div class="row">
		 
		  <div class="col-md-3 sidebar_wrap welcome-left page--videos">       
			<?php 
				get_sidebar('menu'); 
			?>
		  </div>
		  <div class="col-md-9 content_wrap welcome-right">
			<?php the_content(); ?>
			<?php if(is_array($content_tab_repeater)) { ?>
			<div class="section__tab">
				<ul class="nav nav-tabs <?php echo $tab_repeater_count; ?>">
					<?php 
					$i_ctr = 0; foreach ($content_tab_repeater as $tab_title) {
						if($i_ctr == 0){
						$active = 'active';
						}else{
						$active = '';
						}
					?>
					<li class="<?php echo $active; ?>"><a href="#tab-<?php echo $i_ctr++; ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $tab_title['heading'] ?></a></li>
					<?php $i_ctr++;  }  ?>
				</ul>
					<div class="tab-content">
						<?php $i_ctr = 0; foreach ($content_tab_repeater as $tab_content) { 
							if($i_ctr == 0){
							$conactive = 'in active';
							}else{
							$conactive = '';
							}
						?>
						<div role="tabpanel" class="tab-pane fade <?php echo $conactive; ?>" id="tab-<?php echo $i_ctr++; ?>" class="video_cat">
							<h3><?php echo $tab_content['heading'] ?></h3>
							<div class="tab-desc">
								<?php echo $tab_content['content']; ?>
							</div>
						</div>
						<?php $i_ctr++; } ?>
					</div>
				</div>
		  	</div>
			<?php } ?>
		</div>
	
	<?php //else : ?>
		
		<?php //get_template_part('restricted-error'); ?>
		
	<?php //endif; ?>
  </div>
</section>
<?php get_footer(); ?>