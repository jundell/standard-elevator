<?php 
/* Template Name: All Archive */
get_header();

$image = get_field('sub_header_image');

?>
<section class="header-image-new">

  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">
    <div class="bg-side"></div>
    <div class="row">
      <div class="clear"></div>
      <?php 
       if( is_user_logged_in() ) {
      ?>
      <div class="col-md-3 col-xs-6 sidebar_wrap welcome-left">
        <?php 
			get_sidebar('menu'); 
		?>
      </div>
      <div class="col-md-9 col-xs-6 content_wrap welcome-right">
          <?php the_content(); $header = ''; $last_date = '' ?>
            <?php
              global $post;
              $args = array( 'posts_per_page' => -1, 'post_status' => 'publish' );
              $myposts = get_posts( $args );
              foreach ( $myposts as $post ) : 
                setup_postdata( $post );
                $this_date = get_the_date('F Y');
            ?>
              <?php if ( !$header ) : $header = true; $last_date = $this_date; ?>
                <h4><?php echo $this_date; ?></h4>
                <ul class="alink">
              <?php elseif ( $this_date != $last_date ) : $last_date = $this_date; ?>
                </ul>
                <h4><?php echo $last_date; ?></h4>
                <ul class="alink">
              <?php endif; ?>

              <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
            <?php endforeach;
            wp_reset_postdata(); ?>
          </ul>
      </div>
      <?php } ?>
    </div>
  </div>
</section>
<?php get_footer(); ?>