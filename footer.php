	<!-- end of structure -->
  <footer>
      <div class="container">
        <div class="details-wrapper">
          <div class="grid-container contact-details">
            <!-- <h3>Contact details</h3> -->
              <div class="custom-col-grid footer__item footer-separator">
                <?php dynamic_sidebar( 'address' ); ?>
              </div>
              <div class="custom-col-grid footer__item footer-separator">
                <?php dynamic_sidebar( 'Contact Info' ); ?>
              </div>
              <div class="custom-col-grid footer__item footer-separator">
                <?php dynamic_sidebar( 'Email and Schedule' ); ?>
              </div>
              <div class="custom-col-grid footer__item social footer-separator">
              <!-- <h3>Follow us</h3> -->
              <?php dynamic_sidebar( 'Social Media' ); ?>
              </div>
              <div class="custom-col-grid footer__item text-footer text-center">
                <?php dynamic_sidebar( 'Our Brands' ); ?>
              </div>
          </div>
        </div>
      </div>
      <div>
        <div class="copy">
          <div class="container">
              <?php dynamic_sidebar( 'Footer Copyright' ); ?>
          </div>
        </div>
      </div>
        
      
    </footer>
    <?php wp_footer();
    $user = wp_get_current_user();
    $member_login_page = $_SERVER['REQUEST_URI'];

    if($member_login_page == '/membership-login/'){
      if(!empty($user->roles[0]) && $user->roles[0]=='subscriber'){
        echo '<script type="text/javascript">window.location.href ="/membership-login/membership-profile/";</script>';
      }
    }

    if($member_login_page == '/login/'){
      if(!empty($user->roles[0]) && $user->roles[0]=='subscriber'){
        echo '<script type="text/javascript">window.location.href ="/dashboard";</script>';
      }
    }

    ?>
    <script type="text/javascript">
      jQuery(document).ready(function(){
        jQuery('.swpm-join-us-link').insertBefore('.swpm-forgot-pass-link');
        jQuery('.swpm-join-us-link #register').css('display','none');
        jQuery('.swpm-join-us-link').css('display','none');
      });
    </script>
	
	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">		
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title"></h4>
		  </div>
		  <div class="modal-body"></div> 
      <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>     
		</div>
	  </div>
	</div>
  </body>
</html>