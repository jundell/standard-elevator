<?php
/* Template Name: Order Page */
	get_header();
?>


<style>
.order-summary {

border: 2px solid #446084;
    padding: 15px 30px 30px;
}
.order-summary h3 {

    color: #555;
    width: 100%;
    margin-top: 0;
    margin-bottom: .5em;
    text-rendering: optimizeSpeed;
   text-transform : uppercase;
}
table {
    width: 100%;
    margin-bottom: 1em;
    border-color: #ececec;
    border-spacing: 0;
}
thead {
    display: table-header-group;
    vertical-align: middle;
    border-color: inherit;
}
tr {
    display: table-row;
    vertical-align: inherit;
    border-color: inherit;
}
th, td {
    padding: .5em;
    text-align: left;
    border-bottom: 1px solid #ececec;
    line-height: 1.3;
    font-size: .9em;
}
.shop_table .cart_item td {
    padding-top: 15px;
    padding-bottom: 15px;
}
td.product-name {
    word-break: break-word;
    text-overflow: ellipsis;
}
th:first-child, td:first-child {
    padding-left: 0;
}
td {
    color: #666;
}
.woocommerce-checkout-payment ul {
list-style:none !important;
}
.woocommerce-billing-fields {
    padding-top: 15px;
    border-top: 2px solid #ddd;
}
.message-wrapper {
    margin: 0;
    padding-bottom: .5em;
}
form.checkout h3 {
    font-size: 1.1em;
    overflow: hidden;
    padding-top: 10px;
    font-weight: bolder;
    text-transform: uppercase;
}
.wc_payment_methods li {
  list-style:none  !important;
}
#place_order{
padding: 10px;
    border: #446084 1.5px solid;
    color: #446084;
    text-transform: uppercase;
    font-style: bold;
    font-weight: bold;
   background: #fff;
}
#place_order:hover {
  color : #fff;
  background: #446084;

}
input[type='email'], input[type='date'], input[type='search'], input[type='number'], input[type='text'], input[type='tel'], input[type='url'], input[type='password'], textarea, select, .select-resize-ghost, .select2-container .select2-choice, .select2-container .select2-selection {
    box-sizing: border-box;
    border: 1px solid #ddd;
    padding: 0 .75em;
    height: 2.507em;
    font-size: .97em;
    border-radius: 0;
    max-width: 100%;
    width: 100%;
    vertical-align: middle;
    background-color: #fff;
    color: #333;
    box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
    transition: color .3s, border .3s, background .3s, opacity .3s;
}
p.form-row-wide {
    clear: both;
}
.woocommerce-billing-fields p {
    margin-bottom: .5em;
}
form p {
    margin-bottom: .5em;
}

p {
    margin-top: 0;
}

.order-wrapper {

    border : 1px solid #176fa5;
    margin :10px;
}
.order-wrapper table > thead > tr:first-child>th {
    width: 0px;
    padding: 0px 6px !important;
    height: 48px;
    vertical-align: middle;
    background-color: transparent !important;
    color: #333;
    border:1px solid #ddd;
}
.order-wrapper >table {
    background : transparent !important;
    width : 100%;
}

</style>
<section class="header-image-new">
  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">
	<?php
		if( is_user_logged_in() ):
	?>
		<div class="bg-side"></div>
		<div class="row">

		  <div class="col-md-3 sidebar_wrap welcome-left">
			<?php
				get_sidebar('menu');
			?>
		  </div>
		  <div class="col-md-9 content_wrap welcome-right">
					<?php
				global $wpdb;
						$order_id = $_GET['id'];


			$orders = $wpdb->get_results("SELECT * FROM sparepart_purchases_data WHERE order_id = '$order_id' ") ;
 			?>


						<p class="order-title">ORDER # <?=$order_id?></p>
								<div class="order-wrapper">
									<table class="shop_table woocommerce-checkout-review-order-table">
				<thead>
					<tr>
						<th class="product-name">Product</th>

						<th class="product-total" style="text-align:center;width:25%;">Sub total</th>
					</tr>
				</thead>
				<tbody>
					<?php
							$total = 0 ;

 				?>
							<?php foreach($orders as $order) { ?>
									<tr class="cart_item">
								<td class="product-name" style="text-align:left;background-color: transparent !important;">
								<?=$order->item_name;?>&nbsp;						 <strong class="product-quantity">×&nbsp;	<?=$order->quantity;?></strong>											</td>
								<td class="product-total" style="text-align:left;background-color: transparent !important;">
																		$ <?=$order->price * $order->quantity;?>
								</td>
							</tr>


			<?php 		$total = $total + ($order->price * $order->quantity);  } ?>
				<?php


 			?>
						</php } ?>
								</tbody>
				<tfoot>
		<?php

			$fees = $wpdb->get_results("SELECT * FROM sparepart_purchases WHERE order_id = '$order_id' ") ;
		$cc_fee = 0 ;
		$discount = 0 ;

		foreach ($fees as $key => $value) {
				$cc_fee = $value->cc_fee;
				$discount = $value->total_discount;
		}

 ?>
					<tr class="cart-subtotal">
						<th style="text-align:right;padding-right:10px;">Sub Total</th>
						<td style="background-color: transparent !important;text-align:left;"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span><?=number_format($total ,2)?></span></td>
					</tr>



										<tr class="order-total">
											<th style="text-align:right;padding-right:10px;">CC fee</th>
											<td style="background-color: transparent !important;"><strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span><?=number_format($cc_fee,2)?></span></strong> </td>
										</tr>


															<tr class="order-total">
																<th style="text-align:right;padding-right:10px;">Dicount</th>
																<td style="background-color: transparent !important;"><strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span><?=number_format($discount,2)?></span></strong> </td>
															</tr>
					<tr class="order-total">
						<th style="text-align:right;padding-right:10px;">Total</th>
						<td style="background-color: transparent !important;"><strong><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span><?=number_format(($total + $cc_fee) - $discount,2)?></span></strong> </td>
					</tr>


				</tfoot>
				</table>
								</div>
<?php
						$order = wc_get_order( $order_id ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited

if ( ! $order ) {
	return;
}

$order_items           = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();
$show_downloads        = $order->has_downloadable_item() && $order->is_download_permitted();

if ( $show_downloads ) {
	wc_get_template(
		'order/order-downloads.php',
		array(
			'downloads'  => $downloads,
			'show_title' => true,
		)
	);
}
?>
<div class="order-summary">

<section class="woocommerce-order-details">
	<?php do_action( 'woocommerce_order_details_before_order_table', $order ); ?>

	<h2 class="woocommerce-order-details__title"><?php esc_html_e( 'Order details', 'woocommerce' ); ?></h2>

	<table class="woocommerce-table woocommerce-table--order-details shop_table order_details">

		<thead>
			<tr>
				<th class="woocommerce-table__product-name product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
				<th class="woocommerce-table__product-table product-total"><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
			do_action( 'woocommerce_order_details_before_order_table_items', $order );

			foreach ( $order_items as $item_id => $item ) {
				$product = $item->get_product();

				wc_get_template(
					'order/order-details-item.php',
					array(
						'order'              => $order,
						'item_id'            => $item_id,
						'item'               => $item,
						'show_purchase_note' => $show_purchase_note,
						'purchase_note'      => $product ? $product->get_purchase_note() : '',
						'product'            => $product,
					)
				);
			}

			do_action( 'woocommerce_order_details_after_order_table_items', $order );
			?>
		</tbody>

		<tfoot>
			<?php
			foreach ( $order->get_order_item_totals() as $key => $total ) {
				?>
					<tr>
						<th scope="row"><?php echo esc_html( $total['label'] ); ?></th>
						<td><?php echo ( 'payment_method' === $key ) ? esc_html( $total['value'] ) : wp_kses_post( $total['value'] ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></td>
					</tr>
					<?php
			}
			?>
			<?php if ( $order->get_customer_note() ) : ?>
				<tr>
					<th><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
					<td><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td>
				</tr>
			<?php endif; ?>
		</tfoot>
	</table>

	</section>


<?php

					 ?>
		  </div>

		</div>

	<?php else : ?>

		<?php get_template_part('restricted-error'); ?>

	<?php endif; ?>
  </div>
</section>
</div>
<?php get_footer(); ?>
