<div class="restricted-error" style="padding:40px 0;">
	<p>LOGIN REQUIRED! <br> This page is only accessible to logged-in users with proper access privileges. Please click "Login" to proceed logging in to your account.</p>
	<p><a class="btn_login" href="<?php echo site_url(); ?>/login"> Login</a></p>
</div>