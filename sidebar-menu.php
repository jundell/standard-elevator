<?php 
	// display customer Logo
	echo get_user_company_logo(); 
?>
 
<?php 
	
	// user role
	$user_role = wp_get_current_user()->roles[0];
	$user_id = wp_get_current_user()->ID;

	switch( $user_role ) {
		case 'administrator':
			$fields = array('admin_menu_title','admin_menu_link');
			$list = custom_repeater_data('dashboard_amin_menu', $fields, null, 'option');
			break;
		case 'project-manager':
			$fields = array('admin_menu_title','admin_menu_link');
			$list = custom_repeater_data('dashboard_project_manager', $fields,null,'option');
			break;
		default:
			$fields = array('text','page');
			$list = custom_repeater_data('left_sidebar', $fields, null, 'option');			
			break;
	}	
?>
<div class="dashboard_menu">
	<?php
		if ( $user_role == 'project-manager') {
	?> 
			<h3 class="pm-welcome-heading">Welcome, <?php echo get_user_meta($user_id, 'first_name', true); ?>! </h3> 
	<?php
		}
	?>
	<ul>
		<?php foreach ( $list as $item ): ?>
			<li>
				<a href="<?php echo $item[$fields[1]]; ?>"><?php echo $item[$fields[0]]; ?><i class="fa fa-angle-right"></i></a>
			</li>
		<?php endforeach; ?>
	</ul>
</div>