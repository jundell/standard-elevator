<?php


use Carbon_Fields\Field;
use Carbon_Fields\Block;

Block::make( __( 'Classic Rich Text' ) )
->set_category( $kinslow['guttenberg_category'], __( $category, $context ), $icon )
->set_inner_blocks( false )
->add_fields( array(
    Field::make( 'complex', 'cwrap', '' )
        ->set_layout( 'tabbed-horizontal' )
        ->add_fields( 'cwrap', __( 'Per Content', $context ), array(
            Field::make( 'text', 'class', __( 'Enter Wrapper Class', $context ) ),
            Field::make( 'rich_text', 'content', __( 'Enter Content', $context ) )
        ) )
) )
->set_preview_mode( false )
->set_render_callback( function( $fields, $attributes, $inner_blocks ) {
    ?>
        <div class="mm-richtext<?php echo ' ' . $attributes['className']; ?>">
            <?php 
                $cwrap = $fields['cwrap'];
                if( is_array( $cwrap ) && !empty( $cwrap ) ):
                    foreach($cwrap as $c):
                        ?>
                        <div class="content-wrap-module <?php echo $c['class']; ?>">
                            <?php echo $c['content']; ?>
                        </div>
                        <?php
                    endforeach;
                endif;
            ?>
        </div>
    <?php
} );