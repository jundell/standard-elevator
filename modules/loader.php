<?php

function carbon_fields_initialize() {
	\Carbon_Fields\Carbon_Fields::boot();
}
add_action( 'after_setup_theme', 'carbon_fields_initialize' );

function mm_custom_blocks_loader( $files = array(), $repository = '/modules/custom-blocks/' ) {
    
    $context = "mm_custom_blocks";
    $category = "Torin Blocks";
    $icon = "laptop";

    foreach( $files as $file ) {
        require get_template_directory() . $repository . $file . '.php';
    }
}

add_action( 'carbon_fields_register_fields', 'mm_load_custom_blocks_assets' );

function mm_load_custom_blocks_assets () {

    mm_custom_blocks_loader( array(
        // custom blocks list of module starts here
        'classic-editor'
    ) );
    
}