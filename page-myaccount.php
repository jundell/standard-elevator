<?php
/* Template Name: My Account Page */
	get_header();
?>
<style>
.order-summary h3 {

    color: #555;
    width: 100%;
    margin-top: 0;
    margin-bottom: .5em;
    text-rendering: optimizeSpeed;
   text-transform : uppercase;
}
.woocommerce-checkout-payment ul {
list-style:none !important;
}
.woocommerce-billing-fields {
    padding-top: 15px;
    border-top: 2px solid #ddd;
}
.message-wrapper {
    margin: 0;
    padding-bottom: .5em;
}
form.checkout h3 {
    font-size: 1.1em;
    overflow: hidden;
    padding-top: 10px;
    font-weight: bolder;
    text-transform: uppercase;
}
.wc_payment_methods li {
  list-style:none  !important;
}
#place_order{
padding: 10px;
    border: #446084 1.5px solid;
    color: #446084;
    text-transform: uppercase;
    font-style: bold;
    font-weight: bold;
   background: #fff;
}
#place_order:hover {
  color : #fff;
  background: #446084;

}
input[type='email'], input[type='date'], input[type='search'], input[type='number'], input[type='text'], input[type='tel'], input[type='url'], input[type='password'], textarea, select, .select-resize-ghost, .select2-container .select2-choice, .select2-container .select2-selection {
    box-sizing: border-box;
    border: 1px solid #ddd;
    padding: 0 .75em;
    height: 2.507em;
    font-size: .97em;
    border-radius: 0;
    max-width: 100%;
    width: 100%;
    vertical-align: middle;
    background-color: #fff;
    color: #333;
    box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
    transition: color .3s, border .3s, background .3s, opacity .3s;
}
p.form-row-wide {
    clear: both;
}
.woocommerce-billing-fields p {
    margin-bottom: .5em;
}
form p {
    margin-bottom: .5em;
}

p {
    margin-top: 0;
}



.order-summary h3 {

    color: #555;
    width: 100%;
    margin-top: 0;
    margin-bottom: .5em;
    text-rendering: optimizeSpeed;
   text-transform : uppercase;
}
.woocommerce-checkout-payment ul {
list-style:none !important;
}
.woocommerce-billing-fields {
    padding-top: 15px;
    border-top: 2px solid #ddd;
}
.message-wrapper {
    margin: 0;
    padding-bottom: .5em;
}
form.checkout h3 {
    font-size: 1.1em;
    overflow: hidden;
    padding-top: 10px;
    font-weight: bolder;
    text-transform: uppercase;
}
.wc_payment_methods li {
  list-style:none  !important;
}
#place_order{
padding: 10px;
    border: #446084 1.5px solid;
    color: #446084;
    text-transform: uppercase;
    font-style: bold;
    font-weight: bold;
   background: #fff;
}
#place_order:hover {
  color : #fff;
  background: #446084;

}
input[type='email'], input[type='date'], input[type='search'], input[type='number'], input[type='text'], input[type='tel'], input[type='url'], input[type='password'], textarea, select, .select-resize-ghost, .select2-container .select2-choice, .select2-container .select2-selection {
    box-sizing: border-box;
    border: 1px solid #ddd;
    padding: 0 .75em;
    height: 2.507em;
    font-size: .97em;
    border-radius: 0;
    max-width: 100%;
    width: 100%;
    vertical-align: middle;
    background-color: #fff;
    color: #333;
    box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
    transition: color .3s, border .3s, background .3s, opacity .3s;
}
p.form-row-wide {
    clear: both;
}
.woocommerce-billing-fields p {
    margin-bottom: .5em;
}
form p {
    margin-bottom: .5em;
}

p {
    margin-top: 0;
}

.table {
  width: 100%;
  max-width: 100%;
  margin-bottom: 1rem;
}

.table th,
.table td {
  padding: 0.75rem;
  vertical-align: top;
  border-top: 1px solid #eceeef;
}

.table thead th {
  vertical-align: bottom;
  border-bottom: 2px solid #eceeef;
}

.table tbody + tbody {
  border-top: 2px solid #eceeef;
}

.table .table {
  background-color: #fff;
}

.table-sm th,
.table-sm td
  padding: 0.3rem;
}

.table-bordered {
  border: 1px solid #eceeef;
}

.table-bordered th,
.table-bordered td {
  border: 1px solid #eceeef;
}

.table-bordered thead th,
.table-bordered thead td {
  border-bottom-width: 2px;
}

.table-striped tbody tr:nth-of-type(odd) {
  background-color: rgba(0, 0, 0, 0.05);
}

.table-hover tbody tr:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-active,
.table-active > th,
.table-active > td {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover > td,
.table-hover .table-active:hover > th {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-success,
.table-success > th,
.table-success > td {
  background-color: #dff0d8;
}

.table-hover .table-success:hover {
  background-color: #d0e9c6;
}

.table-hover .table-success:hover > td,
.table-hover .table-success:hover > th {
  background-color: #d0e9c6;
}

.table-info,
.table-info > th,
.table-info > td {
  background-color: #d9edf7;
}

.table-hover .table-info:hover {
  background-color: #c4e3f3;
}

.table-hover .table-info:hover > td,
.table-hover .table-info:hover > th {
  background-color: #c4e3f3;
}

.table-warning,
.table-warning > th,
.table-warning > td {
  background-color: #fcf8e3;
}

.table-hover .table-warning:hover {
  background-color: #faf2cc;
}

.table-hover .table-warning:hover > td,
.table-hover .table-warning:hover > th {
  background-color: #faf2cc;
}

.table-danger,
.table-danger > th,
.table-danger > td {
  background-color: #f2dede;
}

.table-hover .table-danger:hover {
  background-color: #ebcccc;
}

.table-hover .table-danger:hover > td,
.table-hover .table-danger:hover > th {
  background-color: #ebcccc;
}

.thead-inverse th {
  color: #fff;
  background-color: #292b2c;
}

.thead-default th {
  color: #464a4c;
  background-color: #eceeef;
}

.table-inverse {
  color: #fff;
  background-color: #292b2c;
}

.table-inverse th,
.table-inverse td,
.table-inverse thead th {
  border-color: #fff;
}
.add_to_cart_button {
    color: #fff !important;
    background-color: #5cb85c;
    border-color: #4cae4c;
}
.table {
 font-size:11px;
}
.table > p {
font-size:11px;
}
.table tr td {
font-size:11px;
}
table > tbody > tr td, .table > tbody > tr td {

 font-size : 11px !important;
}
.torin-calc table > thead > tr > th, .table > thead > tr > th, table.table>thead:first-child>tr:first-child>th {
 font-size:11px;
}


.section.content  .table > tbody > tr > td > p {
font-size:11px !important;
}

.table tr {
    background:   #f6f6f6 ;

 border:transparent !important;
}
.table tr td {
	 border:transparent !important;
}


.table tr:nth-child(2n+1)  {
    background: #e4e4e4  !important ;
   border-top : 1px #e4e4e4 solid !important;


  }


 .table-wrapper .row-vm.shown-even,
 .table-wrapper .row-vm.shown-even + .row-details,
.table-wrapper .row-vm.shown,
 .table-wrapper .row-vm.shown + .row-details { display: table-row; background: #f6f6f6; }
.table-wrapper .row-vm.shown-even,
 .table-wrapper .row-vm.shown-even + .row-details { background: #e4e4e4; }
.table-wrapper .row-vm.hidden,
 .table-wrapper .row-vm.hidden + .row-details { display: none; }

#style-2_length ,#style-2_filter{
    display:none;
}

.filter-wrapper {
    background: #dad9d9;
    margin-bottom: 10px;
    padding: 10px 20px 0px 15px;
    display: inline-block;
	width:100%;

}
</style>

<style>
    hr {
        margin-top: 40px;
        margin-bottom: 40px;
        border: 0;
        border-top: 1px solid #7b7b7b;
    }
    .torin-calc .form-horizontal .form-group{
        margin-bottom: 10px;
        margin-right: 0;
        margin-left: 0;
    }
    .torin-calc .btn-custom-dropdown {
        background-color: hsl(12, 4%, 80%) !important;
        background-repeat: repeat-x;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#12f133134", endColorstr="#cecac9");
        background-image: -khtml-gradient(linear, left top, left bottom, from(#12f133134), to(#cecac9));
        background-image: -moz-linear-gradient(top, #12f133134, #cecac9);
        background-image: -ms-linear-gradient(top, #12f133134, #cecac9);
        background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #12f133134), color-stop(100%, #cecac9));
        background-image: -webkit-linear-gradient(top, #12f133134, #cecac9);
        background-image: -o-linear-gradient(top, #12f133134, #cecac9);
        background-image: linear-gradient(#12f133134, #cecac9);
        border-color: #cecac9 #cecac9 hsl(12, 4%, 70%);
        color: #333 !important;
        text-shadow: 0 1px 1px rgba(255, 255, 255, 0.66);
        -webkit-font-smoothing: antialiased;
    }
    .button_example{
		width:100%;
        text-transform: uppercase;
        line-height: 28px;
        height: 30px;
        border-radius: 3px;
        border: 1px solid #bbbaba;
        font-size: 9px;
        font-family: arial, helvetica, sans-serif;
        padding: 0px 22px 0px 13px;
        text-decoration: none;
        display: inline-block;
        color: #000;
        background: rgba(253,253,253,1);
        background: -moz-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(253,253,253,1)), color-stop(12%, rgba(250,250,250,1)), color-stop(28%, rgba(247,247,247,1)), color-stop(49%, rgba(234,232,232,1)), color-stop(76%, rgba(219,217,217,1)), color-stop(100%, rgba(213,210,210,1)));
        background: -webkit-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
        background: -o-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
        background: -ms-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
        background: linear-gradient(to bottom, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
	}

    .button_example:hover{
        border: 1px solid #bbbaba;
        background: rgba(232,232,232,1);
        background: -moz-linear-gradient(top, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(232,232,232,1)), color-stop(19%, rgba(230,230,230,1)), color-stop(36%, rgba(230,230,230,1)), color-stop(60%, rgba(230,230,230,1)), color-stop(81%, rgba(230,230,230,1)), color-stop(100%, rgba(213,210,210,1)));
        background: -webkit-linear-gradient(top, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        background: -o-linear-gradient(top, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        background: -ms-linear-gradient(top, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        background: linear-gradient(to bottom, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e8e8e8', endColorstr='#d5d2d2', GradientType=0 );
    }
    .filter-wrapper .filter-container .filter-item select {
        font-size: 9px;
        text-transform: uppercase;
    }
    .torin-calc .final{
        background-color: #ffd700;
    }
    .torin-calc .request-final{
        color:#B52C3E;
        font-weight: bold;
        text-transform: uppercase;
        text-decoration: underline;
        letter-spacing: 0.1px;
		font-size: 11px;
    }
    .torin-calc .underline{
        text-decoration: underline;
        text-transform: uppercase;
        color: #000;
		font-size: 11px;
    }
    .torin-calc .cust-scrollbar{
        overflow-x: hidden;
        max-height: 558px;
    }

    .force-overflow-cust
    {
        min-width: 847px;
    }
    .force-overflow-cust table{
        margin-left: -15px;
        height: 550px;
    }

    #style-2::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 1px rgba(0,0.3,0,0);
        /*-webkit-box-shadow: inset 0 0 1px rgba(0,0,0,0.3);*/
        background-color: #F5F5F5;
    }


    .quotation .table thead tr{
        background-color: #eee;
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#f2f2f2), to(#fafafa));
        background-image: -webkit-linear-gradient(top, #f2f2f2 0, #fafafa 100%);
        background-image: -moz-linear-gradient(top, #f2f2f2 0, #fafafa 100%);
        background-image: -ms-linear-gradient(top, #f2f2f2 0, #fafafa 100%);
        background-image: -o-linear-gradient(top, #f2f2f2 0, #fafafa 100%);
        font-size: 12px
    }
    .table-bordered {
        border: 1px solid #ddd;
    }
    .torin-calc table > thead > tr > th, .table > thead > tr > th, table.table>thead:first-child>tr:first-child>th{
        border:none;
		text-align: center;
    }
    .torin-calc .table thead tr{
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#f2f2f2), to(#fafafa));
    }
    .torin-calc .table>thead>tr>th{
        vertical-align: bottom;
        border-bottom: 2px solid #ddd;
        padding: 8px;
        line-height: 1.428571429;
    }
    .torin-calc .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        padding: 8px;
        line-height: 1.428571429;
        vertical-align: top;
        border:none;
    }
    .table tr{
        border: 1px solid #ddd;
    }

    .list-btns {
        margin: 0;
        padding: 0;
        list-style: none !important;
        list-style-type: none !important;
    }
    .list-btns li{
        margin-top: 10px;
    }
    .bg-color-red {
        background-color: rgb(237, 26, 36) !important;
        border-radius: 0;
    }
    .list-btns .txt-color-white{
        color: #fff;
        text-transform: uppercase;
        font-size: 12px;
        font-weight: bold;
    }
    .btn-block{
        padding: 5px 16px;
        display: block;
        width: 100%;
    }
    .img-logo{
        border: 2px solid #ccc;
        float: left;
        width: 150px;
        height: 100px;
        position: relative;
        margin-bottom: 10px;
    }
    .img-logo .img-responsive{
        height: 100%;
        width: 100%;
    }
   .quotation-quote .button {
        background-color: transparent;
        text-align: center;
        display: inline-block;
        text-decoration: none;
        border: 1px solid;
        -webkit-transition: all 0.2s ease-in;
        -moz-transition: all 0.2s ease-in;
        -o-transition: all 0.2s ease-in;
        transition: all 0.2s ease-in;
        padding: 10px 30px 8px;
        font-size: 16px;
        padding: 5px 8px 5px;

        font-weight: 300;
        letter-spacing: 0.2rem;
        line-height: 1;
        text-transform: uppercase;
        border-radius: 0;
    }
    .quotation-quote .button:hover{
        text-decoration: none;
        cursor: pointer;
        color: #000;
    }
    .quotation-quote .no-left-margin{
        margin-left: 0;
    }
    .quotation-quote .button-neutral{
        color: #000;
        border-color: #000;
        font-size: 11px;
    }
    .quotation-quote .button-neutral:hover{
        color: #fff;
        background-color: rgb(237, 26, 36);
        border-color:rgb(237, 26, 36);
    }
    .quotation-quote button.button.button-neutral.btn-xs {
        margin-bottom: 10px;
    }

/*


    .center-td{
        text-align: center;
    }
    .border-tbl{
        border: 1px solid #ccc;
    }
    .search-input{
        width: 25%;
        float: right;
        margin-bottom: 10px;
    }
*/

    .stylish-input-group .input-group-addon{
        background: white !important;
    }
    .stylish-input-group .form-control{
        border-right:0;
        box-shadow:0 0 0;
        border-color:#ccc;
    }
    .stylish-input-group button{
        border:0;
        background:transparent;
    }
    .page-template .sidebar_wrap h3 {
        color: #151515;
        margin: 15px 15px 15px 0;
        background: #FFF;
        box-shadow: 1px 1px 1px #929292;
    }
    .page-template .sidebar_wrap h3 img {
        max-width: 100%;
    }
    .torin-calc .nav-tabs>li>a{
        color:#000;
        border-radius: 1px;
    }

    .scrollbar /*gale*/
    {
        float: left;
        width: 847px;
        background: #F5F5F5;
        overflow-y: scroll;
        margin-bottom: 25px;
    }

    .force-overflow
    {
        min-width: 847px;
    }
    .force-overflow table{
        margin-left: -15px;
        width: 847px;
    }

    #style-3::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 1px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }

    #style-3::-webkit-scrollbar
    {
        width: 1px;
        background-color: #F5F5F5;
    }

    .csv-tbl .input-sm{
          border-radius: 0;
      }
    .csv-tbl input[type="text"]{
        /*width: 100px;*/
        font-size: 12px;
        padding: 5px;
    }
    .btn-save-csv {
        margin-bottom: 50px;
    }
    .csv-tbl >tbody>tr>td{
        padding:0;
    }
    .csv-tbl .tbl-header{
        font-size: 11px;
    }
    .black-th{
        background-color: #333333;
        color: #fff;
    }
  /*  .black-th.action{
        font-size: 11px;
    }*/
    table .glyphicon-remove{
        text-align: center;
        color: red;
    }
    .glyphicon-remove{
        cursor: pointer;
    }
    label.btn.btn-default.semiBold {
        background-color: #333333;
        color: #fff;
    }
    .torin-calc .nav>li>a{
        padding:15px 15px;
        color: #000;
        font-size: 14px;
        font-weight: bold;
    }
    .torin-calc .nav-tabs>li>a:hover, .torin-calc .nav-tabs>li.active>a{
        color:#fff;
        background-color: #333333;
    }
    .package-table tbody tr:nth-child(odd){background:#f5f4f4;}
    /*.package-table td{border-top:0px !important; border-right:1px solid #fff !important;}*/
    .package-table tbody tr:nth-child(even){background:#ececec; }
    .package-table tbody > tr td {
        font-size: 15px !important;
        font-weight: 600 !important;
        color: #333333;
        padding: 20px 0px 20px 31px !important;
    }
    .package-table > tbody > tr:first-child td {font-weight: normal;}
    .package-table span.glyphicon.glyphicon-edit{
        color:#000;
        font-size: 15px;
    }
    .package-table td, .package-table tr{border: none!important; padding: 5px 10px;}
    .package-table > tbody > tr > td{
        padding: 10px 10px 10px;
        height: 60px;
        vertical-align: middle;
    }
    .torin-calc .view-survey{
        color: #000;
    }

	.torin-calc .view-survey:hover,
    .toric-calc .view-survey:focus {
        text-decoration: none !important;
    }

    table .glyphicon-remove{
        padding-top: 7px;
    }
    .torin-calc .panel{
        border-radius: 0;
    }
    .torin-calc .panel-body {
        padding: 10px;
    }

    .torin-calc .panel-body p{
        margin-bottom: 0 !important;
        font-size: 13px;
    }
    .torin-calc .filter-wrapper .filter-container .filter-item select{
        height: auto;
    }
    .torin-calc .new-quote{
        border-radius: 0;
        min-width: 108px;
        padding: 3px;
        border: 0;
        background-color:#000;
        margin-top: 15px;
        margin-bottom: 15px;
        color:#fff;
    }
    .torin-calc .new-quote:hover{
        color:#fff;
    }
    .torin-calc .asc-btn{
        padding: 0;
        font-size: 13px;
        border-radius: 1px;
        color: #fff;
        border: 0;
        background-color: #000;
        outline:0;
    }
    .torin-calc .asc-btn:focus{
        outline: 0;
    }
    p.table-top-h1{
        text-transform: uppercase;
        font-weight: bold;
        font-size: 11px !important;
        margin-bottom: 3px !important;
        margin-left: 9px;
    }
    select.button_example.form-control{
        border-radius:0;
    }
    p.table-top-h1{
        text-transform: uppercase;
        font-weight: bold;
        font-size: 15px !important;
        margin-bottom: 3px !important;
        margin-left: 9px;
    }
    /*
    table.tablesorter thead tr .header{
        background-image: url(../torin/public/img/bg.png);
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }

    th.header.headerSortUp{
        background-image: url(../torin/public/img/trans-asc.png) !important;
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }
    th.header.headerSortDown{
        background-image: url(../torin/public/img/trans-desc.png) !important;
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }
    */
    th.center{
        text-align: center!important;
    }
    a.final.btn.btn-primary.csv-data:hover,a.initial.btn.btn-primary.csv-data:hover{
        cursor: pointer !important;
    }

    table.tablesorter thead tr .header {
        position: relative;
    }

    table.tablesorter thead tr .header:after {
        background-image: url(../torin/public/img/bg.png);
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
        display: block;
        height: 10px;
        width: 18px;
        content: '';
        position: absolute;
        right: -12px;
        top: 18px;
        /*top: 50%;*/
        /*transform: translateY(-50%);*/
        z-index: 1;
    }
    table.tablesorter thead tr .headerSortUp:after {
        background-image: url(../torin/public/img/trans-asc.png) !important;
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }
    table.tablesorter thead tr .headerSortDown:after {
        background-image: url(../torin/public/img/trans-desc.png) !important;
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }
    table.tablesorter thead tr .header:last-child:after {
        right: 0px;
    }
     table.tablesorter thead tr .header:first-child:after {
        right: -5px;
     }

     table.tablesorter.customer-tbl thead tr .header:first-child:after  {
        right: 0px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(2):after  {
        right: -8px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(3):after  {
        right: -4px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(4):after  {
        right: 0px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(5):after  {
        right: -11px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(6):after  {
        right: 10px;
    }
     table.tablesorter.customer-tbl thead tr .header:nth-child(7):after  {
        right: 3px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(8):after  {
        right: -6px;
     }
     table.tablesorter.customer-tbl thead tr .header:last-child:after  {
       right: 3px;
     }

     /* ADMIN */

     table.tablesorter.admin-tbl thead tr .header:first-child:after  {
        right: -5px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(2):after  {
        right: -10px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(3):after  {
        right: -9px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(4):after  {
        right: -5px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(5):after  {
        right: -4px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(6):after  {
        right: -14px;
    }
     table.tablesorter.admin-tbl thead tr .header:nth-child(7):after  {
        right: 4px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(8):after  {
        right: -3px;
     }
      table.tablesorter.admin-tbl thead tr .header:nth-child(9):after  {
        right: -10px;
     }
     table.tablesorter.admin-tbl thead tr .header:last-child:after  {
       right: -2px;
     }

    /** search input box **/
    .reset-btn{
        width: 8%;
        height:31px;
        float: right;
        display: inline-block;
        margin-right: 36px;
    }
    #search-input {
        height:31px;
        display: inline-block;
    }
    .search_wrapper .form-group{
        width: 350px;
    }
    span.glyphicon.glyphicon-search.form-control-feedback{
        right: 2px;
    }
    .filter-reset-btn{
        margin-left: 7px;
        display: block;
        float: left;
        margin-bottom: 5px;
        line-height: 34px;
        text-align: center;
    }
    .filter-reset-btn a{
        font-size: 12px;
    }
    .filter-reset-btn a:hover{
        color:#000;
        text-decoration: underline;
    }
    #purchased input{
        border-radius: 0px;
        padding: 5px;
        height: 25px;
    }
    #purchased .row{
        margin-bottom: 5px;
    }

    #purchased .btn-primary{
        border-radius: 0;
        min-width: 108px;
        padding: 3px;
        border: 0;
        background-color: #000;
        margin-top: 15px;
        margin-bottom: 15px;
        color: #fff;
        text-transform: uppercase !important;
        padding: 9px 20px !important;
        font-size: 15px;
        font-weight: bold;
        letter-spacing: -0.5px !important;
    }

    .po-error, .so-error{
        /*text-align: center;*/
        margin-top: 4px;
        font-size: 11px !important;
    }
    span#purchase-error{
        color: #FF0004;
    }
    .torin-calc .purchased {
        background-color: #00A82D;
    }
    a.purchased.btn.btn-primary.csv-data:hover{
        cursor: default !important;
    }
    .torin-calc .purchased:hover,
    .toric-calc .purchased:focus {
        text-decoration: none !important;
        background-color: #00A82D;
    }
    .custom {
        background-color: #fb6d29;
    }

    #csv-table-form .table-scrollbar{
        background-color:#fff;
        overflow-y: hidden;
    }
    table#csvtbl{
        width: auto !important;
    }

    #style-3::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 1px rgba(0,0.3,0,0);
        background-color: #F5F5F5;
    }

    #style-3::-webkit-scrollbar
    {
        width: 15px;
        background-color: #F5F5F5;
        height: 15px;
    }
    #style-3::-webkit-scrollbar-thumb{
        background-color: #000;
    }

    .dashboard_content .table-wrapper table tr.row-vm:nth-child(4n+1), .dashboard_content .table-wrapper table tr.row-vm:nth-child(4n+2) {
    border-bottom: 1px solid #dedede !important;
}


.dashboard_content .table-wrapper table tr.row-details {
    text-decoration: none;
}
.dashboard_content .table-wrapper table tr.row-details {
    text-transform: uppercase;
    text-decoration: underline;
    border-top: 1px solid #f5f5f5 !important;
}
.dashboard_content .table-wrapper table tr:nth-child(4n+1), .dashboard_content .table-wrapper table tr:nth-child(4n+2) {
    background: #f6f6f6 !important;
}
.dashboard_content .table-wrapper table tr {
    background: #e4e4e4 !important;
    border-bottom:1px solid #ffffff !important;
}
.form-group {
    margin-bottom :5px;
}
.status {
  cursor: text;
    pointer-events: none;
    border-radius: 0;
    min-width: 108px;
    padding: 7px;
    border: 0;
    letter-spacing: 0.3px;
    color: #fff;
    text-decoration: none;

}
 .processing {
  background-color:#ffd700;
}
.shipped {
  background-color : #5cb85c;
}
.confirmed {
  background-color :#fb6d29;
}
.failed {
  background-color:#ed1c24;
}
.cancelled {

  background-color:#ed1c24;
}
.completed  {
  background-color : #5cb85c;
}
</style>


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

<section class="header-image-new">
  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">
	<?php
		if( is_user_logged_in() ):
	?>
		<div class="bg-side"></div>
		<div class="row">

		  <div class="col-md-3 sidebar_wrap welcome-left">
			<?php
				get_sidebar('menu');
			?>
		  </div>
		  <div class="col-md-9 content_wrap welcome-right">

		  	<?php
					/**
 * My Orders - Deprecated
 *
 * @deprecated 2.6.0 this template file is no longer used. My Account shortcode uses orders.php.
 * @package WooCommerce/Templates
 */

defined( 'ABSPATH' ) || exit;

$my_orders_columns = apply_filters(
	'woocommerce_my_account_my_orders_columns',
	array(
		'order-number'  => esc_html__( 'Order', 'woocommerce' ),
		'order-date'    => esc_html__( 'Date', 'woocommerce' ),
		'order-status'  => esc_html__( 'Status', 'woocommerce' ),
		'order-total'   => esc_html__( 'Total', 'woocommerce' ),
		'order-actions' => '&nbsp;',
	)
);

$customer_orders = get_posts(
	apply_filters(
		'woocommerce_my_account_my_orders_query',
		array(
			'numberposts' => $order_count,
			'meta_key'    => '_customer_user',
			'meta_value'  => get_current_user_id(),
			'post_type'   => wc_get_order_types( 'view-orders' ),
			'post_status' => array_keys( wc_get_order_statuses() ),
		)
	)
);
?>
  <div class="dashboard_content">
  <div class="js-wrap table-wrapper cust-scrollbar table-responsive" id="style-2">
		<?php
			global $wpdb;
		$user_id = get_current_user_id();
			$orders = $wpdb->get_results("SELECT * FROM sparepart_purchases WHERE customer_id = '$user_id' ORDER BY id DESC") ;



 ?>
	<table class="table tablesorter customer-tb" id="tbl" >

		<thead class="thead-inverse">
      <tr>

          <th>ORDER #</th>
          <th>DATE SUBMITTED</th>
          <!-- <th>NO. OF ITEMS</th> -->
          <th  style="text-align:right;">TOTAL &nbsp;&nbsp;</th>
      </tr>
		</thead>

		<tbody id="grid-body">

			<?php
			foreach ( $orders as $order ) :

				?>
			<tr class="row-vm shown">
			<td class="csv-data">  <?php echo $order->order_id; ?></td>
			<td class="csv-data"> <?=date('Y-m-d',strtotime($order->date_created))?> </td>
			<td class="csv-data"  style="text-align:right;">$ <?=number_format(($order->total_amount + $order->cc_fee),2)?></td>
		</tr>
		<tr class="row-details">
			<td class="status-button-wrap">
			 <!--Processing(Yellow) / Confirmed (Orange) / Shipped (Green)-->
				<?php
					$status = strtolower( $order->order_status);
				?>
                <?php if ( @$order->sap_id ) : ?>
                    <button class="status completed">PROCESSED</button><span style="display:inline-block;margin-left:10px;">REF#: <?php echo $order->sap_id; ?></span>
                <?php else : ?>
                    <button class="status <?=$status ?>"><?=strtoupper($status)?></button>
                <?php endif; ?>
			</button>
			 </td>
			<td></td>


			<td  style="text-align:right;"><a href="/view-orders?id=<?php echo $order->order_id; ?>"  style="color:#000;text-decoration:none;">VIEW</a></td>
		</tr>



			<?php endforeach; ?>
		</tbody>
	</table>
  </div>
  </div>
	<br>
	</div>

		  </div>

		</div>

	<?php else : ?>

		<?php get_template_part('restricted-error'); ?>

	<?php endif; ?>
  </div>
</section>

<?php get_footer(); ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
