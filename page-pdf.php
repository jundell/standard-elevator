<?php 

// Template Name: PDF Viewer

get_header(); 

?>

<section class="header-image-new">

  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>

<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2">
        <?php
          // Start the loop.
          while ( have_posts() ) : the_post();
          the_content();
          // End the loop.
          endwhile;
        ?>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>