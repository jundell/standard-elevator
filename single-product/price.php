<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;



?>
<?php
global $wpdb;
$user_id = get_current_user_id();
$key = 'price_level';
$single = true;
$price_level = get_user_meta( $user_id, $key, $single );
$name= $product->name;
$new_price = $wpdb->get_row("SELECT * FROM wp_pricing_table WHERE machines = '$name'") ;

								               	$price = 0 ;

			  					 		/*if($price_level == 'b4' || $price_level=='B4 Price') {
			  					 			$price = $new_price->b4_price;
			  					 		}
			  					 		else */if($price_level=='otis' || $price_level =='Otis' || $price_level == 'Otis Price') {
			  					 			$price = $new_price->otis_price;
			  					 		}
			  					 		else {
			  					 		  $price = $new_price->general_price;
			  					 		}


?>
<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price' ) );?>">$ <?php echo number_format($price,2); ?> </p>
