<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>
<style>
.single_add_to_cart_button{

background: #333333 !important;
}
.onsale{
display:none !important;
}
.product .stock {
	display: none;
}
.product.show-stock .stock {
	display: block;
}
.woocommerce-breadcrumb {
	display: none;
}
.wp-post-image {
  width: 100% !important;
	height:auto !important;
}

#tab-description {
	display:none !important;
}
.wc-tabs {
	display:none !important;
}
.product_title  {
	font-size: 20px !important;
}

.single_add_to_cart_button {color: #fff !important;
    background-color: #5cb85c !important;
    border-color: #4cae4c !important;
	text-transform : uppercase;
	border-radius:0px !important;


}
.button.view-details {
    color: #fff !important;
    background-color: #31b0d5 !important;
    border-color: #31b0d5 !important;
    text-transform: uppercase;
    font-size: 14px !important;
    border-radius: 0px !important;
    font-weight: normal !important;
}
.product_meta {
font-size: 13px !important;
}
.size-woocommerce_thumbnail {
	width:80px !important;
	height:80px !important;
}
.woocommerce-placeholder {
	width:80px !important;
	height:80px !important;
}
.ajax_add_to_cart {
	color: #fff !important;
    background-color: #5cb85c !important;
    border-color: #4cae4c !important;
	text-transform : uppercase;
	font-size:14px  !important;
	border-radius:0px !important;

}
.woocommerce-loop-product__title {
font-size: 12px !important;
}
.input-text {

	    display: block;
	    width: 100%;
	    height: 34px;
	    padding: 6px 12px;
	    font-size: 14px;
	    line-height: 1.42857143;
	    color: #555;
	    background-color: #fff;
	    background-image: none;
	    border: 1px solid #ccc;
	    border-radius: 4px;
	    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
	    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
	    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
	    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;

}
.add-to-cart-button a {
    padding: 10px;
    border: #446084 1.5px solid;
    color: #446084;
    text-transform: uppercase;
    font-style: bold;
    font-weight: bold;
}
.woocommerce-page div.product div.images {
    float: left;
    width: 200px;
    clear: none;
}
.woocommerce #content div.product div.summary, .woocommerce div.product div.summary, .woocommerce-page #content div.product div.summary, .woocommerce-page div.product div.summary {
    margin-left: 230px !important;
    display: block;
    position: relative;
    float: none;
    width: auto;
}
.woocommerce div.product form.cart::after {
	display: none;
}
.woocommerce div.product form.cart .button {
	float: none;
}
figure.woocommerce-product-gallery__wrapper:after {
    content: "*Shown for reference only, actual product may vary";
    display: block;
    font-size: 12px;
    margin-top: 6px;
    padding: 0 30px 0 7px;
    text-indent: -5px;
}
.related  {
    margin-top: 0px !important;
}
</style>
<section class="header-image-new">
  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?> </h1>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">

	<?php
		if( is_user_logged_in() ):
	?>
	<div class="bg-side"></div>
		<div class="row">

		  <div class="col-md-3 sidebar_wrap welcome-left">
			<?php
				get_sidebar('menu');
			?>
		  </div>
		  <div class="col-md-9 content_wrap welcome-right">
				<div class="row"><div class="col-md-6">
					<div class="add-to-cart-button pull-left"><a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>"><?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?> - <?php echo WC()->cart->get_cart_total(); ?> </a></div>

				</div>
				<div class="col-md-6">
					<a href="/spare-parts" class="btn btn-success pull-right" style="background:#00a72e !important;color:#ffffff;"><i class="fa fa-arrow-left"> </i> Continue Shopping</a>


				</div>
				</div>
	<hr>

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>
		  </div>

		</div>




<?php else : ?>

		<?php get_template_part('restricted-error'); ?>

	<?php endif; ?>
  </div>
</section>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<?php get_footer(); ?>

<script>

$(function(){
	$('.woocommerce-product-gallery__image a').attr('data-fancybox','gallery');
	var product_name = $('.product_title').text();
	var stock = $('.stock').first().text();
	var stockQuantity = 0 ;
	$('.product').addClass('show-stock');
	$('.stock').html('Checking Stock...');
	if(stock < 1) {
		$("<p class='stock'>Checking Stock...</p>").insertAfter(".woocommerce-product-details__short-description");
		// $( "<p class='stock text-danger' style='color:red;'>Back Order</p>" ).insertAfter( ".woocommerce-product-details__short-description" );
	}
	$.ajax({
		type: "post",
		dataType: "json",
		url : "<?php echo admin_url('admin-ajax.php'); ?>",
		data : {action : "get_stock" , product_name : product_name},
		success: function(response){
				console.log(response);
			stockQuantity = response ;
			$('.stock').html('<span class="text-success">'+stockQuantity+' in stock</span>');
			if(stockQuantity < 1) {
				$('.stock').html('<span class="text-danger">Back Order</span>');
			}
		}
	});

			console.log(stockQuantity);
			console.log(stock) ;
	$('[name=quantity]').on('blur',function() {
		console.log($(this).val());
		var usr_qty = $(this).val();
		console.log(usr_qty, stockQuantity);
		if(usr_qty > stockQuantity) {
			$('.stock').html('<span class="text-danger">Back Order</span>');
		} else {
			$('.stock').html('<span class="text-success">'+stockQuantity+'</span>');
		}

	});


});



</script>


/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
