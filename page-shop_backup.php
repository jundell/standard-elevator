<?php 
/* Template Name: Shop Page */
	get_header();
?>
<?php 
global $wpdb; 
$user_id = get_current_user_id(); 
$key = 'price_level'; 
$single = true; 
$price_level = get_user_meta( $user_id, $key, $single ); 
		


?>
<style>
.page-numbers {
border: none !important;
}
 .page-numbers a:link,  .page-numbers a:visited {
display: block !important;
background: #77a464 !important;
color: #363636 !important;
padding: 10px 14px !important;
}
.page-numbers a:link,  .page-numbers a:hover {
display: block !important;
background: #fff !important;
color: #363636 !important;
}
 .page-numbers .current,  .page-numbers li a:hover {
padding: 10px 14px !important;
background: #77A464 !important;
color: #fff !important;
}
.page-numbers > ul {
 display:block;
  list-style:none;
}
.page-numbers ul > li {
  display:inline-block;
}
.content_wrap ul li {
 display:inline-block;
 margin : 0px;
}
.content_wrap ul {
 display : block;
 list-style:none;

}
.product-box {
	position: relative;
	width: 100%;
	height: 30%;
	display: flex;
	align-items: center;
	border: 1px solid #446084;
	margin-top: 25x;
	margin-bottom: 25px;
    font-size: 1.1em;
 border-radius : 5px;
box-shadow: 5px 10px #fff;
}
.add-to-cart-button a {
	padding:10px;
	border:#446084 1.5px solid;
	color : #446084 ;
	text-transform : uppercase;
	font-style:bold;
	font-weight:bold;
}
.add-to-cart-button a:hover{
	background-color:#446084;
	color : #ffffff;
}
.wc-proceed-to-checkout a {
	padding:10px;
	border:#446084 1.5px solid;
	color : #446084 ;
	text-transform : uppercase;
	font-style:bold;
	font-weight:bold;
}
.filters {
display:block;
list-style:none;
}
.filters .filter-item {

display:inline-block;
}
.title-wrapper .category > p {
	
	padding: 0px;
   	margin: 0px;
    	text-transform: uppercase !important;;
}
.box-image{
direction: ltr;
    display: table-cell;
    vertical-align: middle;
 position:relative;	
}
.box-image img {
    max-width: 100%;
    width: 100%;
    transform: translateZ(0);
    margin: 0 auto;
    min-width: 247px!important;
    width: 247px!important;
}

.product-details {
 margin-left:10px;
	padding-top: .7em;
    padding-bottom: 1.4em;
    position: relative;
    width: 100%;
    font-size: .9em;
 
}
img {
    max-width: 100%;
    height: auto;
    display: inline-block;
    vertical-align: middle;
}
.product-title {
color: #446084 !important;
}
.is-small, .is-small.button {
    font-size: .8em;
}
.widget_shopping_cart_content .blockUI.blockOverlay,
.woocommerce-checkout-review-order .blockUI.blockOverlay {
    background-color: white !important;
    opacity: 0.6 !important
}

.widget_shopping_cart_content .blockUI.blockOverlay::before,
.woocommerce-checkout-review-order .blockUI.blockOverlay::before {
    display: block;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-left: -0.5em;
    margin-top: -0.5em;
    content: '';
    line-height: 1;
    text-align: center;
    font-size: 2em;
    border-top: 3px solid rgba(0, 0, 0, 0.1) !important;
    border-right: 3px solid rgba(0, 0, 0, 0.1) !important;
    border-bottom: 3px solid rgba(0, 0, 0, 0.1) !important;
    pointer-events: none;
    border-left: 3px solid #446084;
    animation: spin .6s infinite linear;
    border-radius: 50%;
    width: 30px;
    height: 30px
}

.category-page-row {
    padding-top: 30px
}

.price_slider_amount input {
    display: none
}

.woocommerce-result-count {
    display: inline-block;
    margin: 0 1em 0 auto
}

.woocommerce-ordering,
.woocommerce-ordering select {
    margin: 5px 0;
    display: inline-block
}

.add_to_cart_button.added {
    display: none
}

a.added_to_cart {
    display: inline-block;
    font-size: .9em;
    padding: 10px 0;
    text-transform: uppercase;
    font-weight: bold
}

a.added_to_cart:after {
    content: " →"
}

.grid-style-3 .title-wrapper {
    -ms-flex: 1;
    flex: 1;
    padding-right: 15px;
    min-width: 60%;
    overflow: hidden;
    text-overflow: ellipsis
}

.grid-style-3 .price-wrapper {
    text-align: right
}

.grid-style-3 .star-rating {
    margin: 0.2em 0;
    text-align: right
}

.grid-style-3 .price del {
    display: block
}

.grid-style-3 .price del span.amount {
    margin: 0
}

.products .box-vertical .box-text {
    font-size: 1.1em
}

.page-numbers.button.current {
    pointer-events: none;
    opacity: .6
}

.grid-tools {
    display: -ms-flexbox;
    display: flex;
    width: 100%;
    padding: 0 !important;
    margin-bottom: -1px;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    white-space: nowrap
}

.grid-tools a {
    text-overflow: ellipsis;
    opacity: .95;
    display: block;
    width: 100%;
    padding: .4em 0 .5em;
    font-size: .85em;
    font-weight: bold;
    text-transform: uppercase;
    background-color: #446084;
    color: #f1f1f1;
    transition: opacity .3s
}

.grid-tools a+a {
    border-left: 1px solid rgba(255, 255, 255, 0.1)
}

.grid-tools a:hover {
    color: #FFF;
    opacity: 1
}

.grid-tools .add-to-cart-grid {
    width: 0
}

@media (max-width: 849px) {
    .category-filter-row {
        padding: 10px 0
    }
}

.filter-button {
    display: inline-block;
    margin-top: .5em
}

.box-image .out-of-stock-label {
    color: #333;
    font-weight: bold;
    text-transform: uppercase;
    position: absolute;
    top: 40%;
    left: 0;
    right: 0;
    background: #fff;
    padding: 20px 0;
    background: rgba(255, 255, 255, 0.9);
    text-align: center;
    opacity: .9
}

.featured-title .woocommerce-result-count {
    display: none
}

.widget_product_categories>ul>li {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-flow: row wrap;
    flex-flow: row wrap;
    -ms-flex-align: center;
    align-items: center
}

.widget_product_categories>ul>li span {
    font-size: .85em;
    opacity: .4
}

.widget_product_categories>ul>li ul span.count {
    display: none
}

.message-wrapper+.login {
    padding: 30px;
    background-color: rgba(0, 0, 0, 0.03)
}

.woocommerce-form-login .button {
    margin-bottom: 0
}

.woocommerce-billing-fields {
    padding-top: 15px;
    border-top: 2px solid #ddd
}

.woocommerce-billing-fields p {
    margin-bottom: .5em
}

form.checkout h3 {
    font-size: 1.1em;
    overflow: hidden;
    padding-top: 10px;
    font-weight: bolder;
    text-transform: uppercase
}

form.checkout input[type="checkbox"] {
    margin-top: 0;
    margin-bottom: 0;
    margin-right: .5em
}

.payment_methods li+li {
    padding-top: 7px;
    border-top: 1px solid #ececec
}

.payment_methods p {
    font-size: .9em
}

.payment_method_paypal img {
    max-width: 130px;
    margin: 0 10px
}

a.about_paypal {
    font-size: .8em
}

.woocommerce-privacy-policy-text {
    font-size: 85%
}

p.form-row-wide {
    clear: both
}

p.form-row-push {
    margin-top: -15px
}

@media (min-width: 550px) {
    p.form-row-first,
    p.form-row-last {
        width: 48%;
        float: left
    }
    p.form-row-first {
        margin-right: 4%
    }
}

input#place_order {
    font-size: 1.2em;
    white-space: normal;
    line-height: 1.2;
    padding-top: .5em;
    padding-bottom: .5em
}

#ship-to-different-address {
    padding-top: 0
}

#ship-to-different-address label {
    text-transform: none;
    font-weight: normal
}

#billing_address_2_field>label {
    width: 0;
    opacity: 0;
    overflow: hidden;
    white-space: nowrap
}

@media (max-width: 549px) {
    #billing_address_2_field>label {
        display: none
    }
}

.wc-terms-and-conditions {
    margin-top: -15px;
    border-top: 1px solid #ececec;
    padding: 15px 0
}

.wc-terms-and-conditions input {
    margin-bottom: 0
}

.wc-terms-and-conditions label {
    font-weight: normal
}

div.create-account {
    clear: both
}

.form-row.create-account {
    font-size: 1.1em;
    margin: 0
}

.form-row.create-account label {
    font-weight: normal
}

.page-checkout-simple {
    padding: 3% 0
}

.js_active .woocommerce-account-fields p.create-account+div.create-account,
.js_active .woocommerce-shipping-fields #ship-to-different-address+div.shipping_address {
    display: none
}

.widget_price_filter form {
    margin: 0
}

.widget_price_filter .price_slider {
    margin-bottom: 1em;
    background: #f1f1f1
}

.widget_price_filter .price_label {
    padding-top: 6px
}

.widget_price_filter span {
    font-weight: bold
}

.widget_price_filter .price_slider_amount {
    text-align: right;
    line-height: 1;
    font-size: .8751em
}

.widget_price_filter .price_slider_amount .button {
    border-radius: 99px;
    background-color: #666;
    float: left;
    font-size: .85em
}

.widget_price_filter .ui-slider {
    position: relative;
    text-align: left
}
a.added_to_cart {
    display: inline-block;
    font-size: .9em;
    padding: 10px;
    text-transform: uppercase;
    font-weight: bold;
}
.widget_price_filter .ui-slider .ui-slider-handle {
    position: absolute;
    z-index: 2;
    width: 15px;
    height: 15px;
    cursor: pointer;
    outline: none;
    top: -5px;
    border-radius: 999px;
    background-color: #666
}

.widget_price_filter .ui-slider .ui-slider-handle:last-child {
    margin-left: -10px
}

.widget_price_filter .ui-slider .ui-slider-range {
    position: absolute;
    opacity: .5;
    border-radius: 99px;
    z-index: 1;
    font-size: 10px;
    display: block;
    border: 0;
    background-color: #666
}

.widget_price_filter .ui-slider-horizontal {
    height: 5px;
    border-radius: 99px
}

.widget_price_filter .ui-slider-horizontal .ui-slider-range {
    top: 0;
    height: 100%
}

.widget_price_filter .ui-slider-horizontal .ui-slider-range-min {
    left: -1px
}

.widget_price_filter .ui-slider-horizontal .ui-slider-range-max {
    right: -1px
}

.widget li.chosen a:before {
    content: 'x';
    display: inline-block;
    opacity: .6;
    color: currentColor;
    margin-right: 5px
}

.wc-layered-nav-term.chosen>a:before {
    background-color: #f1f1f1;
    border: 1px solid rgba(0, 0, 0, 0.1);
    line-height: 12px;
    width: 18px;
    height: 18px;
    text-align: center;
    border-radius: 99px
}

.widget_layered_nav_filters ul li.chosen {
    display: inline-block;
    margin-right: 10px;
    border: 0 !important
}

.widget_layered_nav_filters ul li.chosen a {
    display: inline-block;
    background-color: #f1f1f1;
    border: 1px solid rgba(0, 0, 0, 0.1);
    border-radius: 99px;
    opacity: .9;
    padding: 0 7px;
    font-size: .85em;
    font-weight: normal
}

.dark .widget_layered_nav_filters ul li.chosen a {
    color: #FFF;
    background-color: rgba(0, 0, 0, 0.5)
}

.widget_layered_nav_filters ul li.chosen a:before {
    content: 'x';
    opacity: .4;
    margin-right: 3px
}

.widget_layered_nav_filters ul li.chosen a:hover:before {
    opacity: 1
}

.woocommerce-product-gallery figure {
    margin: 0
}

.message-wrapper {
    margin: 0;
    padding-bottom: .5em
}

#wrapper>.message-wrapper {
    padding-top: .75em;
    padding-bottom: .75em;
    margin-bottom: 10px;
    font-size: 1.1em
}

ul.message-wrapper li {
    list-style: none
}

.message-container span {
    font-weight: bold
}

.message-container .wc-forward {
    display: none
}

.message-container a {
    margin: 0 15px 0 0
}

.container .message-container {
    padding-left: 0;
    padding-right: 0
}

.message-wrapper+main .product-main {
    padding-top: 0
}

.demo_store {
    padding: 5px;
    margin: 0;
    text-align: center;
    background-color: #000;
    color: #FFF
}

.has-transparent+main>.message-wrapper {
    position: fixed;
    z-index: 999;
    width: 100%;
    bottom: 0;
    background-color: #FFF;
    box-shadow: 1px 1px 10px 1px rgba(0, 0, 0, 0.1)
}

.form-row input[type="submit"] {
    margin: 0
}

.form-row input[type="submit"]+label {
    margin-left: 15px
}

.my-account-header.featured-title .page-title-inner {
    min-height: 100px
}

.my-account-header .button {
    margin-top: 5px;
    margin-bottom: 5px
}

.woocommerce-form-register .woocommerce-privacy-policy-text {
    margin-bottom: 1.5em
}

form.lost_reset_password {
    padding: 30px 0
}

.dashboard-links {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    line-height: 1;
    font-size: 1.3em;
    list-style: none
}

.dashboard-links a {
    display: block;
    border-radius: 5px;
    padding: 20px 0;
    border: 1px solid #ddd;
    text-align: center;
    margin-right: 10px;
    transition: all .3s
}

.dashboard-links .active {
    display: none
}

.dashboard-links a:hover {
    background-color: #333;
    color: #FFF
}

.dashboard-links li {
    width: 33.333%
}

@media (max-width: 849px) {
    .dashboard-links li {
        width: 50%
    }
}

@media (max-width: 549px) {
    .dashboard-links li {
        width: 100%
    }
}

.price {
    line-height: 1
}

.product-info .price {
    font-size: 1.5em;
    margin: .5em 0;
    font-weight: bolder
}

.woocommerce-variation-price {
    border-top: 1px dashed #ddd;
    font-size: .8em;
    padding: 7.5px 0
}

.price-wrapper .price {
    display: block
}

span.amount {
    white-space: nowrap;
    color: #111;
    font-weight: bold
}

.dark .price,
.dark span.amount {
    color: #FFF
}

.header-cart-title span.amount {
    color: currentColor
}

del span.amount {
    opacity: .6;
    font-weight: normal;
    margin-right: .3em
}

.no-prices .amount {
    display: none !important
}

ul.product_list_widget li {
    list-style: none;
    padding: 10px 0 5px 75px;
    min-height: 80px;
    position: relative;
    overflow: hidden;
    vertical-align: top;
    line-height: 1.33
}

ul.product_list_widget li+li {
    border-top: 1px solid #ececec
}

.dark ul.product_list_widget li {
    border-color: rgba(255, 255, 255, 0.2)
}

.widget_shopping_cart ul.product_list_widget li {
    padding-right: 30px
}

ul.product_list_widget li>span.reviewer {
    font-size: .8em
}

ul.product_list_widget li a:not(.remove) {
    display: block;
    margin-bottom: 5px;
    padding: 0;
    overflow: hidden;
    text-overflow: ellipsis;
    line-height: 1.3
}

ul.product_list_widget li a.remove {
    position: absolute;
    right: 0px;
    z-index: 9
}

ul.product_list_widget li dl {
    margin: 0;
    line-height: 1;
    font-size: .7em
}

ul.product_list_widget li img {
    top: 10px;
    position: absolute;
    left: 0;
    width: 60px;
    height: 60px;
    margin-bottom: 5px;
    object-fit: cover;
    object-position: 50% 50%
}

ul.product_list_widget li .quantity {
    display: block;
    margin-top: 3px;
    font-size: .85em;
    opacity: 0.6
}

.product-main {
    padding: 40px 0
}

.page-title:not(.featured-title)+main .product-main {
    padding-top: 15px
}

.product-info {
    padding-top: 10px
}

.product-summary .woocommerce-Price-currencySymbol {
    font-size: .75em;
    vertical-align: top;
    display: inline-block;
    margin-top: .05em
}

.product-summary .quantity {
    margin-bottom: 1em
}

.product-summary .variations_button {
    padding: .5em 0
}

.product-summary table tr+tr {
    border-top: 1px dashed #ddd
}

.product_meta {
    font-size: .8em;
    margin-bottom: 1em
}

.product_meta>span {
    display: block;
    border-top: 1px dotted #ddd;
    padding: 5px 0
}

.product-info p.stock {
    margin-bottom: 1em;
    line-height: 1.3;
    font-size: .8em;
    font-weight: bold
}

p.in-stock {
    color: #7a9c59
}

.group_table .quantity {
    margin: 0
}

.group_table .price {
    font-size: 1em
}

.group_table .label label {
    padding: 0;
    margin: 0
}

.product-gallery,
.product-thumbnails .col {
    padding-bottom: 0 !important
}

.product-thumbnails img,
.product-gallery-slider img {
    width: 100%
}

.product-gallery-wide {
    position: relative
}
p.table-top-h1 {
    text-transform: uppercase;
    font-weight: bold;
    font-size: 15px !important;
    margin-bottom: 3px !important;
    margin-left: 9px;
}
@media screen and (min-width: 850px) {
    .product-gallery-stacked {
        white-space: normal !important;
        overflow: auto !important;
        width: auto !important
    }
    .product-gallery-stacked .flickity-slider,
    .product-gallery-stacked .flickity-viewport {
        height: auto !important
    }
    .product-gallery-stacked .slide,
    .product-gallery-stacked .flickity-slider {
        position: relative !important;
        -ms-transform: none !important;
        transform: none !important;
        left: 0 !important;
        right: 0 !important
    }
    .product-gallery-stacked .slide {
        overflow: hidden
    }
    .product-gallery-stacked .slide:not(:last-child) {
        margin-bottom: 1.5em
    }
    .product-stacked-info {
        padding: 5vh 5% 2vh
    }
}

.product-thumbnails {
    padding-top: 0
}

.product-thumbnails a {
    overflow: hidden;
    display: block;
    border: 1px solid transparent;
    background-color: #FFF;
    -ms-transform: translateY(0);
    transform: translateY(0)
}

.product-thumbnails a:hover,
.product-thumbnails .is-nav-selected a {
    border-color: rgba(0, 0, 0, 0.2)
}

.product-thumbnails img {
    margin-bottom: -5px;
    opacity: 0.5;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    transition: transform 0.6s, opacity 0.6s
}

.product-thumbnails a:hover img,
.product-thumbnails .is-nav-selected a img {
    border-color: rgba(0, 0, 0, 0.3);
    -ms-transform: translateY(-5px);
    transform: translateY(-5px);
    opacity: 1
}

.vertical-thumbnails .row-slider:not(.flickity-enabled) {
    opacity: 0
}

@media screen and (min-width: 850px) {
    .vertical-thumbnails {
        overflow-x: hidden;
        overflow-y: auto
    }
    .vertical-thumbnails .col {
        position: relative !important;
        left: 0 !important;
        max-width: 100% !important;
        min-height: 0 !important;
        margin-left: 1px;
        width: 95% !important;
        right: 0 !important;
        padding: 0 0 15px !important
    }
    .vertical-thumbnails .flickity-slider,
    .vertical-thumbnails .flickity-viewport {
        -ms-transform: none !important;
        transform: none !important;
        overflow: visible !important;
        height: auto !important
    }
}

.product-footer .woocommerce-tabs {
    padding: 30px 0;
    border-top: 1px solid #ececec
}

.product-footer .woocommerce-tabs>.nav-line-grow,
.product-footer .woocommerce-tabs>.nav-line:not(.nav-vertical) {
    margin-top: -31px
}

#product-sidebar .next-prev-thumbs {
    margin: -.5em 0 3em
}

.product-sidebar-small {
    font-size: .9em
}

.product-sidebar-small .widget-title {
    text-align: center
}

.product-sidebar-small .is-divider {
    margin-left: auto;
    margin-right: auto
}

.product-sidebar-small ul.product_list_widget li {
    padding-left: 60px
}

.product-sidebar-small ul.product_list_widget li img {
    width: 50px;
    height: 50px
}

.product-section {
    border-top: 1px solid #ececec
}

.easyzoom-notice {
    display: none
}

.easyzoom-flyout {
    position: absolute;
    z-index: 1;
    overflow: hidden;
    background: #fff;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    pointer-events: none;
    transition: opacity 1s;
    animation-delay: 1s;
    animation: stuckFadeIn .6s
}

@media (max-width: 849px) {
    .easyzoom-flyout {
        display: none !important
    }
}

.product-gallery-slider .slide .easyzoom-flyout img {
    max-width: 1000px !important;
    width: 1000px !important
}

.woocommerce-product-gallery__trigger {
    display: none
}

.product-info .composite_form .composite_navigation .page_button {
    font-size: 1em;
    line-height: 1.2;
    font-weight: normal
}

.woocommerce-pagination ul.links li {
    margin-left: inherit
}

.has-sticky-product-cart {
    padding-bottom: 60px
}

.has-sticky-product-cart .back-to-top.active {
    bottom: 10px
}

.sticky-add-to-cart__product {
    display: none;
    -ms-flex-align: center;
    align-items: center;
    padding: 3px
}

.sticky-add-to-cart__product .product-title-small {
    margin-right: 1em;
    max-width: 180px;
    line-height: 1
}

.sticky-add-to-cart__product img {
    width: 45px;
    height: 45px;
    object-fit: cover;
    object-position: 50% 50%;
    margin-right: 1em;
    border-radius: 5px
}

.sticky-add-to-cart--active {
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 20;
    display: -ms-flexbox;
    display: flex;
    padding: 3px;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex-pack: center;
    justify-content: center;
    font-size: .9em;
    background-color: rgba(255, 255, 255, 0.9);
    border-top: 1px solid #ddd;
    animation: stuckMoveUp .6s
}

.sticky-add-to-cart--active .woocommerce-variation-description,
.sticky-add-to-cart--active .variations {
    display: none
}

.sticky-add-to-cart--active .woocommerce-variation-add-to-cart,
.sticky-add-to-cart--active .single_variation_wrap {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex-wrap: no-wrap;
    flex-wrap: no-wrap
}

.sticky-add-to-cart--active .woocommerce-variation-price,
.sticky-add-to-cart--active .product-page-price {
    margin-top: 0;
    margin-right: 0.9em;
    margin-bottom: 0;
    font-size: 15px;
    padding: 0;
    border: 0
}

.sticky-add-to-cart--active .quantity,
.sticky-add-to-cart--active form,
.sticky-add-to-cart--active button {
    margin-bottom: 0
}

.sticky-add-to-cart--active .sticky-add-to-cart__product {
    display: -ms-flexbox;
    display: flex
}

@media (max-width: 550px) {
    .sticky-add-to-cart--active {
        font-size: .8em
    }
}

.flex-viewport {
    max-height: 2000px;
    transition: all 1s ease;
    cursor: pointer
}

.flex-viewport a� {
    display: block
}

.flex-viewport img {
    width: 100%
}

.flex-control-thumbs {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin: 10px -5px 0 -5px
}

.flex-control-thumbs li {
    list-style: none;
    width: 25%;
    padding: 0 5px;
    cursor: pointer
}

.flex-control-thumbs li img {
    opacity: .6
}

.flex-control-thumbs li img.flex-active {
    opacity: 1
}

.text-center .quantity,
.quantity {
    opacity: 1;
    display: inline-block;
    display: -ms-inline-flexbox;
    display: inline-flex;
    margin-right: 1em;
    white-space: nowrap;
    vertical-align: top
}

.text-center .button+.quantity,
.button+.quantity {
    margin-right: 0
}

.quantity+.button {
    margin-right: 0;
    font-size: 1em
}

.quantity .minus {
    border-right: 0 !important;
    border-top-right-radius: 0 !important;
    border-bottom-right-radius: 0 !important
}

.quantity .plus {
    border-left: 0 !important;
    border-top-left-radius: 0 !important;
    border-bottom-left-radius: 0 !important
}

.quantity .minus,
.quantity .plus {
    padding-left: 0.5em;
    padding-right: 0.5em
}

.quantity input {
    padding-left: 0;
    padding-right: 0;
    display: inline-block;
    vertical-align: top;
    margin: 0
}

.quantity input[type="number"] {
    max-width: 2.5em;
    width: 2.5em;
    text-align: center;
    border-radius: 0 !important;
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    font-size: 1em
}

@media (max-width: 549px) {
    .quantity input[type="number"] {
        width: 2em
    }
}

.quantity input[type="number"]::-webkit-outer-spin-button,
.quantity input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0
}

.cart-icon {
    display: inline-block
}

.cart-icon strong {
    border-radius: 0;
    font-weight: bold;
    margin: .3em 0;
    border: 2px solid #446084;
    color: #446084;
    position: relative;
    display: inline-block;
    vertical-align: middle;
    text-align: center;
    width: 2.2em;
    height: 2.2em;
    font-size: 1em;
    line-height: 1.9em;
    font-family: Helvetica, Arial, Sans-serif
}

.cart-icon strong:after {
    transition: height .1s ease-out;
    bottom: 100%;
    margin-bottom: 0;
    margin-left: -7px;
    height: 8px;
    width: 14px;
    left: 50%;
    content: ' ';
    position: absolute;
    pointer-events: none;
    border: 2px solid #446084;
    border-top-left-radius: 99px;
    border-top-right-radius: 99px;
    border-bottom: 0
}

.current-dropdown .cart-icon strong,
.cart-icon:hover strong {
    background-color: #446084;
    color: #fff
}

.current-dropdown .cart-icon strong:after,
.cart-icon:hover strong:after {
    height: 10px
}

.nav-small .cart-icon {
    font-size: .66em
}

.nav-dark .cart-icon strong {
    color: #fff;
    border-color: #fff
}

.nav-dark .current-dropdown .cart-icon strong,
.nav-dark .cart-icon:hover strong {
    background-color: #fff;
    color: #446084
}

.nav-dark .cart-icon strong:after {
    border-color: #fff
}

.loading .cart-icon strong,
.loading .cart-icon strong:after {
    border-color: #7a9c59;
    color: #7a9c59
}

.loading .cart-icon:hover strong {
    background-color: #7a9c59;
    color: #FFF
}

.header-cart-icon {
    position: relative
}

@media (min-width: 850px) {
    .off-canvas .off-canvas-cart {
        width: 320px
    }
}

.cross-sells h2 {
    font-size: 1.2em;
    margin-bottom: 1em
}

.shop_table ul li,
.checkout ul li {
    list-style: none;
    margin: 0
}

.shop_table .quantity {
    margin: 0
}

td.product-name {
    word-break: break-word;
    text-overflow: ellipsis
}

td.product-thumbnail {
    min-width: 60px;
    max-width: 90px;
    width: 90px
}

td.product-remove {
    width: 20px;
    padding: 0
}

.shop_table tfoot th {
    font-size: 85%;
    text-transform: inherit;
    letter-spacing: 0
}

td.product-total,
.shop_table tfoot tr td,
.cart_totals tbody tr td,
.shop_table thead tr th:last-of-type,
.shop_table tr td:last-of-type {
    text-align: right
}

.shop_table thead th,
.shop_table .order-total td,
.shop_table .order-total th {
    border-width: 3px
}

.shop_table th:last-child {
    border-right: 0
}

.shop_table .cart_item td {
    padding-top: 15px;
    padding-bottom: 15px
}

.shop_table .actions {
    border: 0;
    padding: 15px 0 10px
}

.shop_table .submit-col {
    padding-left: 30px
}

@media (max-width: 849px) {
    .shop_table {
        font-size: .9em
    }
    .shop_table tr.shipping th {
        width: 50%
    }
    .shop_table .product-name {
        min-width: 80px
    }
    .shop_table .product-remove {
        position: relative;
        width: 0
    }
    .shop_table .product-remove a {
        position: absolute;
        top: 10px;
        left: 0px;
        width: 24px;
        height: 24px;
        line-height: 18px !important;
        font-size: 18px !important
    }
    .shop_table .mobile-product-price {
        margin: .5em 0
    }
}

@media (max-width: 549px) {
    .shop_table .product-price {
        display: none
    }
    .shop_table .product-subtotal {
        display: none
    }
    .product-quantity {
        text-align: right
    }
    .cross-sells {
        overflow: hidden
    }
}

.cart_totals tbody th {
    font-size: .9em;
    text-transform: inherit;
    letter-spacing: 0;
    font-weight: normal
}

.cart_totals>h2 {
    display: none
}

.cart_totals .button {
    min-width: 100%;
    margin-right: 0;
    display: block
}

.cart_totals .wc-proceed-to-checkout {
    margin: 1.5em 0
}

.shipping__table {
    margin: 0
}

.shipping__inner {
    border: 0;
    padding: 0;
    font-size: 1em
}

.shipping__list {
    margin-bottom: 0
}

.shipping__list_item {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    min-height: 2em
}

.shipping__list_label {
    font-weight: normal;
    margin: 0;
    padding: .5em 0;
    opacity: .8;
    -ms-flex: 1;
    flex: 1
}

.shipping.shipping--boxed .shipping__list {
    padding: 0
}

.shipping.shipping--boxed .shipping__list_item {
    background-color: rgba(0, 0, 0, 0.01);
    border: 1px solid rgba(0, 0, 0, 0.1);
    padding: .3em .6em;
    border-radius: 3px;
    margin-bottom: 5px;
    min-height: 2.4em
}

.shipping__table--multiple {
    display: block;
    text-align: left
}

.shipping__table--multiple tbody,
.shipping__table--multiple tr,
.shipping__table--multiple th,
.shipping__table--multiple td {
    display: block;
    text-align: left;
    padding-left: 0
}

.shipping__table--multiple th {
    border: 0
}

.shipping__table--multiple .shipping__list {
    padding: 0 .2em
}

.shipping__table--multiple .woocommerce-shipping-calculator,
.shipping__table--multiple .woocommerce-shipping-destination {
    text-align: left
}

.shipping__table--multiple .shipping__list_label {
    text-align: left
}

tr.shipping input:checked+label,
tr.shipping input:hover+label,
tr.shipping label:hover {
    opacity: 1
}

tr.shipping input:checked+label {
    font-weight: bold
}

tr.shipping input {
    margin-bottom: 0;
    margin-top: -2px
}

tr.shipping span.amount {
    margin-left: .2em
}

.woocommerce-shipping-calculator {
    margin-top: .5em;
    margin-bottom: 0
}

.woocommerce-shipping-destination,
.shipping-calculator-button {
    font-size: .9em
}

.shipping-calculator-form {
    background-color: rgba(0, 0, 0, 0.03);
    padding: 15px 15px 10px;
    border-radius: 5px;
    margin-top: 5px
}

.cart-discount {
    background-color: rgba(122, 156, 89, 0.2);
    font-size: .85em
}

.cart-discount th,
.cart-discount td {
    padding: 10px 5px
}

.cart-sidebar .widget-title {
    border-bottom: 3px solid #ececec;
    font-size: .95em;
    padding-bottom: 10px;
    margin-bottom: 15px
}

.widget_shopping_cart .button {
    width: 100%;
    margin: .5em 0 0
}

.widget_shopping_cart li.empty {
    padding: 0 10px !important;
    margin: 0;
    min-height: 0;
    text-align: center
}

.widget_shopping_cart p.total {
    text-align: center;
    padding: 10px 0;
    border-top: 1px solid #ececec;
    border-bottom: 2px solid #ececec;
    margin-bottom: .5em
}

.dark .widget_shopping_cart p.total {
    border-color: rgba(255, 255, 255, 0.2)
}

.nav-dropdown .product_list_widget {
    overflow-y: auto;
    -webkit-overflow-scrolling: touch;
    max-height: 500px;
    max-height: 50vh
}

.tagcloud {
    padding-bottom: 15px
}

.tagcloud a {
    font-size: 0.8em !important;
    display: inline-block;
    border: 1px solid currentColor;
    opacity: .8;
    margin: 0 3px 5px 0;
    padding: 2px 5px;
    border-radius: 3px
}

.tagcloud a:hover {
    opacity: 1;
    border-color: #446084;
    background-color: #446084;
    color: #fff
}

.variations {
    position: relative
}

.variations td {
    vertical-align: middle;
    padding: .2em 0;
    border: 0
}

.variations .reset_variations {
    position: absolute;
    right: 0;
    bottom: 95%;
    color: currentColor;
    opacity: 0.6;
    font-size: 11px;
    text-transform: uppercase
}
.add-to-cart-button:hover span.amount{
	color: #fff !important;

}
.woocommerce-Price-amount .amount {
color:#446084!important;
}
span.amount {
    white-space: nowrap;
    color: #446084 !important;
    font-weight: bold;
}
span.amount:hover {
color:#fff;
}
.box-premalink {
	position:absolute;
	bottom:0 ;
	left:0 ;
	right:0;
	color : #fff;
	background : #337ab7;
	width:100%;
	text-align:center;
	height: 40px;
	display:none;
}
.box-premalink a {
  color : #fff;
  text-align:center;
 text-transform: uppercase;
  padding:20px;
}
.box-premalink a:hover {
	color:#fff;
}
.box-image:hover .box-premalink {
	display:flex;
	align-items:center;
	justify-content:center;
	display:none;
	
}

             h3.h3{text-align:center;margin:1em;text-transform:capitalize;font-size:1.7em;}


/********************* PRODUCT **********************/
.product-grid4,.product-grid4 .product-image4{position:relative}
.product-grid4{font-family:Poppins,sans-serif;text-align:center;border-radius:5px;overflow:hidden;z-index:1;transition:all .3s ease 0s;box-shadow:0 0 10px rgba(0,0,0,.2);}
.product-grid4:hover{box-shadow:0 0 10px rgba(0,0,0,.2)}
.product-grid4 .product-image4 a{display:block}
.product-grid4 .product-image4 img{width:100%;height:auto}
.product-grid4 .pic-1{opacity:1;transition:all .5s ease-out 0s}
.product-grid4:hover .pic-1{opacity:0}
.product-grid4 .pic-2{position:absolute;top:0;left:0;opacity:0;transition:all .5s ease-out 0s}
.product-grid4:hover .pic-2{opacity:1}
.product-grid4 .social{width:180px;padding:0;margin:0 auto;list-style:none;position:absolute;right:0;left:0;top:50%;transform:translateY(-50%);transition:all .3s ease 0s}
.product-grid4 .social li{display:inline-block;opacity:0;transition:all .7s}
.product-grid4 .social li:nth-child(1){transition-delay:.15s}
.product-grid4 .social li:nth-child(2){transition-delay:.3s}
.product-grid4 .social li:nth-child(3){transition-delay:.45s}
.product-grid4:hover .social li{opacity:1}
.product-grid4 .social li a{color:#fff;background:#333333;font-size:17px;line-height:36px;width:40px;height:36px;border-radius:2px;margin:0 5px;display:block;transition:all .3s ease 0s}
.product-grid4 .social li a:hover{color:#fff;background:#333333}
.product-grid4 .social li a:after,.product-grid4 .social li a:before{content:attr(data-tip);color:#fff;background-color:#000;font-size:12px;line-height:20px;border-radius:3px;padding:0 5px;white-space:nowrap;opacity:0;transform:translateX(-50%);position:absolute;left:50%;top:-30px}
.product-grid4 .social li a:after{content:'';height:15px;width:15px;border-radius:0;transform:translateX(-50%) rotate(45deg);top:-22px;z-index:-1}
.product-grid4 .social li a:hover:after,.product-grid4 .social li a:hover:before{opacity:1}
.product-grid4 .product-discount-label,.product-grid4 .product-new-label{color:#fff;background-color:#333333;font-size:13px;font-weight:800;text-transform:uppercase;line-height:45px;height:45px;width:50%;position:absolute;left:0;top:5px;transition:all .3s}
.product-grid4 .product-discount-label{left:auto;right:10px;background-color:#d7292a}
.product-grid4:hover .product-new-label{opacity:0}
.product-grid4 .product-content{padding:25px}
.product-grid4 .title{font-size:15px;font-weight:400;text-transform:capitalize;margin:0 0 7px;transition:all .3s ease 0s}
.product-grid4 .title a{color:#222}
.product-grid4 .title a:hover{color:#16a085}
.product-grid4 .price{color:#16a085;font-size:17px;font-weight:700;margin:0 2px 15px 0;display:block}
.product-grid4 .price span{color:#909090;font-size:13px;font-weight:400;letter-spacing:0;text-decoration:line-through;text-align:left;vertical-align:middle;display:inline-block}
.product-grid4 .add_to_cart_button{border:1px solid #e5e5e5;display:inline-block;padding:10px 20px;color:#888;font-weight:600;font-size:14px;border-radius:4px;transition:all .3s}
.product-grid4:hover .add_to_cart_button{border:1px solid transparent;background:#333333;color:#fff}
.product-grid4 .add_to_cart_button:hover{background-color:#505050;box-shadow:0 0 10px rgba(0,0,0,.5)}
@media only screen and (max-width:990px){.product-grid4{margin-bottom:30px}
}


#btn_search {
	background: #333333;
}
.loader {
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
	 position: absolute;
    left: 50%;
    top: 35;
	 z-index: 1000;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.filter-wrapper {
    background: #dad9d9;
    margin-bottom: 10px;
    padding: 15px 20px 0px 20px;
    display: inline-block;
}
.button_example {
width: 100%;
    text-transform: uppercase;
    line-height: 28px;
    height: 30px;
    border-radius: 3px;
    border: 1px solid #bbbaba;
    font-size: 9px;
    font-family: arial, helvetica, sans-serif;
    padding: 0px 22px 0px 13px;
    text-decoration: none;
    display: inline-block;
    color: #000;
    background: rgba(253,253,253,1);
    background: -moz-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
    background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(253,253,253,1)), color-stop(12%, rgba(250,250,250,1)), color-stop(28%, rgba(247,247,247,1)), color-stop(49%, rgba(234,232,232,1)), color-stop(76%, rgba(219,217,217,1)), color-stop(100%, rgba(213,210,210,1)));
    background: -webkit-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
    background: -o-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
    background: -ms-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
    background: linear-gradient(to bottom, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
}

</style>
<section class="header-image-new">
	
  <div class="wOuter">
    	<div class="wInner">
		<h1><?=the_title()?></h1>
			         </div>
  </div>
</section>
<section class="content">
  <div class="container">
	 <div class="loader"></div> 
	<?php 
		if( is_user_logged_in() ):
	?>	  
		<div class="bg-side"></div>
		
		<div class="row">
		 
		  <div class="col-md-3 sidebar_wrap welcome-left">       
			<?php 
				get_sidebar('menu'); 
			?>
		  </div>
		  <div class="col-md-9 content_wrap welcome-right">
	
					<?php
					    $taxonomyName = "product_cat"; 
					    $parent_terms = get_terms( $taxonomyName, array( 'parent' => 0, 'orderby' => 'slug', 'hide_empty' => false ) );   

	//echo var_dump($parent_terms);
    $machine_type = get_terms( $taxonomyName, array( 'parent' => 22, 'orderby' => 'slug', 'hide_empty' => false ) );
    $machine_model = get_terms( $taxonomyName, array( 'parent' => 26, 'orderby' => 'slug', 'hide_empty' => false ) );
   $part_type = get_terms( $taxonomyName, array( 'parent' => 39, 'orderby' => 'slug', 'hide_empty' => false ) );

	$mc_type = $_GET['machine_type'];
	$mc_model = $_GET['machine_model'];
	$pc_type = $_GET['part_type'];
	$mc_number = $_GET['model_number'];
	


    ?>			
	<div class="row">
			<div class="col-md-6">
				<div class="add-to-cart-button pull-left"><a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>"><?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?> - <?php echo WC()->cart->get_cart_total(); ?> </a></div>

			</div>
			<div class="col-md-3">
				<div class="add-to-cart-button pull-right"><a href="/check-out">CHECKOUT</a></div>
			</div>
			<div class="col-md-3">
				<div class="add-to-cart-button pull-right"><a href="/my-orders">RECENT ORDERS</a></div>
			</div>
			
		</div>
			<hr>
				<div class="row">
				<div class="filter-wrapper">
					<p class="table-top-h1"> Manage Filters:</p>
				<form method="get" style="padding-bottom:10px;">
				<div class="col-md-3">
						<div class="form-group">
								<select class="form-control button_example" name="machine_type" id="machine_type">
									<option><label> Machine Type : All</label></option>
									<?php if($machine_type !=' ') { ?>
										<option vale="<?=$machine_type?>"><?=ucfirst($machine_type)?></option>
										<?php } else { ?>
											option></option>

										<?php } ?>
									<?php foreach(   $machine_type as $machine_type) {?>
										<option value="<?=$machine_type->term_id?>"><?=$machine_type->name?></option>
									<?php } ?>

								</select>
							</div>
						 </div>
					<div class="col-md-3">
							<div class="form-group">
								<select class="form-control  button_example"  name="machine_model" id="machine_model">
									<option><label> Machine Model : All</label></option>

								</select>
							</div>
						 </div>	

	
					<div class="col-md-3">
							<div class="form-group">
							 	<select class="form-control  button_example" name="model_number" id="model_number" >
								<option><label> Machine Number : All</label></option>

							</select>
							</div>
						 </div>	
					<div class="col-md-3">
							<input type="hidden" name="machine-type" id="mc-type">
							<input type="hidden" name="machine-model" id="mc-model">
							<input type="hidden" name="model-number" id="mc-num">
							<input type="hidden" name="part-type" id="part-type">


							<div class="form-group">
									<select class="form-control  button_example" name="part_type" id="part_type">
											<option><label> Part Type : All</label></option>
										<?php if($pc_type !=' ') { ?>
										<option vale="<?=$pc_type?>"><?=ucfirst($pc_type)?></option>
										<?php } else { ?>
											<option value=" "> All</option>

										<?php } ?>
									<?php foreach(   $part_type as $part_type) {?>
										<option value="<?=$part_type->slug?>"><?=$part_type->name?></option>
									<?php } ?>

								</select>
							</div>
							<button class="btn btn-info pull-right" id="btn_search" style="margin-bottom:10px;">Search <i class="fa fa-search"></i></button>
						 </div>	

						</form>
					</div>
					</div>
					
					<hr>
				
				<?php do_action( 'woocommerce_before_cart' ); ?>
				<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$params = array('posts_per_page' => 21,'post_type' => 'product','paged' => $paged ,'orderby' => 'title',
 'order' => 'ASC', ); // (1)
			if(isset($_GET['model_number']) && isset($_GET['model_number']) !='') {
					$mc_number = $_GET['model_number'];
					if(!empty($mc_number) && $mc_number !="All"){

						$params['tax_query']['relation'] = 'IN';
							$params['tax_query'][] = 	array(
										'taxonomy' => 'product_cat',
										'terms'    => $mc_number,
										'field'      => 'term_id',
							);
					}
										
				}
			
			if(isset($_GET['machine_type']) && isset($_GET['machine_type']) !='') {
				$mc_type = $_GET['machine_type'];
				if(!empty($mc_type) && $mc_type  !="All"){
					$params['tax_query']['relation'] = 'IN';
				$params['tax_query'][] = 	array(
										'taxonomy' => 'product_cat',
										'terms'    => $mc_type,
										'field'      => 'term_id',
					);
				}
				
			}
			if(isset($_GET['machine_model']) && isset($_GET['machine_model']) !='') {
				
				$mc_model = $_GET['machine_model'];
					if(!empty($mc_model) && $mc_model !="All") {
						$params['tax_query']['relation'] = 'IN';
				$params['tax_query'][] = array(
								'taxonomy'   => 'product_cat',
								'terms'      => $mc_model,
								'field'      => 'term_id',
					);
					}
			}
			if(isset($_GET['part_type']) && isset($_GET['part_type']) !='') {
				$p_type = $_GET['part_type'];
					if(!empty($p_type) && $p_type !='All'){
						$params['tax_query']['relation'] = 'OR';
						$params['tax_query'][] = array(
								'taxonomy'   => 'product_cat',
								'terms'      => $p_type,
								'field'      => 'slug',
						);
					}		
			}
			$wc_query = new WP_Query($params); // (2)
			?>
			<div id="product_container">
				<div class="row">
								<?php if ($wc_query->have_posts()) : // (3) ?>
								<?php while ($wc_query->have_posts()) : // (4)
								                $wc_query->the_post(); // (4.1) 
								                	global $product; 
								                	$name = get_the_title();

										$new_price = $wpdb->get_row("SELECT * FROM wp_pricing_table WHERE machines = '$name'") ;

								               	$price = 0 ;
			  					 											
			  					 		if($price_level == 'b4' || $price_level=='B4 Price') {
			  					 			$price = $new_price->b4_price;
			  					 		}	
			  					 		else if($price_level=='otis' || $price_level =='Otis' || $price_level == 'Otis Price') {
			  					 			$price = $new_price->otice_price;
			  					 		}	
			  					 		else {
			  					 		  $price = $new_price->general_price;
			  					 		}

			  						?>
							
							
				
    <div class="col-md-4 col-sm-6">
            <div class="product-grid4" style="margin-bottom:30px;">
                <div class="product-image4">
                    <a href="#">
			<?php 
				if ( has_post_thumbnail( $wc_query->post->ID ) )  { ?>

				<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $wc_query->post->ID ), 'single-post-thumbnail' );?>
                       		 <img class="pic-1" src="<?php  echo $image[0]; ?>" style="width:100%;height:197px;">
                      		  <img class="pic-2" src="<?php  echo $image[0]; ?>" style="width:100%;height:197px;">
			<?php } else { ?>
				 <img class="pic-1" src="http://staging.torindriveintl.com/wp-content/uploads/2019/11/no_image.jpg">
                      		  <img class="pic-2" src="http://staging.torindriveintl.com/wp-content/uploads/2019/11/no_image.jpg">
															 
			   <?php } ?>
	

			
                    </a>
			 <ul class="social">
                        <li><a href="<?=get_permalink( $product->get_id() );?>" data-tip="View Details"><i class="fa fa-eye"></i></a></li>
                   
                    </ul>           <span class="product-new-label"><?php $product_cats_ids = wc_get_product_term_ids( $product->get_id(), 'product_cat' ); ?>
  <?php 

$ternname = "";
foreach( $product_cats_ids as $cat_id ) {
    $term = get_term_by( 'id', $cat_id, 'product_cat' );

  
$ternname = $term->name; 
}
echo $ternname;?></span>
                    <!-- <span class="product-discount-label">-10%</span> -->
                </div>
                <div class="product-content">
                    <h3 class="title"><a href="#"><?php the_title(); // (4.2) ?></a></h3>
                                         <hr>
                    <div class="price">
                        $<?=number_format($price,2)?>                                            
		</div>
                           <?php
		if($price > 0 ) {

									               						woocommerce_template_loop_add_to_cart( $wc_query->post, $product );

									               					}
									               					else {
									               								echo 'Not Available';
									               					} 
?>

		</div>
            </div>
        </div>
			

					<?php endwhile; ?>
				</div>
										<?php wp_reset_postdata(); // (5) ?>
									
								<?php else:  ?>
								<p>
								     <?php _e( 'No Products Found' ); // (6) ?>
								</p>
				<?php endif; ?>
			</div>
							<div class="row">
									
		  						<div class="col-md-12 float-right">
		  								<?php
										echo paginate_links( array(
													'base'        => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, true ) ) ) ),
													'format' 			=> '?paged=%#%',
													'add_args'    => false,
													'current' 		=> max( 1, get_query_var('paged') ),
													'total' 			=> $wc_query->max_num_pages,
													'prev_text'   => '&larr;',
													'next_text'   => '&rarr;',
													'type'        => 'list',
													'end_size'    => 3,
													'mid_size'    => 3
								) );
									?>
		  						</div>
		  				</div>	
					
			</div>
	
		 	
		</div>
	
	<?php else : ?>
		
		<?php get_template_part('restricted-error'); ?>
		
	<?php endif; ?>
  </div>
</section>
<?php get_footer(); ?>
<script>
	 $(document).ready(function() {
		   $('.loader').hide();  
		   $('#model_loader').hide(); 
		   $('#machine_loader').hide();     
	  $('#machine_type').on('change', function() {
				$('#machine_loader').show(); 

			   $('#mc-type').val($('#machine_type option:selected').text());
				$.ajax({
   					 type: "post",
    			 		dataType: "json",
    					 url : "<?php echo admin_url('admin-ajax.php'); ?>",
             		 		data : {action: "get_machine_model" , term_id :  $('#machine_type').val()},
    					success: function(response){
        				 console.log(response);
					 $('#machine_model option').remove();
					$('#machine_model').append($('<option>').text('Machine Model : All').attr('value','All'));

					$.each(response, function(index, value) {
   					 $('#machine_model').append($('<option>').text(value['name']).attr('value', value['term_id']));
				});
					$('#machine_loader').hide(); 
  
    			      }
			});
		});
	$('#machine_model').on('change', function() {
		$('#mc-model').val($('#machine_model option:selected').text());
			$('#model_loader').show(); 

              		$.ajax({
   				type: "post",
    			 	dataType: "json",
    				url : "<?php echo admin_url('admin-ajax.php'); ?>",
             		 	data : {action: "get_model_numbers" , term_id :  $('#machine_model').val()},
    					success: function(response){
        					console.log(response);
					 $('#model_number option').remove();
					$('#model_number').append($('<option>').text('Machine Number : All').attr('value','All'));

					$.each(response, function(index, value) {
   					 $('#model_number').append($('<option>').text(value['name']).attr('value', value['term_id']));
				});
				$('#model_loader').hide(); 

    			      }
			});
            });
	   $('#model_number').on('change', function() {
			$('#mc-num').val($('#model_number option:selected').text());

              		//$('form').submit();
            });
  		$('#part_type').on('change', function() {
			$('#part-type').val($('#part_type option:selected').text());

              		//$('form').submit();
            });
     $('form').submit(function(){
			$('#btn_search').attr('disable',true);
			$('#btn_search').html('<span><i class="fa fa-spin fa-spinner"></i></span>');

	});


        });
</script>