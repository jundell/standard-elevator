<?php 
	/* Template Name: Customer Dashboard */
	get_header();
?>

<section class="header-image-new">

  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">
    <div class="bg-side"></div>
    <div class="row">
      <div class="col-md-9 col-xs-12">
        <?php
          // Start the loop.
          while ( have_posts() ) : the_post();
          // the_content();
          // End the loop.
          endwhile;
        ?>
      </div>
      <div class="clear"></div>
     
      <div class="col-md-3 sidebar_wrap welcome-left">
		    <?php get_sidebar('menu'); ?>
      </div>



      <?php       
        if ( is_user_logged_in() ){
      ?>
      <div class="col-md-9 col-xs-12 content_wrap welcome-right">       
          
          <?php the_content(); $header = ''; $last_date = '' ?>
            <?php
              global $post;
              $showall = @$_GET['show'] == 'all';
              $per_page = ( $showall ) ? -1 : 5;
              $args = array( 'posts_per_page' => $per_page, 'post_status' => 'publish' );
              $myposts = get_posts( $args );
              foreach ( $myposts as $post ) : 
                setup_postdata( $post );
                $this_date = get_the_date('Y');
            ?>
              <?php if ( $showall && !$header ) : $header = true; $last_date = $this_date; ?>
                <h4><?php echo $this_date; ?></h4>
                <ul class="alink">
              <?php elseif ( $showall && $this_date != $last_date ) : $last_date = $this_date; ?>
                </ul>
                <h4><?php echo $last_date; ?></h4>
                <ul class="alink">
              <?php elseif ( !$showall ) : ?>
                <ul class="alink">
              <?php endif; ?>

              <li><a href="<?php the_field('pdf_file'); ?>" target="_blank"><?php the_title(); ?></a></li>
            <?php endforeach;
            wp_reset_postdata(); ?>
          </ul>
          <?php if ( !$showall ) : ?>
            <a class="btn btn-link view-all-rako" href="?show=all">View All Bulletins &raquo;</a>
          <?php endif; ?>
          <?php
            $requests = custom_repeater_data('requests',array('button_text','button_page'));
          ?>
          <hr>
          <div class="box-wrapper">
            <div class="box-wrap">
              <div class="action-box start">
                <div class="box-content">
                  <!-- <i class="fa fa-calculator"></i>
                 -->
                 <div class="box-content-image">
                   <img src="<?php echo get_template_directory_uri();?>/images/initial-quote.png" alt="Initial Quote">
                 </div>
                  <h3><?php echo $requests[1]['button_text'] ?></h3>
                  <p>For bidding purpose only</p>
                  <a class="btn-box" href="/torin/project/form">Start</a>
                </div>
              </div>
              <div class="action-box box-sep">
                <div class="box-content">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-separator.png" alt="">
                </div>
              </div>
              <div class="action-box box-yellow">
                <div class="box-content">
                  <!-- <i class="fa fa-group"></i> -->
                  <div class="box-content-image">
                   <img src="<?php echo get_template_directory_uri();?>/images/final-proposal.png" alt="Final Proposal">
                 </div>
                  <h3><?php echo $requests[2]['button_text'] ?></h3>
                  <p>Once you have the job</p>
                  <a class="btn-box" href="/torin/project/form/final">Obtain</a>
                </div>
              </div>
              <div class="action-box box-sep">
                <div class="box-content">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-separator.png" alt="">
                </div>
              </div>
              <div class="action-box box-green">
                <div class="box-content">
                  <!-- <i class="fa fa-group"></i> -->
                  <div class="box-content-image">
                   <img src="<?php echo get_template_directory_uri();?>/images/purchase.png" alt="Purchase">
                 </div>
                  <h3>Purchase</h3>
                  <p>Order your job</p>
                  <a class="btn-box" href="/torin/projects">Purchase</a>
                </div>
              </div>
            </div>
          </div>
          <br>
          <br>
          <div class="box-wrapper">
            <div class="box-wrap">
              <div class="action-box">
                <div class="box-content">
                  <!-- <i class="fa fa-gears"></i> -->
                  <div class="box-content-image">
                   <img src="<?php echo get_template_directory_uri();?>/images/components.png" alt="Elevator Components">
                 </div>
                  <h3>Elevator Components</h3>
                  <p>For component only</p>
                  <a class="btn-box" href="/torin/project/form/component">Purchase</a>
                </div>
              </div>
              <div class="action-box box-sep no-content">
                <div class="box-content">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-separator.png" alt="">
                </div>
              </div>
              <div class="action-box">
                <div class="box-content">
                  <!-- <i class="fa fa-gears"></i> -->
                  <div class="box-content-image">
                   <img src="<?php echo get_template_directory_uri();?>/images/spare-parts.png" alt="Spare Parts">
                 </div>
                  <h3>Spare Parts</h3>
                  <p>For spare parts only</p>
                  <a class="btn-box" href="/spare-parts">Purchase</a>
                </div>
              </div>
              <div class="action-box box-sep no-content">
                <div class="box-content">
                  <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-separator.png" alt="">
                </div>
              </div>
              <div class="action-box no-content">
                <div class="box-content">
                  <!-- <i class="fa fa-gears"></i> -->
                  <div class="box-content-image">
                   <img src="<?php echo get_template_directory_uri();?>/images/spare-parts.png" alt="Spare Parts">
                 </div>
                  <h3>Spare Parts</h3>
                  <p>For spare parts only</p>
                  <a class="btn-box" href="/torin/project/form/component">Start</a>
                </div>
              </div>
            </div>
          </div>
      </div>
      <?php } else { get_template_part('restricted-error'); } ?>
    </div>
  </div>
</section>
<?php get_footer(); ?>