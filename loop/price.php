<?php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
global $wpdb;
$user_id = get_current_user_id(); 
$key = 'price_level'; 
$single = true; 
$price_level = get_user_meta( $user_id, $key, $single ); 
$name= $product->name;

$new_price = $wpdb->get_row("SELECT * FROM wp_pricing_table WHERE machines = '$product->name'") ;

								               	$price = 0 ;
			  					 											
			  					 		/*if($price_level == 'b4' || $price_level=='B4 Price') {
			  					 			$price = $new_price->b4_price;
			  					 		}	
			  					 		else */if($price_level=='otis' || $price_level =='Otis' || $price_level == 'Otis Price') {
			  					 			$price = $new_price->otis_price;
			  					 		}	
			  					 		else {
			  					 		  $price = $new_price->general_price;
			  					 		}

?>


<?php if ( $price_html = $price ) : ?>
	<span class="price">$ <?php echo number_format($price_html,2); ?></span>
<?php endif; ?>
