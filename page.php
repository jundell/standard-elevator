<?php
  global $post;
  $post_slug = $post->post_name;
  if ( @$_GET['display'] == 'content' ) {
    if ( $post_slug == 'shipping-policy' ) {
      echo "<div style='text-align-center;'>Shipping Policy<small>EFFECTIVE ".get_the_date('F j, Y', $post->ID)."</small></div>";
    }
    echo "<div style='font-family:Arial;font-size:14px !important;'>";
    echo apply_filters('the_content', $post->post_content);
    echo "</div>";
    die();
  }
?>
<?php get_header(); ?>

<section class="header-image-new">

  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
      <?php if ( $post_slug == 'shipping-policy' ) : ?>
        <div style="color:#FFF;">EFFECTIVE <?php echo get_the_date('F j, Y', $post->ID); ?></div>
      <?php endif; ?>
    </div>
  </div>
</section>

<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php
          // Start the loop.
          while ( have_posts() ) : the_post();
          the_content();
          // End the loop.
          endwhile;
        ?>
      </div>
    </div>
  </div>
  <?php if ( in_array($post_slug,[ 'warranty', 'shipping-policy']) ) : ?>
    <div class="container">&nbsp;</div>
    <div class="container">&nbsp;</div>
  <?php endif; ?>
</section>
<?php get_footer(); ?>