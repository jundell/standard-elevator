<?php 
/* Template Name: Product Detail Page */
	get_header();

global $product;
?>
<section class="header-image-new">
  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">
	<?php 
		if( is_user_logged_in() ):
	?>	  
		<div class="bg-side"></div>
		<div class="row">
		 
		  <div class="col-md-3 sidebar_wrap welcome-left">       
			<?php 
				get_sidebar('menu'); 
			?>
		  </div>
		  <div class="col-md-9 content_wrap welcome-right">
						
		  </div>
		 
		</div>
	
	<?php else : ?>
		
		<?php get_template_part('restricted-error'); ?>
		
	<?php endif; ?>
  </div>
</section>
<?php get_footer(); ?>