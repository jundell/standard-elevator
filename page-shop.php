<?php
/* Template Name: Shop Page */
	get_header();
?>
<?php
global $wpdb;
$user_id = get_current_user_id();
$key = 'price_level';
$single = true;
$price_level = get_user_meta( $user_id, $key, $single );



?>
<style>
.page-numbers {
border: none !important;
}
 .page-numbers a:link,  .page-numbers a:visited {
display: block !important;
background: #77a464 !important;
color: #363636 !important;
padding: 10px 14px !important;
}
.page-numbers a:link,  .page-numbers a:hover {
display: block !important;
background: #fff !important;
color: #363636 !important;
}
 .page-numbers .current,  .page-numbers li a:hover {
padding: 10px 14px !important;
background: #77A464 !important;
color: #fff !important;
}
.page-numbers > ul {
 display:block;
  list-style:none;
}
.page-numbers ul > li {
  display:inline-block;
}
.content_wrap ul li {
 display:inline-block;
 margin : 0px;
}
.content_wrap ul {
 display : block;
 list-style:none;

}
.product-box {
	position: relative;
	width: 100%;
	height: 30%;
	display: flex;
	align-items: center;
	border: 1px solid #446084;
	margin-top: 25x;
	margin-bottom: 25px;
    font-size: 1.1em;
 border-radius : 5px;
box-shadow: 5px 10px #fff;
}
.add-to-cart-button a {
	padding:10px;
	/* border:#446084 1.5px solid; */
	border:#000 1.5px solid;
	color: #000 ;
	text-transform : uppercase;
	font-style:bold;
	font-weight:bold;
}
.add-to-cart-button a:hover{
    background-color:#446084;
    border-color: #446084;
	color : #ffffff;
}
.wc-proceed-to-checkout a {
	padding:10px;
	border:#446084 1.5px solid;
	color : #446084 ;
	text-transform : uppercase;
	font-style:bold;
	font-weight:bold;
}
.filters {
display:block;
list-style:none;
}
.filters .filter-item {

display:inline-block;
}
.title-wrapper .category > p {

	padding: 0px;
   	margin: 0px;
    	text-transform: uppercase !important;;
}
.box-image{
direction: ltr;
    display: table-cell;
    vertical-align: middle;
 position:relative;
}
.box-image img {
    max-width: 100%;
    width: 100%;
    transform: translateZ(0);
    margin: 0 auto;
    min-width: 247px!important;
    width: 247px!important;
}

.product-details {
 margin-left:10px;
	padding-top: .7em;
    padding-bottom: 1.4em;
    position: relative;
    width: 100%;
    font-size: .9em;

}
img {
    max-width: 100%;
    height: auto;
    display: inline-block;
    vertical-align: middle;
}
.product-title {
color: #446084 !important;
}
.is-small, .is-small.button {
    font-size: .8em;
}
.widget_shopping_cart_content .blockUI.blockOverlay,
.woocommerce-checkout-review-order .blockUI.blockOverlay {
    background-color: white !important;
    opacity: 0.6 !important
}

.widget_shopping_cart_content .blockUI.blockOverlay::before,
.woocommerce-checkout-review-order .blockUI.blockOverlay::before {
    display: block;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-left: -0.5em;
    margin-top: -0.5em;
    content: '';
    line-height: 1;
    text-align: center;
    font-size: 2em;
    border-top: 3px solid rgba(0, 0, 0, 0.1) !important;
    border-right: 3px solid rgba(0, 0, 0, 0.1) !important;
    border-bottom: 3px solid rgba(0, 0, 0, 0.1) !important;
    pointer-events: none;
    border-left: 3px solid #446084;
    animation: spin .6s infinite linear;
    border-radius: 50%;
    width: 30px;
    height: 30px
}

.category-page-row {
    padding-top: 30px
}

.price_slider_amount input {
    display: none
}

.woocommerce-result-count {
    display: inline-block;
    margin: 0 1em 0 auto
}

.woocommerce-ordering,
.woocommerce-ordering select {
    margin: 5px 0;
    display: inline-block
}

.add_to_cart_button.added {
    display: none
}

a.added_to_cart {
    display: inline-block;
    font-size: .9em;
    padding: 10px 0;
    text-transform: uppercase;
    font-weight: bold
}

a.added_to_cart:after {
    content: " →"
}

.grid-style-3 .title-wrapper {
    -ms-flex: 1;
    flex: 1;
    padding-right: 15px;
    min-width: 60%;
    overflow: hidden;
    text-overflow: ellipsis
}

.grid-style-3 .price-wrapper {
    text-align: right
}

.grid-style-3 .star-rating {
    margin: 0.2em 0;
    text-align: right
}

.grid-style-3 .price del {
    display: block
}

.grid-style-3 .price del span.amount {
    margin: 0
}

.products .box-vertical .box-text {
    font-size: 1.1em
}

.page-numbers.button.current {
    pointer-events: none;
    opacity: .6
}

.grid-tools {
    display: -ms-flexbox;
    display: flex;
    width: 100%;
    padding: 0 !important;
    margin-bottom: -1px;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    white-space: nowrap
}

.grid-tools a {
    text-overflow: ellipsis;
    opacity: .95;
    display: block;
    width: 100%;
    padding: .4em 0 .5em;
    font-size: .85em;
    font-weight: bold;
    text-transform: uppercase;
    background-color: #446084;
    color: #f1f1f1;
    transition: opacity .3s
}

.grid-tools a+a {
    border-left: 1px solid rgba(255, 255, 255, 0.1)
}

.grid-tools a:hover {
    color: #FFF;
    opacity: 1
}

.grid-tools .add-to-cart-grid {
    width: 0
}

@media (max-width: 849px) {
    .category-filter-row {
        padding: 10px 0
    }
}

.filter-button {
    display: inline-block;
    margin-top: .5em
}
.dashboard_content .table-wrapper a.btn {
    cursor: pointer;
    pointer-events: auto !important;

}
.box-image .out-of-stock-label {
    color: #333;
    font-weight: bold;
    text-transform: uppercase;
    position: absolute;
    top: 40%;
    left: 0;
    right: 0;
    background: #fff;
    padding: 20px 0;
    background: rgba(255, 255, 255, 0.9);
    text-align: center;
    opacity: .9
}

.featured-title .woocommerce-result-count {
    display: none
}

.widget_product_categories>ul>li {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-flow: row wrap;
    flex-flow: row wrap;
    -ms-flex-align: center;
    align-items: center
}

.widget_product_categories>ul>li span {
    font-size: .85em;
    opacity: .4
}

.widget_product_categories>ul>li ul span.count {
    display: none
}

.message-wrapper+.login {
    padding: 30px;
    background-color: rgba(0, 0, 0, 0.03)
}

.woocommerce-form-login .button {
    margin-bottom: 0
}

.woocommerce-billing-fields {
    padding-top: 15px;
    border-top: 2px solid #ddd
}

.woocommerce-billing-fields p {
    margin-bottom: .5em
}

form.checkout h3 {
    font-size: 1.1em;
    overflow: hidden;
    padding-top: 10px;
    font-weight: bolder;
    text-transform: uppercase
}

form.checkout input[type="checkbox"] {
    margin-top: 0;
    margin-bottom: 0;
    margin-right: .5em
}

.payment_methods li+li {
    padding-top: 7px;
    border-top: 1px solid #ececec
}

.payment_methods p {
    font-size: .9em
}

.payment_method_paypal img {
    max-width: 130px;
    margin: 0 10px
}

a.about_paypal {
    font-size: .8em
}

.woocommerce-privacy-policy-text {
    font-size: 85%
}

p.form-row-wide {
    clear: both
}

p.form-row-push {
    margin-top: -15px
}

@media (min-width: 550px) {
    p.form-row-first,
    p.form-row-last {
        width: 48%;
        float: left
    }
    p.form-row-first {
        margin-right: 4%
    }
}

input#place_order {
    font-size: 1.2em;
    white-space: normal;
    line-height: 1.2;
    padding-top: .5em;
    padding-bottom: .5em
}

#ship-to-different-address {
    padding-top: 0
}

#ship-to-different-address label {
    text-transform: none;
    font-weight: normal
}

#billing_address_2_field>label {
    width: 0;
    opacity: 0;
    overflow: hidden;
    white-space: nowrap
}

@media (max-width: 549px) {
    #billing_address_2_field>label {
        display: none
    }
}

.wc-terms-and-conditions {
    margin-top: -15px;
    border-top: 1px solid #ececec;
    padding: 15px 0
}

.wc-terms-and-conditions input {
    margin-bottom: 0
}

.wc-terms-and-conditions label {
    font-weight: normal
}

div.create-account {
    clear: both
}

.form-row.create-account {
    font-size: 1.1em;
    margin: 0
}

.form-row.create-account label {
    font-weight: normal
}

.page-checkout-simple {
    padding: 3% 0
}

.js_active .woocommerce-account-fields p.create-account+div.create-account,
.js_active .woocommerce-shipping-fields #ship-to-different-address+div.shipping_address {
    display: none
}

.widget_price_filter form {
    margin: 0
}

.widget_price_filter .price_slider {
    margin-bottom: 1em;
    background: #f1f1f1
}

.widget_price_filter .price_label {
    padding-top: 6px
}

.widget_price_filter span {
    font-weight: bold
}

.widget_price_filter .price_slider_amount {
    text-align: right;
    line-height: 1;
    font-size: .8751em
}

.widget_price_filter .price_slider_amount .button {
    border-radius: 99px;
    background-color: #666;
    float: left;
    font-size: .85em
}

.widget_price_filter .ui-slider {
    position: relative;
    text-align: left
}
a.added_to_cart {
    display: inline-block;
    font-size: .9em;
    padding: 10px;
    text-transform: uppercase;
    font-weight: bold;
}
.widget_price_filter .ui-slider .ui-slider-handle {
    position: absolute;
    z-index: 2;
    width: 15px;
    height: 15px;
    cursor: pointer;
    outline: none;
    top: -5px;
    border-radius: 999px;
    background-color: #666
}

.widget_price_filter .ui-slider .ui-slider-handle:last-child {
    margin-left: -10px
}

.widget_price_filter .ui-slider .ui-slider-range {
    position: absolute;
    opacity: .5;
    border-radius: 99px;
    z-index: 1;
    font-size: 10px;
    display: block;
    border: 0;
    background-color: #666
}

.widget_price_filter .ui-slider-horizontal {
    height: 5px;
    border-radius: 99px
}

.widget_price_filter .ui-slider-horizontal .ui-slider-range {
    top: 0;
    height: 100%
}

.widget_price_filter .ui-slider-horizontal .ui-slider-range-min {
    left: -1px
}

.widget_price_filter .ui-slider-horizontal .ui-slider-range-max {
    right: -1px
}

.widget li.chosen a:before {
    content: 'x';
    display: inline-block;
    opacity: .6;
    color: currentColor;
    margin-right: 5px
}

.wc-layered-nav-term.chosen>a:before {
    background-color: #f1f1f1;
    border: 1px solid rgba(0, 0, 0, 0.1);
    line-height: 12px;
    width: 18px;
    height: 18px;
    text-align: center;
    border-radius: 99px
}

.widget_layered_nav_filters ul li.chosen {
    display: inline-block;
    margin-right: 10px;
    border: 0 !important
}

.widget_layered_nav_filters ul li.chosen a {
    display: inline-block;
    background-color: #f1f1f1;
    border: 1px solid rgba(0, 0, 0, 0.1);
    border-radius: 99px;
    opacity: .9;
    padding: 0 7px;
    font-size: .85em;
    font-weight: normal
}

.dark .widget_layered_nav_filters ul li.chosen a {
    color: #FFF;
    background-color: rgba(0, 0, 0, 0.5)
}

.widget_layered_nav_filters ul li.chosen a:before {
    content: 'x';
    opacity: .4;
    margin-right: 3px
}

.widget_layered_nav_filters ul li.chosen a:hover:before {
    opacity: 1
}

.woocommerce-product-gallery figure {
    margin: 0
}

.message-wrapper {
    margin: 0;
    padding-bottom: .5em
}

#wrapper>.message-wrapper {
    padding-top: .75em;
    padding-bottom: .75em;
    margin-bottom: 10px;
    font-size: 1.1em
}

ul.message-wrapper li {
    list-style: none
}

.message-container span {
    font-weight: bold
}

.message-container .wc-forward {
    display: none
}

.message-container a {
    margin: 0 15px 0 0
}

.container .message-container {
    padding-left: 0;
    padding-right: 0
}

.message-wrapper+main .product-main {
    padding-top: 0
}

.demo_store {
    padding: 5px;
    margin: 0;
    text-align: center;
    background-color: #000;
    color: #FFF
}

.has-transparent+main>.message-wrapper {
    position: fixed;
    z-index: 999;
    width: 100%;
    bottom: 0;
    background-color: #FFF;
    box-shadow: 1px 1px 10px 1px rgba(0, 0, 0, 0.1)
}

.form-row input[type="submit"] {
    margin: 0
}

.form-row input[type="submit"]+label {
    margin-left: 15px
}

.my-account-header.featured-title .page-title-inner {
    min-height: 100px
}

.my-account-header .button {
    margin-top: 5px;
    margin-bottom: 5px
}

.woocommerce-form-register .woocommerce-privacy-policy-text {
    margin-bottom: 1.5em
}

form.lost_reset_password {
    padding: 30px 0
}

.dashboard-links {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    line-height: 1;
    font-size: 1.3em;
    list-style: none
}

.dashboard-links a {
    display: block;
    border-radius: 5px;
    padding: 20px 0;
    border: 1px solid #ddd;
    text-align: center;
    margin-right: 10px;
    transition: all .3s
}

.dashboard-links .active {
    display: none
}

.dashboard-links a:hover {
    background-color: #333;
    color: #FFF
}

.dashboard-links li {
    width: 33.333%
}

@media (max-width: 849px) {
    .dashboard-links li {
        width: 50%
    }
}

@media (max-width: 549px) {
    .dashboard-links li {
        width: 100%
    }
}

.price {
    line-height: 1
}

.product-info .price {
    font-size: 1.5em;
    margin: .5em 0;
    font-weight: bolder
}

.woocommerce-variation-price {
    border-top: 1px dashed #ddd;
    font-size: .8em;
    padding: 7.5px 0
}

.price-wrapper .price {
    display: block
}

span.amount {
    white-space: nowrap;
    color: #111;
    font-weight: bold
}

.dark .price,
.dark span.amount {
    color: #FFF
}

.header-cart-title span.amount {
    color: currentColor
}

del span.amount {
    opacity: .6;
    font-weight: normal;
    margin-right: .3em
}

.no-prices .amount {
    display: none !important
}

ul.product_list_widget li {
    list-style: none;
    padding: 10px 0 5px 75px;
    min-height: 80px;
    position: relative;
    overflow: hidden;
    vertical-align: top;
    line-height: 1.33
}

ul.product_list_widget li+li {
    border-top: 1px solid #ececec
}

.dark ul.product_list_widget li {
    border-color: rgba(255, 255, 255, 0.2)
}

.widget_shopping_cart ul.product_list_widget li {
    padding-right: 30px
}

ul.product_list_widget li>span.reviewer {
    font-size: .8em
}

ul.product_list_widget li a:not(.remove) {
    display: block;
    margin-bottom: 5px;
    padding: 0;
    overflow: hidden;
    text-overflow: ellipsis;
    line-height: 1.3
}

ul.product_list_widget li a.remove {
    position: absolute;
    right: 0px;
    z-index: 9
}

ul.product_list_widget li dl {
    margin: 0;
    line-height: 1;
    font-size: .7em
}

ul.product_list_widget li img {
    top: 10px;
    position: absolute;
    left: 0;
    width: 60px;
    height: 60px;
    margin-bottom: 5px;
    object-fit: cover;
    object-position: 50% 50%
}

ul.product_list_widget li .quantity {
    display: block;
    margin-top: 3px;
    font-size: .85em;
    opacity: 0.6
}

.product-main {
    padding: 40px 0
}

.page-title:not(.featured-title)+main .product-main {
    padding-top: 15px
}

.product-info {
    padding-top: 10px
}

.product-summary .woocommerce-Price-currencySymbol {
    font-size: .75em;
    vertical-align: top;
    display: inline-block;
    margin-top: .05em
}

.product-summary .quantity {
    margin-bottom: 1em
}

.product-summary .variations_button {
    padding: .5em 0
}

.product-summary table tr+tr {
    border-top: 1px dashed #ddd
}

.product_meta {
    font-size: .8em;
    margin-bottom: 1em
}

.product_meta>span {
    display: block;
    border-top: 1px dotted #ddd;
    padding: 5px 0
}

.product-info p.stock {
    margin-bottom: 1em;
    line-height: 1.3;
    font-size: .8em;
    font-weight: bold
}

p.in-stock {
    color: #7a9c59
}

.group_table .quantity {
    margin: 0
}

.group_table .price {
    font-size: 1em
}

.group_table .label label {
    padding: 0;
    margin: 0
}

.product-gallery,
.product-thumbnails .col {
    padding-bottom: 0 !important
}


p.table-top-h1 {
    text-transform: uppercase;
    font-weight: bold;
    font-size: 15px !important;
    margin-bottom: 3px !important;
    margin-left: 9px;
}
@media screen and (min-width: 850px) {
    .product-gallery-stacked {
        white-space: normal !important;
        overflow: auto !important;
        width: auto !important
    }
    .product-gallery-stacked .flickity-slider,
    .product-gallery-stacked .flickity-viewport {
        height: auto !important
    }
    .product-gallery-stacked .slide,
    .product-gallery-stacked .flickity-slider {
        position: relative !important;
        -ms-transform: none !important;
        transform: none !important;
        left: 0 !important;
        right: 0 !important
    }
    .product-gallery-stacked .slide {
        overflow: hidden
    }
    .product-gallery-stacked .slide:not(:last-child) {
        margin-bottom: 1.5em
    }
    .product-stacked-info {
        padding: 5vh 5% 2vh
    }
}

.product-thumbnails {
    padding-top: 0
}

.product-thumbnails a {
    overflow: hidden;
    display: block;
    border: 1px solid transparent;
    background-color: #FFF;
    -ms-transform: translateY(0);
    transform: translateY(0)
}

.product-thumbnails a:hover,
.product-thumbnails .is-nav-selected a {
    border-color: rgba(0, 0, 0, 0.2)
}

.product-thumbnails img {
    margin-bottom: -5px;
    opacity: 0.5;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    transition: transform 0.6s, opacity 0.6s
}

.product-thumbnails a:hover img,
.product-thumbnails .is-nav-selected a img {
    border-color: rgba(0, 0, 0, 0.3);
    -ms-transform: translateY(-5px);
    transform: translateY(-5px);
    opacity: 1
}

.vertical-thumbnails .row-slider:not(.flickity-enabled) {
    opacity: 0
}

@media screen and (min-width: 850px) {
    .vertical-thumbnails {
        overflow-x: hidden;
        overflow-y: auto
    }
    .vertical-thumbnails .col {
        position: relative !important;
        left: 0 !important;
        max-width: 100% !important;
        min-height: 0 !important;
        margin-left: 1px;
        width: 95% !important;
        right: 0 !important;
        padding: 0 0 15px !important
    }
    .vertical-thumbnails .flickity-slider,
    .vertical-thumbnails .flickity-viewport {
        -ms-transform: none !important;
        transform: none !important;
        overflow: visible !important;
        height: auto !important
    }
}

.product-footer .woocommerce-tabs {
    padding: 30px 0;
    border-top: 1px solid #ececec
}

.product-footer .woocommerce-tabs>.nav-line-grow,
.product-footer .woocommerce-tabs>.nav-line:not(.nav-vertical) {
    margin-top: -31px
}

#product-sidebar .next-prev-thumbs {
    margin: -.5em 0 3em
}

.product-sidebar-small {
    font-size: .9em
}

.product-sidebar-small .widget-title {
    text-align: center
}

.product-sidebar-small .is-divider {
    margin-left: auto;
    margin-right: auto
}

.product-sidebar-small ul.product_list_widget li {
    padding-left: 60px
}

.product-sidebar-small ul.product_list_widget li img {
    width: 50px;
    height: 50px
}

.product-section {
    border-top: 1px solid #ececec
}

.easyzoom-notice {
    display: none
}

.easyzoom-flyout {
    position: absolute;
    z-index: 1;
    overflow: hidden;
    background: #fff;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    pointer-events: none;
    transition: opacity 1s;
    animation-delay: 1s;
    animation: stuckFadeIn .6s
}

@media (max-width: 849px) {
    .easyzoom-flyout {
        display: none !important
    }
}

.product-gallery-slider .slide .easyzoom-flyout img {
    max-width: 1000px !important;
    width: 1000px !important
}

.woocommerce-product-gallery__trigger {
    display: none
}

.product-info .composite_form .composite_navigation .page_button {
    font-size: 1em;
    line-height: 1.2;
    font-weight: normal
}

.woocommerce-pagination ul.links li {
    margin-left: inherit
}

.has-sticky-product-cart {
    padding-bottom: 60px
}

.has-sticky-product-cart .back-to-top.active {
    bottom: 10px
}

.sticky-add-to-cart__product {
    display: none;
    -ms-flex-align: center;
    align-items: center;
    padding: 3px
}

.sticky-add-to-cart__product .product-title-small {
    margin-right: 1em;
    max-width: 180px;
    line-height: 1
}

.sticky-add-to-cart__product img {
    width: 45px;
    height: 45px;
    object-fit: cover;
    object-position: 50% 50%;
    margin-right: 1em;
    border-radius: 5px
}

.sticky-add-to-cart--active {
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 20;
    display: -ms-flexbox;
    display: flex;
    padding: 3px;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex-pack: center;
    justify-content: center;
    font-size: .9em;
    background-color: rgba(255, 255, 255, 0.9);
    border-top: 1px solid #ddd;
    animation: stuckMoveUp .6s
}

.sticky-add-to-cart--active .woocommerce-variation-description,
.sticky-add-to-cart--active .variations {
    display: none
}

.sticky-add-to-cart--active .woocommerce-variation-add-to-cart,
.sticky-add-to-cart--active .single_variation_wrap {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    -ms-flex-wrap: no-wrap;
    flex-wrap: no-wrap
}

.sticky-add-to-cart--active .woocommerce-variation-price,
.sticky-add-to-cart--active .product-page-price {
    margin-top: 0;
    margin-right: 0.9em;
    margin-bottom: 0;
    font-size: 15px;
    padding: 0;
    border: 0
}

.sticky-add-to-cart--active .quantity,
.sticky-add-to-cart--active form,
.sticky-add-to-cart--active button {
    margin-bottom: 0
}

.sticky-add-to-cart--active .sticky-add-to-cart__product {
    display: -ms-flexbox;
    display: flex
}

@media (max-width: 550px) {
    .sticky-add-to-cart--active {
        font-size: .8em
    }
}

.flex-viewport {
    max-height: 2000px;
    transition: all 1s ease;
    cursor: pointer
}

.flex-viewport a� {
    display: block
}

.flex-viewport img {
    width: 100%
}

.flex-control-thumbs {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin: 10px -5px 0 -5px
}

.flex-control-thumbs li {
    list-style: none;
    width: 25%;
    padding: 0 5px;
    cursor: pointer
}



.text-center .quantity,
.quantity {
    opacity: 1;
    display: inline-block;
    display: -ms-inline-flexbox;
    display: inline-flex;
    margin-right: 1em;
    white-space: nowrap;
    vertical-align: top
}

.text-center .button+.quantity,
.button+.quantity {
    margin-right: 0
}

.quantity+.button {
    margin-right: 0;
    font-size: 1em
}

.quantity .minus {
    border-right: 0 !important;
    border-top-right-radius: 0 !important;
    border-bottom-right-radius: 0 !important
}

.quantity .plus {
    border-left: 0 !important;
    border-top-left-radius: 0 !important;
    border-bottom-left-radius: 0 !important
}

.quantity .minus,
.quantity .plus {
    padding-left: 0.5em;
    padding-right: 0.5em
}

.quantity input {
    padding-left: 0;
    padding-right: 0;
    display: inline-block;
    vertical-align: top;
    margin: 0
}

.quantity input[type="number"] {
    max-width: 2.5em;
    width: 2.5em;
    text-align: center;
    border-radius: 0 !important;
    -webkit-appearance: textfield;
    -moz-appearance: textfield;
    appearance: textfield;
    font-size: 1em
}

@media (max-width: 549px) {
    .quantity input[type="number"] {
        width: 2em
    }
}

.quantity input[type="number"]::-webkit-outer-spin-button,
.quantity input[type="number"]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0
}

.cart-icon {
    display: inline-block
}

.cart-icon strong {
    border-radius: 0;
    font-weight: bold;
    margin: .3em 0;
    border: 2px solid #446084;
    color: #446084;
    position: relative;
    display: inline-block;
    vertical-align: middle;
    text-align: center;
    width: 2.2em;
    height: 2.2em;
    font-size: 1em;
    line-height: 1.9em;
    font-family: Helvetica, Arial, Sans-serif
}

.cart-icon strong:after {
    transition: height .1s ease-out;
    bottom: 100%;
    margin-bottom: 0;
    margin-left: -7px;
    height: 8px;
    width: 14px;
    left: 50%;
    content: ' ';
    position: absolute;
    pointer-events: none;
    border: 2px solid #446084;
    border-top-left-radius: 99px;
    border-top-right-radius: 99px;
    border-bottom: 0
}

.current-dropdown .cart-icon strong,
.cart-icon:hover strong {
    background-color: #446084;
    color: #fff
}

.current-dropdown .cart-icon strong:after,
.cart-icon:hover strong:after {
    height: 10px
}

.nav-small .cart-icon {
    font-size: .66em
}

.nav-dark .cart-icon strong {
    color: #fff;
    border-color: #fff
}

.nav-dark .current-dropdown .cart-icon strong,
.nav-dark .cart-icon:hover strong {
    background-color: #fff;
    color: #446084
}

.nav-dark .cart-icon strong:after {
    border-color: #fff
}

.loading .cart-icon strong,
.loading .cart-icon strong:after {
    border-color: #7a9c59;
    color: #7a9c59
}

.loading .cart-icon:hover strong {
    background-color: #7a9c59;
    color: #FFF
}

.header-cart-icon {
    position: relative
}


@media (min-width: 850px) {
    .off-canvas .off-canvas-cart {
        width: 320px
    }
}

.cross-sells h2 {
    font-size: 1.2em;
    margin-bottom: 1em
}

.shop_table ul li,
.checkout ul li {
    list-style: none;
    margin: 0
}

.shop_table .quantity {
    margin: 0
}

.shop_table thead th,
.shop_table .order-total td,
.shop_table .order-total th {
    border-width: 3px
}

.shop_table th:last-child {
    border-right: 0
}

.shop_table .cart_item td {
    padding-top: 15px;
    padding-bottom: 15px
}

.shop_table .actions {
    border: 0;
    padding: 15px 0 10px
}

.shop_table .submit-col {
    padding-left: 30px
}

@media (max-width: 849px) {
    .shop_table {
        font-size: .9em
    }
    .shop_table tr.shipping th {
        width: 50%
    }
    .shop_table .product-name {
        min-width: 80px
    }
    .shop_table .product-remove {
        position: relative;
        width: 0
    }
    .shop_table .product-remove a {
        position: absolute;
        top: 10px;
        left: 0px;
        width: 24px;
        height: 24px;
        line-height: 18px !important;
        font-size: 18px !important
    }
    .shop_table .mobile-product-price {
        margin: .5em 0
    }
}

@media (max-width: 549px) {
    .shop_table .product-price {
        display: none
    }
    .shop_table .product-subtotal {
        display: none
    }
    .product-quantity {
        text-align: right
    }
    .cross-sells {
        overflow: hidden
    }
}

.cart_totals tbody th {
    font-size: .9em;
    text-transform: inherit;
    letter-spacing: 0;
    font-weight: normal
}

.cart_totals>h2 {
    display: none
}

.cart_totals .button {
    min-width: 100%;
    margin-right: 0;
    display: block
}

.cart_totals .wc-proceed-to-checkout {
    margin: 1.5em 0
}

.shipping__table {
    margin: 0
}

.shipping__inner {
    border: 0;
    padding: 0;
    font-size: 1em
}

.shipping__list {
    margin-bottom: 0
}

.shipping__list_item {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: center;
    align-items: center;
    min-height: 2em
}

.shipping__list_label {
    font-weight: normal;
    margin: 0;
    padding: .5em 0;
    opacity: .8;
    -ms-flex: 1;
    flex: 1
}

.shipping.shipping--boxed .shipping__list {
    padding: 0
}

.shipping.shipping--boxed .shipping__list_item {
    background-color: rgba(0, 0, 0, 0.01);
    border: 1px solid rgba(0, 0, 0, 0.1);
    padding: .3em .6em;
    border-radius: 3px;
    margin-bottom: 5px;
    min-height: 2.4em
}

.shipping__table--multiple {
    display: block;
    text-align: left
}

.shipping__table--multiple tbody,
.shipping__table--multiple tr,
.shipping__table--multiple th,
.shipping__table--multiple td {
    display: block;
    text-align: left;
    padding-left: 0
}

.shipping__table--multiple th {
    border: 0
}

.shipping__table--multiple .shipping__list {
    padding: 0 .2em
}

.shipping__table--multiple .woocommerce-shipping-calculator,
.shipping__table--multiple .woocommerce-shipping-destination {
    text-align: left
}

.shipping__table--multiple .shipping__list_label {
    text-align: left
}

tr.shipping input:checked+label,
tr.shipping input:hover+label,
tr.shipping label:hover {
    opacity: 1
}

tr.shipping input:checked+label {
    font-weight: bold
}

tr.shipping input {
    margin-bottom: 0;
    margin-top: -2px
}

tr.shipping span.amount {
    margin-left: .2em
}

.woocommerce-shipping-calculator {
    margin-top: .5em;
    margin-bottom: 0
}

.woocommerce-shipping-destination,
.shipping-calculator-button {
    font-size: .9em
}

.shipping-calculator-form {
    background-color: rgba(0, 0, 0, 0.03);
    padding: 15px 15px 10px;
    border-radius: 5px;
    margin-top: 5px
}

.cart-discount {
    background-color: rgba(122, 156, 89, 0.2);
    font-size: .85em
}

.cart-discount th,
.cart-discount td {
    padding: 10px 5px
}

.cart-sidebar .widget-title {
    border-bottom: 3px solid #ececec;
    font-size: .95em;
    padding-bottom: 10px;
    margin-bottom: 15px
}

.widget_shopping_cart .button {
    width: 100%;
    margin: .5em 0 0
}

.widget_shopping_cart li.empty {
    padding: 0 10px !important;
    margin: 0;
    min-height: 0;
    text-align: center
}

.widget_shopping_cart p.total {
    text-align: center;
    padding: 10px 0;
    border-top: 1px solid #ececec;
    border-bottom: 2px solid #ececec;
    margin-bottom: .5em
}

.dark .widget_shopping_cart p.total {
    border-color: rgba(255, 255, 255, 0.2)
}

.nav-dropdown .product_list_widget {
    overflow-y: auto;
    -webkit-overflow-scrolling: touch;
    max-height: 500px;
    max-height: 50vh
}

.tagcloud {
    padding-bottom: 15px
}

.tagcloud a {
    font-size: 0.8em !important;
    display: inline-block;
    border: 1px solid currentColor;
    opacity: .8;
    margin: 0 3px 5px 0;
    padding: 2px 5px;
    border-radius: 3px
}

.tagcloud a:hover {
    opacity: 1;
    border-color: #446084;
    background-color: #446084;
    color: #fff
}

.variations {
    position: relative
}

.variations td {
    vertical-align: middle;
    padding: .2em 0;
    border: 0
}

.variations .reset_variations {
    position: absolute;
    right: 0;
    bottom: 95%;
    color: currentColor;
    opacity: 0.6;
    font-size: 11px;
    text-transform: uppercase
}
.add-to-cart-button:hover span.amount{
	color: #fff !important;

}
.woocommerce-Price-amount .amount {
color:#446084!important;
}
span.amount {
    white-space: nowrap;
    color: #446084 !important;
    font-weight: bold;
}
span.amount:hover {
color:#fff;
}
.box-premalink {
	position:absolute;
	bottom:0 ;
	left:0 ;
	right:0;
	color : #fff;
	background : #337ab7;
	width:100%;
	text-align:center;
	height: 40px;
	display:none;
}
.box-premalink a {
  color : #fff;
  text-align:center;
 text-transform: uppercase;
  padding:20px;
}
.box-premalink a:hover {
	color:#fff;
}
.box-image:hover .box-premalink {
	display:flex;
	align-items:center;
	justify-content:center;
	display:none;

}

        .table {
  width: 100%;
  max-width: 100%;
  margin-bottom: 1rem;
}
.status-button-wrap li:before {
		content : "";
}
.status-button-wrap li:after {
		content : "" !important;
}

.table th,
.table td {
  padding: 0.75rem;
  vertical-align: top;
  border-top: 1px solid #eceeef;
}

.table thead th {
  vertical-align: bottom;
  border-bottom: 2px solid #eceeef;
}

.table tbody + tbody {
  border-top: 2px solid #eceeef;
}

.table .table {
  background-color: #fff;
}

.table-sm th,
.table-sm td {
  padding: 0.3rem;
}

.table-bordered {
  border: 1px solid #eceeef;
}

.table-bordered th,
.table-bordered td {
  border: 1px solid #eceeef;
}

.table-bordered thead th,
.table-bordered thead td {
  border-bottom-width: 2px;
}

.table-striped tbody tr:nth-of-type(odd) {
  background-color: rgba(0, 0, 0, 0.05);
}

.table-hover tbody tr:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-active,
.table-active > th,
.table-active > td {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover > td,
.table-hover .table-active:hover > th {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-success,
.table-success > th,
.table-success > td {
  background-color: #dff0d8;
}

.table-hover .table-success:hover {
  background-color: #d0e9c6;
}

.table-hover .table-success:hover > td,
.table-hover .table-success:hover > th {
  background-color: #d0e9c6;
}

.table-info,
.table-info > th,
.table-info > td {
  background-color: #d9edf7;
}

.table-hover .table-info:hover {
  background-color: #c4e3f3;
}

.table-hover .table-info:hover > td,
.table-hover .table-info:hover > th {
  background-color: #c4e3f3;
}

.table-warning,
.table-warning > th,
.table-warning > td {
  background-color: #fcf8e3;
}

.table-hover .table-warning:hover {
  background-color: #faf2cc;
}

.table-hover .table-warning:hover > td,
.table-hover .table-warning:hover > th {
  background-color: #faf2cc;
}

.table-danger,
.table-danger > th,
.table-danger > td {
  background-color: #f2dede;
}

.table-hover .table-danger:hover {
  background-color: #ebcccc;
}

.table-hover .table-danger:hover > td,
.table-hover .table-danger:hover > th {
  background-color: #ebcccc;
}

.thead-inverse th {
  color: #fff;
  background-color: #292b2c;
}

.thead-default th {
  color: #464a4c;
  background-color: #eceeef;
}

.table-inverse {
  color: #fff;
  background-color: #292b2c;
}

.table-inverse th,
.table-inverse td,
.table-inverse thead th {
  border-color: #fff;
}

.table-inverse.table-bordered {
  border: 0;
}

.table-responsive {
  display: block;
  width: 100%;
  overflow-x: auto;
  -ms-overflow-style: -ms-autohiding-scrollbar;
}

.table-responsive.table-bordered {
  border: 0;
}




.add_to_cart_button{
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
}
.loader {
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
	 position: absolute;
    left: 50%;
    top: 35;
	 z-index: 1000;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
.filter-wrapper {
    background: #dad9d9;
    margin-bottom: 10px;
    padding: 0px 20px 0px 15px;
    display: inline-block;

}
.button_example {
width: 100%;
    text-transform: uppercase;
    line-height: 28px;
    height: 30px;
    border-radius: 3px;
    border: 1px solid #bbbaba;
    font-size: 9px;
    font-family: arial, helvetica, sans-serif;
    padding: 0px 22px 0px 13px;
    text-decoration: none;
    display: inline-block;
    color: #000;
    background: rgba(253,253,253,1);
    background: -moz-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
    background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(253,253,253,1)), color-stop(12%, rgba(250,250,250,1)), color-stop(28%, rgba(247,247,247,1)), color-stop(49%, rgba(234,232,232,1)), color-stop(76%, rgba(219,217,217,1)), color-stop(100%, rgba(213,210,210,1)));
    background: -webkit-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
    background: -o-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
    background: -ms-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
    background: linear-gradient(to bottom, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
}

 .add_to_cart_button {
    color: #fff !important;
    background-color: #5cb85c;
    border-color: #4cae4c;

}
.status-button-wrap li {
    list-style: none !important;
    padding-left: 10px;
    display: inline-block;
    list-style-type: none !important;
}
.table .btn-clickable span {
    display: inline-block;
    background: #459e45;
    padding: 2px 7px;
    width: 29px;
    margin: -7px 2px -7px -8px;
    font-size: 17px;
    line-height: 26px;
}
.table {
 font-size:11px;
}
.table > p {
font-size:11px;
}
.table tr td {
font-size:11px;
}
table > tbody > tr td, .table > tbody > tr td {

 font-size : 11px !important;
}
.torin-calc table > thead > tr > th, .table > thead > tr > th, table.table>thead:first-child>tr:first-child>th {
 font-size:11px;
}


.section.content  .table > tbody > tr > td > p {
font-size:11px !important;
}

.table tr {
    background:  #e4e4e4;

 border:transparent !important;
}
.table tr td {
	 border:transparent !important;
}


.table tr:nth-child(4n+1)  {
    background: #f6f6f6 ;
   border-top : 1px #f6f6f6 solid !important;


  }

.table tr:nth-child(4n+2)  {
    background: #f6f6f6;
  border-top : 1px #d2d2d2 solid !important;



}

 .table-wrapper .row-vm.shown-even,
 .table-wrapper .row-vm.shown-even + .row-details,
.table-wrapper .row-vm.shown,
 .table-wrapper .row-vm.shown + .row-details { display: table-row; background: #f6f6f6; }
.table-wrapper .row-vm.shown-even,
 .table-wrapper .row-vm.shown-even + .row-details { background: #e4e4e4; }
.table-wrapper .row-vm.hidden,
 .table-wrapper .row-vm.hidden + .row-details { display: none; }
.csv-data {


}
table > tbody > tr > td.csv-data > img:hover {
    z-index: 9999;
    outline: 1px solid #a9a9a9;
    transform:scale(5) !important;
    outline-offset: -1px;
}
table > tbody > tr > td.csv-data > ximg:hover {
    transform:scale(5) !important;
    -ms-transform:scale(5) !important; /* IE 9 */
    -moz-transform:scale(5) !important; /* Firefox */
    -webkit-transform:scale(5) !important; /* Safari and Chrome */
    -o-transform:scale(5) !important; /* Opera */
  z-index: 100000000;
	outline: 1px solid #a9a9a9  ;
outline-offset: -1px;
	margin:0;
	 padding:0;
	width: 50% !important;
 overflow : hidden;

}


table > tbody > tr > td.csv-data > img {

  width :50px !important;


}

</style>

<style>
    hr {
        margin-top: 40px;
        margin-bottom: 40px;
        border: 0;
        border-top: 1px solid #7b7b7b;
    }
    .torin-calc .form-horizontal .form-group{
        margin-bottom: 10px;
        margin-right: 0;
        margin-left: 0;
    }
    .torin-calc .btn-custom-dropdown {
        background-color: hsl(12, 4%, 80%) !important;
        background-repeat: repeat-x;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#12f133134", endColorstr="#cecac9");
        background-image: -khtml-gradient(linear, left top, left bottom, from(#12f133134), to(#cecac9));
        background-image: -moz-linear-gradient(top, #12f133134, #cecac9);
        background-image: -ms-linear-gradient(top, #12f133134, #cecac9);
        background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #12f133134), color-stop(100%, #cecac9));
        background-image: -webkit-linear-gradient(top, #12f133134, #cecac9);
        background-image: -o-linear-gradient(top, #12f133134, #cecac9);
        background-image: linear-gradient(#12f133134, #cecac9);
        border-color: #cecac9 #cecac9 hsl(12, 4%, 70%);
        color: #333 !important;
        text-shadow: 0 1px 1px rgba(255, 255, 255, 0.66);
        -webkit-font-smoothing: antialiased;
    }
    .button_example{
		width:100%;
        text-transform: uppercase;
        line-height: 28px;
        height: 30px;
        border-radius: 3px;
        border: 1px solid #bbbaba;
        font-size: 9px;
        font-family: arial, helvetica, sans-serif;
        padding: 0px 22px 0px 13px;
        text-decoration: none;
        display: inline-block;
        color: #000;
        background: rgba(253,253,253,1);
        background: -moz-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(253,253,253,1)), color-stop(12%, rgba(250,250,250,1)), color-stop(28%, rgba(247,247,247,1)), color-stop(49%, rgba(234,232,232,1)), color-stop(76%, rgba(219,217,217,1)), color-stop(100%, rgba(213,210,210,1)));
        background: -webkit-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
        background: -o-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
        background: -ms-linear-gradient(top, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
        background: linear-gradient(to bottom, rgba(253,253,253,1) 0%, rgba(250,250,250,1) 12%, rgba(247,247,247,1) 28%, rgba(234,232,232,1) 49%, rgba(219,217,217,1) 76%, rgba(213,210,210,1) 100%);
	}

    .button_example:hover{
        border: 1px solid #bbbaba;
        background: rgba(232,232,232,1);
        background: -moz-linear-gradient(top, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(232,232,232,1)), color-stop(19%, rgba(230,230,230,1)), color-stop(36%, rgba(230,230,230,1)), color-stop(60%, rgba(230,230,230,1)), color-stop(81%, rgba(230,230,230,1)), color-stop(100%, rgba(213,210,210,1)));
        background: -webkit-linear-gradient(top, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        background: -o-linear-gradient(top, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        background: -ms-linear-gradient(top, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        background: linear-gradient(to bottom, rgba(232,232,232,1) 0%, rgba(230,230,230,1) 19%, rgba(230,230,230,1) 36%, rgba(230,230,230,1) 60%, rgba(230,230,230,1) 81%, rgba(213,210,210,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e8e8e8', endColorstr='#d5d2d2', GradientType=0 );
    }
    .filter-wrapper .filter-container .filter-item select {
        font-size: 9px;
        text-transform: uppercase;
    }
    .torin-calc .final{
        background-color: #ffd700;
    }
    .torin-calc .request-final{
        color:#B52C3E;
        font-weight: bold;
        text-transform: uppercase;
        text-decoration: underline;
        letter-spacing: 0.1px;
		font-size: 11px;
    }
    .torin-calc .underline{
        text-decoration: underline;
        text-transform: uppercase;
        color: #000;
		font-size: 11px;
    }
    .torin-calc .cust-scrollbar{
        overflow-x: hidden;
        max-height: 558px;
    }

    .force-overflow-cust
    {
        min-width: 847px;
    }
    .force-overflow-cust table{
        margin-left: -15px;
        height: 550px;
    }

    #style-2::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 1px rgba(0,0.3,0,0);
        /*-webkit-box-shadow: inset 0 0 1px rgba(0,0,0,0.3);*/
        background-color: #F5F5F5;
    }


    .quotation .table thead tr{
        background-color: #eee;
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#f2f2f2), to(#fafafa));
        background-image: -webkit-linear-gradient(top, #f2f2f2 0, #fafafa 100%);
        background-image: -moz-linear-gradient(top, #f2f2f2 0, #fafafa 100%);
        background-image: -ms-linear-gradient(top, #f2f2f2 0, #fafafa 100%);
        background-image: -o-linear-gradient(top, #f2f2f2 0, #fafafa 100%);
        font-size: 12px
    }
    .table-bordered {
        border: 1px solid #ddd;
    }
    .torin-calc table > thead > tr > th, .table > thead > tr > th, table.table>thead:first-child>tr:first-child>th{
        border:none;
		text-align: center;
    }
    .torin-calc .table thead tr{
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#f2f2f2), to(#fafafa));
    }
    .torin-calc .table>thead>tr>th{
        vertical-align: bottom;
        border-bottom: 2px solid #ddd;
        padding: 8px;
        line-height: 1.428571429;
    }
    .torin-calc .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        padding: 8px;
        line-height: 1.428571429;
        vertical-align: top;
        border:none;
    }
    .table tr{
        border: 1px solid #ddd;
    }

    .list-btns {
        margin: 0;
        padding: 0;
        list-style: none !important;
        list-style-type: none !important;
    }
    .list-btns li{
        margin-top: 10px;
    }
    .bg-color-red {
        background-color: rgb(237, 26, 36) !important;
        border-radius: 0;
    }
    .list-btns .txt-color-white{
        color: #fff;
        text-transform: uppercase;
        font-size: 12px;
        font-weight: bold;
    }
    .btn-block{
        padding: 5px 16px;
        display: block;
        width: 100%;
    }
    .img-logo{
        border: 2px solid #ccc;
        float: left;
        width: 150px;
        height: 100px;
        position: relative;
        margin-bottom: 10px;
    }
    .img-logo .img-responsive{
        height: 100%;
        width: 100%;
    }
   .quotation-quote .button {
        background-color: transparent;
        text-align: center;
        display: inline-block;
        text-decoration: none;
        border: 1px solid;
        -webkit-transition: all 0.2s ease-in;
        -moz-transition: all 0.2s ease-in;
        -o-transition: all 0.2s ease-in;
        transition: all 0.2s ease-in;
        padding: 10px 30px 8px;
        font-size: 16px;
        padding: 5px 8px 5px;

        font-weight: 300;
        letter-spacing: 0.2rem;
        line-height: 1;
        text-transform: uppercase;
        border-radius: 0;
    }
    .quotation-quote .button:hover{
        text-decoration: none;
        cursor: pointer;
        color: #000;
    }
    .quotation-quote .no-left-margin{
        margin-left: 0;
    }
    .quotation-quote .button-neutral{
        color: #000;
        border-color: #000;
        font-size: 11px;
    }
    .quotation-quote .button-neutral:hover{
        color: #fff;
        background-color: rgb(237, 26, 36);
        border-color:rgb(237, 26, 36);
    }
    .quotation-quote button.button.button-neutral.btn-xs {
        margin-bottom: 10px;
    }

/*


    .center-td{
        text-align: center;
    }
    .border-tbl{
        border: 1px solid #ccc;
    }
    .search-input{
        width: 25%;
        float: right;
        margin-bottom: 10px;
    }
*/

    .stylish-input-group .input-group-addon{
        background: white !important;
    }
    .stylish-input-group .form-control{
        border-right:0;
        box-shadow:0 0 0;
        border-color:#ccc;
    }
    .stylish-input-group button{
        border:0;
        background:transparent;
    }
    .page-template .sidebar_wrap h3 {
        /* color: #151515;
        margin: 15px 15px 15px 0;
        background: #FFF;
        box-shadow: 1px 1px 1px #929292; */
    }
    .page-template .sidebar_wrap h3 img {
        max-width: 100%;
    }
    .torin-calc .nav-tabs>li>a{
        color:#000;
        border-radius: 1px;
    }

    .scrollbar /*gale*/
    {
        float: left;
        width: 847px;
        background: #F5F5F5;
        overflow-y: scroll;
        margin-bottom: 25px;
    }

    .force-overflow
    {
        min-width: 847px;
    }
    .force-overflow table{
        margin-left: -15px;
        width: 847px;
    }

    #style-3::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 1px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }

    #style-3::-webkit-scrollbar
    {
        width: 1px;
        background-color: #F5F5F5;
    }

    .csv-tbl .input-sm{
          border-radius: 0;
      }
    .csv-tbl input[type="text"]{
        /*width: 100px;*/
        font-size: 12px;
        padding: 5px;
    }
    .btn-save-csv {
        margin-bottom: 50px;
    }
    .csv-tbl >tbody>tr>td{
        padding:0;
    }
    .csv-tbl .tbl-header{
        font-size: 11px;
    }
    .black-th{
        background-color: #333333;
        color: #fff;
    }
  /*  .black-th.action{
        font-size: 11px;
    }*/
    table .glyphicon-remove{
        text-align: center;
        color: red;
    }
    .glyphicon-remove{
        cursor: pointer;
    }
    label.btn.btn-default.semiBold {
        background-color: #333333;
        color: #fff;
    }
    .torin-calc .nav>li>a{
        padding:15px 15px;
        color: #000;
        font-size: 14px;
        font-weight: bold;
    }
    .torin-calc .nav-tabs>li>a:hover, .torin-calc .nav-tabs>li.active>a{
        color:#fff;
        background-color: #333333;
    }
    .package-table tbody tr:nth-child(odd){background:#f5f4f4;}
    /*.package-table td{border-top:0px !important; border-right:1px solid #fff !important;}*/
    .package-table tbody tr:nth-child(even){background:#ececec; }
    .package-table tbody > tr td {
        font-size: 15px !important;
        font-weight: 600 !important;
        color: #333333;
        padding: 20px 0px 20px 31px !important;
    }
    .package-table > tbody > tr:first-child td {font-weight: normal;}
    .package-table span.glyphicon.glyphicon-edit{
        color:#000;
        font-size: 15px;
    }
    .package-table td, .package-table tr{border: none!important; padding: 5px 10px;}
    .package-table > tbody > tr > td{
        padding: 10px 10px 10px;
        height: 60px;
        vertical-align: middle;
    }
    .torin-calc .view-survey{
        color: #000;
    }

	.torin-calc .view-survey:hover,
    .toric-calc .view-survey:focus {
        text-decoration: none !important;
    }

    table .glyphicon-remove{
        padding-top: 7px;
    }
    .torin-calc .panel{
        border-radius: 0;
    }
    .torin-calc .panel-body {
        padding: 10px;
    }

    .torin-calc .panel-body p{
        margin-bottom: 0 !important;
        font-size: 13px;
    }
    .torin-calc .filter-wrapper .filter-container .filter-item select{
        height: auto;
    }
    .torin-calc .new-quote{
        border-radius: 0;
        min-width: 108px;
        padding: 3px;
        border: 0;
        background-color:#000;
        margin-top: 15px;
        margin-bottom: 15px;
        color:#fff;
    }
    .torin-calc .new-quote:hover{
        color:#fff;
    }
    .torin-calc .asc-btn{
        padding: 0;
        font-size: 13px;
        border-radius: 1px;
        color: #fff;
        border: 0;
        background-color: #000;
        outline:0;
    }
    .torin-calc .asc-btn:focus{
        outline: 0;
    }
    p.table-top-h1{
        text-transform: uppercase;
        font-weight: bold;
        font-size: 11px !important;
        margin-bottom: 3px !important;
        margin-left: 9px;
    }
    select.button_example.form-control{
        border-radius:0;
    }
    p.table-top-h1{
        text-transform: uppercase;
        font-weight: bold;
        font-size: 15px !important;
        margin-bottom: 3px !important;
        margin-left: 9px;
    }
    /*
    table.tablesorter thead tr .header{
        background-image: url(../torin/public/img/bg.png);
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }

    th.header.headerSortUp{
        background-image: url(../torin/public/img/trans-asc.png) !important;
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }
    th.header.headerSortDown{
        background-image: url(../torin/public/img/trans-desc.png) !important;
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }
    */
    th.center{
        text-align: center!important;
    }
    a.final.btn.btn-primary.csv-data:hover,a.initial.btn.btn-primary.csv-data:hover{
        cursor: pointer !important;
    }

    table.tablesorter thead tr .header {
        position: relative;
    }

    table.tablesorter thead tr .header:after {
        background-image: url(../torin/public/img/bg.png);
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
        display: block;
        height: 10px;
        width: 18px;
        content: '';
        position: absolute;
        right: -12px;
        top: 18px;
        /*top: 50%;*/
        /*transform: translateY(-50%);*/
        z-index: 1;
    }
    table.tablesorter thead tr .headerSortUp:after {
        background-image: url(../torin/public/img/trans-asc.png) !important;
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }
    table.tablesorter thead tr .headerSortDown:after {
        background-image: url(../torin/public/img/trans-desc.png) !important;
        background-repeat: no-repeat;
        background-position: center right;
        cursor: pointer;
    }
    table.tablesorter thead tr .header:last-child:after {
        right: 0px;
    }
     table.tablesorter thead tr .header:first-child:after {
        right: -5px;
     }

     table.tablesorter.customer-tbl thead tr .header:first-child:after  {
        right: 0px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(2):after  {
        right: -8px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(3):after  {
        right: -4px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(4):after  {
        right: 0px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(5):after  {
        right: -11px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(6):after  {
        right: 10px;
    }
     table.tablesorter.customer-tbl thead tr .header:nth-child(7):after  {
        right: 3px;
     }
     table.tablesorter.customer-tbl thead tr .header:nth-child(8):after  {
        right: -6px;
     }
     table.tablesorter.customer-tbl thead tr .header:last-child:after  {
       right: 3px;
     }

     /* ADMIN */

     table.tablesorter.admin-tbl thead tr .header:first-child:after  {
        right: -5px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(2):after  {
        right: -10px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(3):after  {
        right: -9px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(4):after  {
        right: -5px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(5):after  {
        right: -4px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(6):after  {
        right: -14px;
    }
     table.tablesorter.admin-tbl thead tr .header:nth-child(7):after  {
        right: 4px;
     }
     table.tablesorter.admin-tbl thead tr .header:nth-child(8):after  {
        right: -3px;
     }
      table.tablesorter.admin-tbl thead tr .header:nth-child(9):after  {
        right: -10px;
     }
     table.tablesorter.admin-tbl thead tr .header:last-child:after  {
       right: -2px;
     }

    /** search input box **/
    .reset-btn{
        width: 8%;
        height:31px;
        float: right;
        display: inline-block;
        margin-right: 36px;
    }
    #search-input {
        height:31px;
        display: inline-block;
    }
    .search_wrapper .form-group{
        width: 350px;
    }
    span.glyphicon.glyphicon-search.form-control-feedback{
        right: 2px;
    }
    .filter-reset-btn{
        margin-left: 7px;
        display: block;
        float: left;
        margin-bottom: 5px;
        line-height: 34px;
        text-align: center;
    }
    .filter-reset-btn a{
        font-size: 12px;
    }
    .filter-reset-btn a:hover{
        color:#000;
        text-decoration: underline;
    }
    #purchased input{
        border-radius: 0px;
        padding: 5px;
        height: 25px;
    }
    #purchased .row{
        margin-bottom: 5px;
    }

    #purchased .btn-primary{
        border-radius: 0;
        min-width: 108px;
        padding: 3px;
        border: 0;
        background-color: #000;
        margin-top: 15px;
        margin-bottom: 15px;
        color: #fff;
        text-transform: uppercase !important;
        padding: 9px 20px !important;
        font-size: 15px;
        font-weight: bold;
        letter-spacing: -0.5px !important;
    }

    .po-error, .so-error{
        /*text-align: center;*/
        margin-top: 4px;
        font-size: 11px !important;
    }
    span#purchase-error{
        color: #FF0004;
    }
    .torin-calc .purchased {
        background-color: #00A82D;
    }
    a.purchased.btn.btn-primary.csv-data:hover{
        cursor: default !important;
    }
    .torin-calc .purchased:hover,
    .toric-calc .purchased:focus {
        text-decoration: none !important;
        background-color: #00A82D;
    }
    .custom {
        background-color: #fb6d29;
    }

    #csv-table-form .table-scrollbar{
        background-color:#fff;
        overflow-y: hidden;
    }
    table#csvtbl{
        width: auto !important;
    }

    #style-3::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 1px rgba(0,0.3,0,0);
        background-color: #F5F5F5;
    }
  #checkOutModal .modal-dialog { width: 940px; max-width: calc(100% - 20px) }

    #style-3::-webkit-scrollbar
    {
        width: 15px;
        background-color: #F5F5F5;
        height: 15px;
    }
    #style-3::-webkit-scrollbar-thumb{
        background-color: #000;
    }

    .dashboard_content .table-wrapper table tr.row-vm:nth-child(odd), .dashboard_content .table-wrapper table tr.row-vm:nth-child(odd) {
    border-bottom: 1px solid #dedede !important;
}


.dashboard_content .table-wrapper table tr.row-details {
    text-decoration: none;
}
.dashboard_content .table-wrapper table tr.row-details {
    text-transform: uppercase;
    text-decoration: underline;
    border-top: 1px solid #f5f5f5 !important;
}
.dashboard_content .table-wrapper table tr:nth-child(even), .dashboard_content .table-wrapper table tr:nth-child(even) {
    background:  #f6f6f6 !important;
}
.dashboard_content .table-wrapper table tr {
    background: #f6f6f6 !important;
    border-bottom:1px solid #ffffff !important;
}


.form-group {
    margin-bottom :5px;
}
.btn_wrapper {
	display: flex;

}
.btn_wrapper div {
	padding-right: 7px;
}
.purchase-step .input-wrap { padding: 2px 10px; }
.purchase-step input[type="checkbox"] { width: 17px; height: 17px; }
.status-button-wrap { white-space:nowrap; }
.status-button-wrap li { list-style:none; padding-left:10px; display:inline-block; }
.status-button-wrap li a { cursor:pointer; }
#pdf-preview { width: 100%; height: 400px; height: calc(100vh - 250px); max-height: 687px; }
#checkOutModal .modal-dialog { width: 940px; max-width: calc(100% - 20px) }
.esign-wrap .btn { display: block; width: 548px; margin: -4px 0 0; border-radius: 0px; }
#cust-table-form .btn-clickable { cursor: pointer; pointer-events: visible; }
#cust-table-form .btn-clickable span {
		display: inline-block;
		background: #459e45;
		padding: 2px 7px;
		width: 29px;
		margin: -7px 2px -7px -8px;
		font-size: 17px;
		line-height: 26px;
}
.tdm_table { width: 100%; }
.tdm_table th, .tdm_table td { padding: 6px 10px; }
.tdm_include { width: 32px; text-align: center; line-height: 18px; }
.tdm_include input { width: 17px; height: 17px; }
.tdm_name { width: 260px; }
.tdm_qty { width: 52px; text-align: center; }
.tdm_subhead { font-weight: bold; }
.tdm_subhead .tdm_item { text-align: right; }
.tdm_price { width: 140px; }
td.tdm_price { text-align: right; }
.tdm_price i { font-style: normal; float: left; }
.row.row-xs { margin: 0 -2px; }
.row-xs [class^="col-"] { padding-left: 2px; padding-right: 2px; }
.row .mb4 { padding-bottom: 4px; }
.modal-header {
    background: #333;
    color: #fff;
    font-size: 19px;
    padding: 24px 20px;
    text-shadow: 1px 1px 3px #000;
    text-align: center;
}

.wc_payment_methods {

    display:none;
}
.place-order {
    display:none;
}
#payment {

    display:none;
}
.order-wrapper {
    border: 1px solid #176fa5;
    margin: 10px;
}


.order-wrapper >table {
    background: transparent !important;
    width: 100%;
}
.order-wrapper table > thead > tr:first-child>th {
    width: 0px;
    padding: 0px 6px !important;
    height: 48px;
    vertical-align: middle;
    background-color: transparent !important;
    color: #333;
    border: 1px solid #ddd;
}
.order-wrapper table > thead > tr:first-child>th {
    width: 0px;
    padding: 0px 6px !important;
    height: 48px;
    vertical-align: middle;
    background-color: transparent !important;
    color: #333;
    border: 1px solid #ddd;
}
span.amount {
    white-space: nowrap;
    color: #333 !important;
    font-weight: bold;
}

.dashboard_content .table-wrapper table tr.row-vm:nth-child(4n+1), .dashboard_content .table-wrapper table tr.row-vm:nth-child(4n+2) {
    border-bottom: transparent !important;
}

.dashboard_content .table-wrapper table tr {
    background: #e4e4e4 !important;
}

.dashboard_content .table-wrapper table tr:nth-child(even) {background: #dedede !important;}
.dashboard_content .table-wrapper table tr:nth-child(odd) {background: #f6f6f6 !important;}
#myTable_length,#myTable_filter {
display: none;
}
body {
	 padding-right: 0px !important;
}
#picture:after {
    background-image:none !important;
}

.shop_table tr .product-total {
  text-align: right !important;

}

.shop_table tfoot tr > td {
		text-align: right !important;
}
</style>
<link href="/torin/public/css/bootstrap-datepicker3.css" rel="stylesheet">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<section class="header-image-new">

  <div class="wOuter">
    	<div class="wInner">
		<h1><?=the_title()?></h1>
			         </div>
  </div>
</section>
<section class="content">
  <div class="container">


		<div class="bg-side"></div>

		<div class="row">

		  <div class="col-md-3 sidebar_wrap welcome-left">
			<?php
				get_sidebar('menu');
			?>
		  </div>
		  <div class="col-md-9 content_wrap welcome-right">
          <?php 
                            if( !is_user_logged_in() ):
                        ?>
                        <div class="row">
                                <div class="col-md-12">
                                <div class="alert alert-danger">You are on a guest account, please login if you want to use your customer account.</div>
                                </div>
                        </div>
                    <?php endif; ?>
					<?php
					    $taxonomyName = "product_cat";
					    $parent_terms = get_terms( $taxonomyName, array( 'parent' => 0, 'orderby' => 'slug', 'hide_empty' => false ) );

							//echo var_dump($parent_terms);
    					$machine_type = get_terms( $taxonomyName, array( 'parent' => 20, 'orderby' => 'slug', 'hide_empty' => false ) );
    					$machine_model = get_terms( $taxonomyName, array( 'parent' => 21, 'orderby' => 'slug', 'hide_empty' => false ) );
   						$part_type = get_terms( $taxonomyName, array( 'parent' => 26, 'orderby' => 'slug', 'hide_empty' => false ) );

							$mc_type = $_GET['machine_type'];
							$mc_model = $_GET['machine_model'];
							$pc_type = $_GET['part_type'];
							$mc_number = $_GET['model_number'];



    ?>
	<div class="row">
			<div class="col-md-6">
				<div class="add-to-cart-button pull-left"><a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>"><?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?> - <?php echo WC()->cart->get_cart_total(); ?> </a></div>

			</div>
			<div class="col-md-3">
	<?php

	global $woocommerce;
$count = $woocommerce->cart->cart_contents_count;
 ?>
				<div class="add-to-cart-button  pull-right ">
		<?php if($count >  0) { ?>
				<a href="#checkOutModal" data-toggle="modal" >CHECKOUT</a>
		<?php } ?>
		<?php if($count <  1) { ?>
				<a href="#checkoutFailed" data-toggle="modal" >CHECKOUT</a>
		<?php } ?>

		</div>
			</div>
			<div class="col-md-3">
				<div class="add-to-cart-button pull-right"><a href="/my-orders">RECENT ORDERS</a></div>
			</div>

		</div>
		<div style="clear:both"><br>&nbsp;</div>
				<div class="filter-wrapper" style="padding-left:35px;padding-right:30px;padding-top:5px;">
				<div class="row">

					<p class="table-top-h1" style="padding-top:5px;"> Filters: &nbsp;&nbsp;</p>
				<form method="get" style="padding-bottom:10px;width: 812px" id="filter_form">
					<div class="col-md-3" style="padding : 2px;">
						<div class="form-group">
								<select class="form-control button_example" name="machine_type" id="machine_type">
									<?php
						$mct = $_GET['machine_type'];
			?>
									<?php if(!empty($mct)) {  ?>
										<option value="<?=$_GET['machine_type']?>"><label> <?=$_GET['machine-type']?></option>
									<?php }  ?>
									<?php if(empty($mct)) { ?>
									<option value="All"><label> Machine Type : All</label></option>
									<?php }  ?>

									<?php /*if($machine_type !=' ') { ?>
										<option vale="<?=$machine_type?>"><?=ucfirst($machine_type)?></option>
										<?php } else { ?>
											option></option>

										<?php }*/ ?>
									<?php foreach(   $machine_type as $machine_type) {?>
										<option value="<?=$machine_type->term_id?>"><?=$machine_type->name?></option>
									<?php } ?>

								</select>
							</div>
						</div>
					<div class="col-md-3" style="padding : 2px;">
							<div class="form-group">
								<?php $mm = $_GET['machine_model']; ?>
								<select class="form-control  button_example"  name="machine_model" id="machine_model">
									<?php if(!empty($mm) && $mm !="All") { ?>
									<option value="<?=$_GET['machine_model']; ?>
"><label><?=$_GET['machine-model']; ?>
</label></option>

									<?php } else { ?>
									<option value="All"><label> Machine Model : All</label></option>
									<?php } ?>

								</select>
								<small class="pull-right" id="machine_loader"><i class="fa fa-spin fa-spinner"></i></small>
							</div>
						 </div>


					<div class="col-md-3" style="padding : 2px;">
							<div class="form-group">
								<?php $mn = $_GET['model_number']; ?>

							 	<select class="form-control  button_example" name="model_number" id="model_number" >
									<?php if(!empty($mn)) { ?>
										<option value="<?=$mn?>"><label><?=$_GET['model-number'];?></label></option>

									<?php } else { ?>
										<option value="All"><label> Machine Number : All</label></option>
									<?php } ?>

							</select>
							<small class="pull-right" id="model_loader"><i class="fa fa-spin fa-spinner"></i></small>

							</div>
					</div>
					<div class="col-md-3" style="padding : 2px;">
							<input type="hidden" name="machine-type" id="mc-type"">
							<input type="hidden" name="machine-model" id="mc-model">
							<input type="hidden" name="model-number" id="mc-num">
							<input type="hidden" name="part-type" id="part-type">


							<div class="form-group">
									<select class="form-control  button_example" name="part_type" id="part_type">
											<?php $pcc = $_GET['part_type']; ?>
											<?php if(!empty($pcc) && $pcc !="All") { ?>
												<option value="<?=$pcc?>"><label> <?=$_GET['part-type']?></label></option>
											<?php } else { ?>
											  <option value="All"><label> Part Type : All</label></option>
											<?php } ?>

										<?php if($pc_type !=' ') { ?>
										<option vale="<?=$pc_type?>"><?=ucfirst($pc_type)?></option>
										<?php } else { ?>
											<option value="All">Part Type : All </option>

										<?php } ?>
									<?php foreach(   $part_type as $part_type) {?>
										<option value="<?=$part_type->term_id?>"><?=$part_type->name?></option>
									<?php } ?>

								</select>
							</div>

								<p class="pull-right" style="display:flex;align-items:center;margin:0px;padding-bottom:5px;"><input type="search" id="search" name="search" value="<?=$_GET['search']?>" style="width:200px;" class="form-control" placeholder="Search" aria-controls="myTable"><span><a href="/spare-parts" style="color:#000;padding-left:5px;" id="reload"><span ><i class="fa fa-refresh"></i></span></a></span></p>

						 </div>



						</form>
					</div>
						</div>



				<?php do_action( 'woocommerce_before_cart' ); ?>
				<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$params = array('posts_per_page' => 1000,'post_type' => 'product','paged' => $paged ,'orderby' => 'title',
 'order' => 'ASC', ); // (1)
			if(isset($_GET['model_number']) && isset($_GET['model_number']) !='') {
					$mc_number = $_GET['model_number'];
					if(!empty($mc_number) && $mc_number !="All"){

						$params['tax_query']['relation'] = 'IN';
							$params['tax_query'][] = 	array(
										'taxonomy' => 'product_cat',
										'terms'    => $mc_number,
										'field'      => 'term_id',
							);
					}

				}

			if(isset($_GET['machine_type']) && isset($_GET['machine_type']) !='') {
				$mc_type = $_GET['machine_type'];
				if(!empty($mc_type) && $mc_type  !="All"){
					$params['tax_query']['relation'] = 'IN';
				$params['tax_query'][] = 	array(
										'taxonomy' => 'product_cat',
										'terms'    => $mc_type,
										'field'      => 'term_id',
					);
				}

			}
			if(isset($_GET['machine_model']) && isset($_GET['machine_model']) !='') {

				$mc_model = $_GET['machine_model'];
					if(!empty($mc_model) && $mc_model !="All") {
						$params['tax_query']['relation'] = 'IN';
				$params['tax_query'][] = array(
								'taxonomy'   => 'product_cat',
								'terms'      => $mc_model,
								'field'      => 'term_id',
					);
					}
			}
			if(isset($_GET['part_type']) && isset($_GET['part_type']) !='') {
				$p_type = $_GET['part_type'];
					if(!empty($p_type) && $p_type !='All'){
						$params['tax_query']['relation'] = 'IN';
						$params['tax_query'][] = array(
								'taxonomy'   => 'product_cat',
								'terms'      => $p_type,
								'field'      => 'slug',
						);
					}
			}

			$wc_query = new WP_Query($params); // (2)
			?>
			<div id="product_container">
				<div class="row">
					<div class="col-md-12">
                        <div class="dashboard_content">
                    <div class="js-wrap table-wrapper cust-scrollbar" id="style-2">
                    <table class="table tablesorter customer-tbl" id="myTable">
                    <thead class="thead-inverse">
											<tr style="font-size:11px;">
								 				<th style="text-align:center;width:20% !important;">TDI PART #</th>
								  			<th style="width:35% !important;">DESCRIPTION</th>
								  			<th style="width:20% !important;" id="picture">PICTURE </th>
											<th tyle="width:25% !important;">PRICE </th>


								</tr>
							</thead>
							<tbody id="grid-body">

								<?php $count=1; ?>
								<?php if ($wc_query->have_posts()) : // (3) ?>
								<?php while ($wc_query->have_posts()) : // (4)
								                $wc_query->the_post(); // (4.1)
								                	global $product;
								                	$name = get_the_title();

										$new_price = $wpdb->get_row("SELECT * FROM wp_pricing_table WHERE machines = '$name'") ;

								               	$price = 0 ;

			  					 		/*if($price_level == 'b4' || $price_level=='B4 Price') {
			  					 			$price = $new_price->b4_price;
			  					 		}
			  					 		else */if($price_level=='otis' || $price_level =='Otis' || $price_level == 'Otis Price') {
			  					 			$price = $new_price->otis_price;
			  					 		}
			  					 		else {
			  					 		  $price = $new_price->general_price;
			  					 		}
                                        if ( $price == 0 ) continue;

			  						?>

								<tr class="row-vm shown" >
																	  	<td style="white-space: nowrap;overflow: hidden;"><p style="font-size:11px !important;margin-bottom: 20px; "><span><?php the_title(); // (4.2) ?></span> </p>

																					<div class="btn_wrapper">
																						<div><a href="<?=get_permalink( $product->get_id() );?>" style="font-size:10px;color:#fff;" class="btn btn-info text-white  btn-clickable" ><span style="background-color:#0d85a9;"><i class="fa fa-eye"> </i></span> VIEW DETAILS</a>
																						</div>
																									<?php
																										if($price > 0 ) { ?>


																											<div><a href="?add-to-cart=<?=$product->id?>" class="btn btn-success btn-clickable" data-product_id="<?php echo $product->id;?>" rel="nofollow"><span>$</span> ADD TO CART</a></div>
																											<?php }  ?>
																										 </div>
																						</td>
																					<td   class="csv-data" style="text-align:left;"><p  style="font-size:11px !important;"><?php echo $product->get_description(); ?></p>
                                                                                        <?php $terms = get_the_terms($wc_query->post->ID,'product_cat'); ?>
                                                                                        <?php if ( $terms ) { ?>
                                                                                            <div style="display:none;" class="pcat"><?php foreach ($terms as $term) { echo '['.$term->term_id.']'; } ?></div> 
                                                                                        <?php } ?>
																					<br>

																	</td>
																					<td  class="csv-data" >

																					<?php
																								if ( has_post_thumbnail( $wc_query->post->ID ) )  { ?>

																									<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $wc_query->post->ID ), 'single-post-thumbnail' );?>
																				                       		 <img  src="<?php  echo $image[0]; ?>"  alt="<?php the_title(); // (4.2) ?>" title="<?php the_title(); // (4.2) ?>">
																				                      					<?php } else { ?>
																								 <img  src="https://torindriveintl.com/wp-content/uploads/2019/11/no_image.jpg"  alt="<?php the_title(); // (4.2) ?>" title="<?php the_title(); // (4.2) ?>">

																							   <?php } ?>
																				</td>
															<td style="text-align:center;" > <p style="font-size:11px !important;">$<?=number_format($price,2)?></p>	<br><br></td>

								</tr>





					<?php endwhile; ?>



				<?php wp_reset_postdata(); // (5) ?>

								<?php else:  ?>
								<tr>
								     <td align="center" colspan="6"><?php _e( 'No Spare Part Found' ); // (6) ?></td>
								</tr>
					<?php endif; ?>
				</tbody>
							</table>
                        </div>
                                        </div>

					</div>

				</div>


			</div>


			</div>


		</div>


  </div>
</section>

<div class="modal fade js-purchase-wrap in" id="checkOutModal" role="dialog" style="padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">CHECKOUT - STEP <span class="js-step">1</span>/4</h4>
            </div>
            <div class="modal-body">
                <form class="form-search form-horizontal" role="form" action="#" method="POST" id="purchase-form" enctype="multipart/form-data">

                    <div class="container">
                        <div class="row">
                            <div class="purchase-step" data-step="1" >
                                <small class="purchase-note shown"> Fields with <span class="required-indicator">*</span> is required to be filled.</small>
                            </div>
                            <div class="purchase-step space-2 shown" id="step1">
                            <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> E-Signature:</div>
                                    <div class="col-md-9 esign-wrap">
                                        <input type="hidden" name="esign" class="js-purchase-esign required">
                                        <input type="hidden" name="esignature">
                                        <iframe src="/esign/" id="esign"></iframe>
                                        <a href="javascript:document.getElementById('esign').contentDocument.location.reload(true);" class="btn btn-danger btn-sm" id="clearSign">CLEAR</a>
                                    </div>
                                
                            </div>
                            <div class="purchase-step space-2 " data-step="4" id="step2">
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Delivery Date:</div>
                                    <div class="col-md-9"><input type="text" name="delivery_date" class="form-control required datepicker" autocomplete="ofF" required>
																				<span class="text-danger" id="dev"></span>
																</div>

                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Ship To:</div>
                                    <div class="col-md-9">
                                        <div class="row row-xs">
                                            <div class="col-md-8 mb4"><input type="text" name="shipping_address" placeholder="* Address" class="form-control required" maxlength="60"  required></div>
                                            <div class="col-md-4 mb4"><input type="text" name="shipping_city" placeholder="* City" class="form-control required"  required></div>
                                            <div class="col-md-4"><input type="text" name="shipping_state" placeholder="* State/Province" class="form-control required"  required></div>
                                            <div class="col-md-4"><input type="text" name="shipping_zip" placeholder="* Zip" class="form-control required"  required></div>
                                            <div class="col-md-4"><input type="text" name="shipping_country" placeholder="* Country" class="form-control required"  required></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Bill To :</div>
                                    <div class="col-md-9">
                                        <div class="row row-xs">
                                            <div class="col-md-8 mb4"><input type="text" name="billing_address" placeholder="* Address" class="form-control required" maxlength="60"  required></div>
                                            <div class="col-md-4 mb4"><input type="text" name="billing_city" placeholder="* City" class="form-control required"  required></div>
                                            <div class="col-md-4"><input type="text" name="billing_state" placeholder="* State/Province" class="form-control required"  required></div>
                                            <div class="col-md-4"><input type="text" name="billing_zip" placeholder="* Zip" class="form-control required"  required></div>
                                            <div class="col-md-4"><input type="text" name="billing_country" placeholder="* Country" class="form-control required"  required></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Shipping Type:</div>
                                    <div class="col-md-9">
                                        <select name="shipping_type" class="form-control required" required>
                                            <option >Customer Account</option>
                                            <option  >Customer Pickup</option>
                                            <option data-stype="tdi">TDI Prepay - 24Hr Lift Gate</option>
                                            <option data-stype="tdi">TDI Prepay - Regular</option>
                                            <option data-stype="tdi">TDI Pre - Express ON</option>
                                            <option data-stype="tdi">TDI Pre - Fed Ground</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Customer Carrier:</div>
                                    <div class="col-md-9"><input type="text" name="customer_carrier" class="form-control required js-sci"  ></div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Cus. Shipping Acct#:</div>
                                    <div class="col-md-9"><input type="text" name="customer_shipping_account" class="form-control required js-sci" ></div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Shipping Contact Name:</div>
                                    <div class="col-md-9"><input type="text" name="shipping_contact_name" class="form-control required js-xsci"  required></div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Shipping Contact Phone:</div>
                                    <div class="col-md-9"><input type="text" id="shipphone" name="shipping_contact_phone" class="form-control required js-xsci" maxlength="14"   required></div>
                                </div>
                                <div class="row input-wrap">
                                    <div class="col-md-3 plabel text-right"><span class="required-indicator">*</span> Shipping Contact Email:</div>
                                    <div class="col-md-9"><input type="text" name="shipping_contact_email" class="form-control required js-xsci"  required></div>
                                </div>
                            </div>
                            <div class="purchase-step" id="step3">
                                <div class="js-pt">
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right">Payment Type</div>
                                        <div class="col-md-8">
                                            <select name="payment_type" id="payment_type" class="form-control required js-payment_type">
                                                <option value="po" selected="">PO</option>
                                                <option value="creditcard">Credit Card</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="js-pt-po" >
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> PO Number:</div>
                                        <div class="col-md-8"><input type="text" name="po_number" class="form-control required js-po" ></div>
                                    </div>
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> PO File <small>(PDF only)</small>:</div>
                                        <div class="col-md-8"><input type="file" name="po_file" class="form-control required js-po" accept="application/pdf"></div>
                                    </div>
                                </div>
                                <div class="js-pt-cc" style="display: none;">
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> Card Type:</div>
                                        <div class="col-md-8">
                                            <select name="card_type" class="form-control required js-card_type js-cc">
                                                <option value="visa" selected="">Visa</option>
                                                <option value="american-express">American Express</option>
                                                <option value="master-card">Master Card</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> Card Number:</div>
                                        <div class="col-md-8">
                                            <input type="text" name="card_number" id="card_number" class="form-control required js-cc" maxlength="19">
                                            <input type="text" name="wc_card_number" class="hidden ccFormatMonitor wc-credit-card-form-card-number" maxlength="19" placeholder="4###-####-####-####">
                                        </div>
                                    </div>
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> CVV:</div>
                                        <div class="col-md-2">
                                            <input type="text" name="card_cvv" id="card_cvv" class="form-control required js-cc" maxlength="3">
                                            <input type="text" name="wc_card_cvv" class="hidden wc-credit-card-form-card-cvc" maxlength="3">
                                        </div>
                                    </div>
                                    <div class="row input-wrap">
                                        <div class="col-md-4 plabel text-right"><span class="required-indicator">*</span> Expiry Date:</div>
                                        <div class="col-md-8">
                                            <div class="row row-xs">
                                                <div class="col-md-6">
                                                    <select name="card_expiry_month" class="form-control required js-cc">
                                                                                                                    <option value="01">January</option>
                                                                                                                    <option value="02">February</option>
                                                                                                                    <option value="03">March</option>
                                                                                                                    <option value="04">April</option>
                                                                                                                    <option value="05">May</option>
                                                                                                                    <option value="06">June</option>
                                                                                                                    <option value="07">July</option>
                                                                                                                    <option value="08">August</option>
                                                                                                                    <option value="09">September</option>
                                                                                                                    <option value="10">October</option>
                                                                                                                    <option value="11">November</option>
                                                                                                                    <option value="12">December</option>
                                                                                                            </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <select name="card_expiry_year" class="form-control required js-cc">
                                                                                                                    <option>2020</option>
                                                                                                                    <option>2021</option>
                                                                                                                    <option>2022</option>
                                                                                                                    <option>2023</option>
                                                                                                                    <option>2024</option>
                                                                                                                    <option>2025</option>
                                                                                                                    <option>2026</option>
                                                                                                                    <option>2027</option>
                                                                                                                    <option>2028</option>
                                                                                                                    <option>2029</option>
                                                                                                                    <option>2030</option>
                                                                                                                    <option>2031</option>
                                                                                                                    <option>2032</option>
                                                                                                                    <option>2033</option>
                                                                                                                    <option>2034</option>
                                                                                                                    <option>2035</option>
                                                                                                                    <option>2036</option>
                                                                                                                    <option>2037</option>
                                                                                                                    <option>2038</option>
                                                                                                                    <option>2039</option>
                                                                                                            </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
																		<div class="row input-wrap">
																				<div class="col-md-4 plabel text-right"> <span class="required-indicator">*</span> Reference No. :</div>
																				<div class="col-md-8"><input type="text" name="customer_reference_no" id="referrence_no" class="form-control required" ></div>
																		</div>
                                    <div style="padding: 10px 10px 0px;">
                                        <small>Note: there is 3.4% fee if you choose Credit Card.</small>
                                    </div>
                                </div>
                            </div>
                            <div class="purchase-step"  id="step4">
                                    <div class="order-wrapper">
                                                <p class="order-title">YOUR ORDERS</p>
                                            <?php do_action( 'woocommerce_checkout_order_review' ); ?>
                                    </div>
                            </div>
                        </div>
                    <input type="hidden" name="step" value="1">

		 	 <input type="hidden" name="customer_id" value="<?=$user_id?>">
			  <input type="hidden" name="customer_name">
			 <input type="hidden" name="customer_code">
			 <input type="hidden" name="customer_phone">
			 <input type="hidden" name="customer_email">
				<input type="hidden" name="cc_fees" value="0" >

			<input type="hidden" name="manager_email" value="<?=the_field('manager_email', 'options')?>">

                    </div>
                </form>
                <div style="padding: 10px 10px 0px; 10px;">
                                      &nbsp;  <small class="error" style="color:red;"></small>
                                    </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" type="button" data-dismiss="modal" style="float:left;">Cancel</button>
                <button class="btn btn-default purchase-button js-purchase-prev" type="button">Back</button>
                <button class="btn btn-primary purchase-button js-purchase-step shown" type="button">Next</button>
                <button class="btn btn-primary purchase-button js-purchase-confirm" type="button">Confirm</button>

            </div>
        </div>
    </div>
</div>
<div id="purchaseSuccess" class="modal fade">
    <div class="modal-dialog modal-confirm modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box"><i class="fa fa-check"></i></div>
            </div>
            <div class="modal-body text-center">
                <h4>Great!</h4>
                <p>Your Order has been received.</p>
                <a class="btn btn-success"  href="/spare-parts/?clear-cart"><span>OK</a>
            </div>
        </div>
    </div>
</div>
<div id="checkoutFailed" class="modal fade">
    <div class="modal-dialog modal-confirm modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box text-danger"><i class="fa fa-exclamation-triangle"></i></div>
            </div>
            <div class="modal-body text-center">
                <h4>Oops!</h4>
                <p>Your cart is empty. Cannot proceed to checkout.</p>
                <a class="btn btn-danger"  href="/spare-parts/"><span>OK</a>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php echo site_url(); ?>/torin/public/js/jquery.mask.min.js?v=3"></script>
<script> var $j3 = jQuery.noConflict(); </script>
<?php get_footer(); ?>
<script type="text/javascript" src="/wp-content/themes/torindriveintl/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="https://github.com/lopezton/jquery-creditcard-formatter/blob/master/ccFormat.js"></script>
<script src="/torin/public/js/bootstrap-datepicker.min.js"></script>
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/plug-ins/1.10.20/sorting/alt-string.js"></script>
<script>

	$(function() {

		$('.woocommerce-Price-currencySymbol').remove();

		$('.woocommerce-Price-amount').append('<span class="woocommerce-Price-currencySymbol">   US Dollars</span>');
	});

    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var allCats = [];
            $('#filter_form select').each(function(){
                var _v = $(this).val();
                if ( _v != '' && _v != 'All' ) {
                    allCats.push('['+_v+']');
                }
            });
     
            if ( allCats.length == 0 ) return true;
            var exist = true;
            for( var x in allCats ){
                if ( !data[1].includes(allCats[x]) ) return false;
            }
            return true;
        }
    );


	 $(document).ready(function() {

		   $('.loader').hide();
		   $('#model_loader').hide();
		   $('#machine_loader').hide();
		   var mch_type = $('#machine_type').val() ;
		   var mch_type_text = $('#machine_type option:selected').text() ;
		   var mm =  $('#machine_model').val();
		   var mm_text =  $('#machine_model option:selected').text();
		   var mn =  $('#model_number').val();
		  var mn_text =  $('#model_number option:selected').text();
		 var pcc_text = $('#part_type option:selected').text();
		$('#part-type').val(pcc_text);


		console.log(mch_type_text);
			if( mch_type !="" && mch_type !="All") {
				 $('#mc-type').val(mch_type_text);
					$.ajax({
   					 type: "post",
    			 		dataType: "json",
    					 url : "<?php echo admin_url('admin-ajax.php'); ?>",
             		 		data : {action: "get_machine_model" , term_id :  $('#machine_type').val()},
    					success: function(response){

					 $('#machine_model option').remove();
					$('#machine_model').append($('<option>').text(mm_text).attr('value',mm));

					$.each(response, function(index, value) {
   					 $('#machine_model').append($('<option>').text(value['name']).attr('value', value['term_id']));
				});
					$('#machine_loader').hide();

    			      }
			});

			}

		if(mm !="") {

				$('#mc-model').val(mm_text);
				$('#mc-num').val(mn_text);
				var md_num = $('#mc-num').val();

			$.ajax({
   				type: "post",
    			 	dataType: "json",
    				url : "<?php echo admin_url('admin-ajax.php'); ?>",
             		 	data : {action: "get_model_numbers" , term_id :  $('#machine_model').val()},
    					success: function(response){
        					console.log(response);
					 $('#model_number option').remove();
					if(mn !="" && mn!="All") {
						$('#model_number').append($('<option>').text(md_num).attr('value',mn));

					}
					else {
						$('#model_number').append($('<option>').text('Machine Number : All').attr('value','All'));
					}




					$.each(response, function(index, value) {
   					 $('#model_number').append($('<option>').text(value['name']).attr('value', value['term_id']));
				});
				$('#model_loader').hide();

    			      }
			});


		}
	  $('#machine_type').on('change', function() {
				$('#machine_loader').show();

			   $('#mc-type').val($('#machine_type option:selected').text());
				$.ajax({
   					 type: "post",
    			 		dataType: "json",
    					 url : "<?php echo admin_url('admin-ajax.php'); ?>",
             		 		data : {action: "get_machine_model" , term_id :  $('#machine_type').val()},
    					success: function(response){

					 $('#machine_model option').remove();
					$('#machine_model').append($('<option>').text('Machine Model : All').attr('value','All'));

					$.each(response, function(index, value) {
   					 $('#machine_model').append($('<option>').text(value['name']).attr('value', value['term_id']));
						});
					$('#machine_loader').hide();
						// $('#filter_form').submit();
                        oTable.draw();
    			  }

			});

		});
	$('#machine_model').on('change', function() {
		$('#mc-model').val($('#machine_model option:selected').text());
			$('#model_loader').show();

              		$.ajax({
   				type: "post",
    			 	dataType: "json",
    				url : "<?php echo admin_url('admin-ajax.php'); ?>",
             		 	data : {action: "get_model_numbers" , term_id :  $('#machine_model').val()},
    					success: function(response){

					 $('#model_number option').remove();
					$('#model_number').append($('<option>').text('Machine Number : All').attr('value','All'));

					$.each(response, function(index, value) {
   					 $('#model_number').append($('<option>').text(value['name']).attr('value', value['term_id']));
				});
				$('#model_loader').hide();
						// $('#filter_form').submit();
                        oTable.draw();
    			      }
			});
            });
	   $('#model_number').on('change', function() {
									$('#mc-num').val($('#model_number option:selected').text());

              		//$('form').submit();
										// $('#filter_form').submit();
                                        oTable.draw();
            });
  		$('#part_type').on('change', function() {
			$('#part-type').val($('#part_type option:selected').text());

              		//$('form').submit();
						// $('#filter_form').submit();
                        oTable.draw();
            });
     $('form').submit(function(){
			$('#btn_search').attr('disable',true);
			$('#btn_search').html('<span><i class="fa fa-spin fa-spinner"></i></span>');

	});
	$('.btn-clickable').click(function(){
			$(this).attr('disabled',true);

			$(this).html('<i class="fa fa-spin fa-spinner"></i>');
	});
	$('#reload').click(function(){
            $('#filter_form select').val('All');
            $('#filter_form #search').val('');
            oTable.search('').draw();
            return false;
	// 		$(this).attr('disabled',true);

	// 		$(this).html('<i class="fa fa-spin fa-spinner"></i>');
	// 		window.location.href="https://torindriveintl.com/spare-parts/" ;
	});



        });
</script>

<!-- <script type="text/javascript">

    var step = 1 ;
    $('.js-step').html(step);
    $('.js-purchase-step').click(function(e) {
        var formData =  $('#purchase-form').serialize();
        $('.error').html('') ;

            $(this).html('<span><i class="fa fa-spin fa-spinner"></i></span>') ;
        if(step == 1) {
            $('[name=step]').val(step);
                                                            $('#step2').hide();
		                                            	    $('.js-purchase-prev').hide();
			                                                 $('#step1').show();
			                                                $('#step3').hide();
                                                             $('.js-purchase-confirm').hide();
                                                            $('.js-purchase-step').show();




            $.ajax({

	   						type: "post",

	    									    url : "https://torindriveintl.com/spareparts/api/check-data",
	             		 						data : formData,
	    										success: function(response){
	        									console.log(response);

                                                    if(response.success ==true) {
                                                        step =2 ;
                                                        $('[name=step]').val(step);
                                                        $('.js-step').html(step);
                                                        $('#step2').show();
                                                        $('.js-purchase-prev').show();
                                                        $('#step1').hide();
                                                        $('#step3').hide();
                                                        $('.js-purchase-confirm').hide();
                                                        $('.js-purchase-step').show();

                                                    }
                                                    else {
                                                        $('.error').html(response) ;
                                                    }

                                                    $('.js-purchase-step').html('<span>NEXT</span>') ;
												}
								});


        }
        else if(step == 2) {

            var formData =  new FormData( $('#purchase-form')[0]);
            $.ajax({
                type: "post",
                 url : "https://torindriveintl.com/spareparts/api/check-data",
                   data : formData,

                    enctype: 'multipart/form-data',
                    processData: false,
                    cache: false,
                     contentType: false,
                     success: function(response){

                             if(response.success ==true) {
                                                        step =3 ;
                                                        $('.js-step').html(step);
                                                        $('[name-step]').val(step);
			                                            $('#step2').hide();
                                                         $('.js-purchase-prev').show();
			                                            $('.js-purchase-step').hide();
			                                            $('#step1').hide();
			                                            $('#step3').show();
																									$('[name=cc_fees]').val(response.cc_fee);

																											var tt = $('[name=total_order]').val();
																											var fees = $('[name=cc_fees]').val();
																											var ff = tt * (fees /100);
																											var final_total = parseFloat(tt) + parseFloat(ff);
																												$('#new_total').html( '<span> ' + parseFloat(final_total).toFixed(2).toLocaleString()  + ' US Dollars</span>');
																											var html_fee = '<tr><th style="text-align:right;padding-right:10px;">CC Fee</th><td>'+parseFloat(ff).toFixed(2) +' US Dollars</td></tr>';
																											$('.cart-subtotal').after(html_fee);
                                                        $('.js-purchase-confirm').show();
                                                        $('.js-purchase-step').html('<span>NEXT</span>') ;
                                                    }
                                                    else {
                                                        $('.error').html(response) ;
                                                    }
                                                    $('.js-purchase-step').html('<span>NEXT</span>') ;
							}


					});

        }
        else if(step == 3 ){
            $('[name-step]').val(step);
			$('#step2').hide();
            $('.js-purchase-prev').show();
			$('.js-purchase-step').hide();
			$('#step1').hide();
			$('#step3').show();
            $('.js-purchase-confirm').show();

        }
        $('.js-step').html(step);
    }) ;


    $('.js-purchase-confirm').click(function(){

        var formDatas =  new FormData( $('#purchase-form')[0]);
        $(this).html('<span><i class="fa fa-spin fa-spinner"></i></span>') ;
        $.ajax({
                type: "post",
                url : "https://torindriveintl.com/spareparts/api/checkout",
                 data : formDatas,
                enctype: 'multipart/form-data',
                processData: false,
                cache: false,
                contentType: false,
                success: function(response){


                     if(response.success ==true) {


                        $('#purchaseSuccess').modal('show')
                        $('#checkOutModal').modal('hide')  ;



                        step =1  ;
                        $('[name-step]').val(step);
                     }
                     else {
                        $('.error').html(response) ;
                    }

                     $('.js-purchase-confirm').html('<span>CONFIRM</span>') ;
            },
            error: function(response){
                var parent = $(response.responseText);
                $('.error').html('Server Error, please report to your Project Manager.<br><small>'+$('.trace-message', parent).text()+'</small>');
                $('.js-purchase-confirm').html('<span>CONFIRM</span>') ;
            }
 });

    });
var fee= 0 ;
    $('.js-purchase-prev').click(function() {
            step -- ;
            $('[name=step]').val(step);

        if(step == 1) {
			$('#step2').hide();
			$('.js-purchase-prev').hide();
			$('#step1').show();
			$('#step3').hide();
            $('.js-purchase-confirm').hide();
            $('.js-purchase-step').show();

        }
        else if(step == 2) {
            $('[name-step]').val(step);
			$('#step2').show();
			$('.js-purchase-prev').show();
			$('#step1').hide();
			$('#step3').hide();
            $('.js-purchase-confirm').hide();
            $('.js-purchase-step').show();
        }
        else if(step == 3 ){
            $('[name-step]').val(step);
			$('#step2').hide();
			$('.js-purchase-prev').show();
			$('#step1').hide();
			$('#step3').show();
            $('.js-purchase-confirm').show();
            $('.js-purchase-step').hide();

        }
        $('.js-step').html(step);

    }) ;
    $('#payment_type').on('change',function () {
        var pay_type = $(this).val();
            if(pay_type == 'po') {
                $('.js-pt-cc').hide();
                $('.js-pt-po').show();
            }
            else if(pay_type =='creditcard') {
									fee = 3.4;
                $('.js-pt-cc').show();
                $('.js-pt-po').hide();
            }
    });

	$(function() {

										var dateToday = new Date();


                        var carrier = "" ;
                        var account_no = "" ;
					$('.add-to-cart-button').click(function() {

									$.ajax({
	   										type: "post",
	    			 							dataType: "json",
	    										url : "<?php echo admin_url('admin-ajax.php'); ?>",
	             		 						data : {action: "get_all_user_meta" },
	    										success: function(response){
	        									console.log(response);

	        										$('[name=billing_address]').val(response.billing_address);
	        										$('[name=billing_city]').val(response.billing_city);
	        										$('[name=billing_state]').val(response.billing_state[0] ? response.billing_state : 'Tennessee');
	        										$('[name=billing_country]').val(response.billing_country[0] ? response.billing_country : 'USA');
	        										$('[name=billing_zip]').val(response.billing_zip[0] ? response.billing_zip : response.profile_zip);

	        										$('[name=shipping_address]').val(response.shipping_address);
	        										$('[name=shipping_city]').val(response.shipping_city);
	        										$('[name=shipping_state]').val(response.shipping_state[0] ? response.shipping_state : 'Tennessee');
	        										$('[name=shipping_country]').val(response.shipping_country[0] ? response.shipping_country : 'USA');
	        										$('[name=shipping_zip]').val(response.shipping_zip[0] ? response.shipping_zip : response.profile_zip);

                                                    $('[name=shipping_contact_name]').val(response.shipping_contact_name);
                                                    $('[name=shipping_contact_phone]').val(response.shipping_contact_phone);
                                                    $('[name=shipping_contact_email]').val(response.shipping_contact_email);
                                                    formatPhone('shipphone');
                                                    account_no = response.customer_shipping_account;
                                                    carrier = response.customer_carrier;
                                                    $('[name=customer_carrier]').val(carrier);
                                                    $('[name=customer_shipping_account]').val(account_no);

						 $('[name=customer_name]').val(response.first_name + ' ' + response.last_name);
						 $('[name=customer_code]').val(response.customer_code);
						 $('[name=customer_phone]').val(response.contact_phone_number);
						 $('[name=customer_email]').val(response.user_email);
                                                                    if(response.cc_only == 1) {


																																						$('#payment_type').val('creditcard');
																																						$('#payment_type option:selected').val('creditcard');
																																						$('#payment_type').attr('readonly',true);
																																						$('#payment_type').find('[value="po"]').remove();
                                                                            $('.js-pt-cc').show();
                                                                            $('.js-pt-po').hide();
                                                                            $('[data-stype="tdi"]').remove();
                                                                    }

												}
											});

					});

                    $('[name=shipping_type]').on('change', function(){
                            var ship_type  = $(this).val();
                            console.log(ship_type);
                            if(ship_type !='Customer Account') {
                                $('[name=customer_carrier]').attr('disabled',true);
                                $('[name=customer_shipping_account]').attr('disabled',true);
                                $('[name=customer_carrier]').val('');
                                $('[name=customer_shipping_account]').val('');
                            }
                            else {
                                $('[name=customer_carrier]').attr('disabled',false);
                                $('[name=customer_shipping_account]').attr('disabled',false);
                                $('[name=customer_carrier]').val(carrier);
                                $('[name=customer_shipping_account]').val(account_no);
                            }
                    });
	});
</script> -->



<script type="text/javascript">

    var step = 1 ;
    $('.js-step').html(step);
    $('.js-purchase-step').click(function(e) {
       
        $('.error').html('') ;



            $(this).html('<span><i class="fa fa-spin fa-spinner"></i></span>') ;

            var esign = $("#esign").contents().find(".pad")[0].toDataURL();
            console.log($('.js-purchase-esign').val(esign));
                   
            var empty_sign =  $("#esign").contents().find('input[name="output-3"]').val() ;
            $('[name=esignature]').val(empty_sign);
            console.log(empty_sign + 'ok');

            if(empty_sign == '' || empty_sign == ' ') {
                console.log('agay');
                $('[name=esignature]').val('empty');
            }


            var formData =  $('#purchase-form').serialize();
        if(step == 1) {

            $('#step1').show();
		                                            	    $('.js-purchase-prev').hide();
			                                                 $('#step2').hide();
			                                                $('#step3').hide();
                                                            $('#step4').hide();
                                                             $('.js-purchase-confirm').hide();
                                                            $('.js-purchase-step').show();
                   

                    $.ajax({ type: "post",

                 url : "https://torindriveintl.com/spareparts/api/check-data",
                   data : formData,
                 success: function(response){
                 console.log(response);

                     if(response.success ==true) {
                         step =2 ;
                         $('[name=step]').val(step);
                         $('.js-step').html(step);
                         $('#step2').show();
                         $('.js-purchase-prev').show();
                         $('#step1').hide();
                         $('#step2').show();
                         $('#step3').hide();
                         $('#step4').hide();
                         $('.js-purchase-confirm').hide();
                         $('.js-purchase-step').show();

                     }
                     else {
                         $('.error').html(response) ;
                     }

                     $('.js-purchase-step').html('<span>NEXT</span>') ;
                 }
 });

        }
        else if (step == 2) {
            $('[name=step]').val(step);
                                                            $('#step1').hide();
		                                            	    $('.js-purchase-prev').hide();
			                                                 $('#step2').show();
			                                                $('#step3').hide();
                                                            $('#step4').hide();
                                                             $('.js-purchase-confirm').hide();
                                                            $('.js-purchase-step').show();




            $.ajax({

	   						type: "post",

	    									    url : "https://torindriveintl.com/spareparts/api/check-data",
	             		 						data : formData,
	    										success: function(response){
	        									console.log(response);

                                                    if(response.success ==true) {
                                                        step =3 ;
                                                        $('[name=step]').val(step);
                                                        $('.js-step').html(step);
                                                        $('#step2').show();
                                                        $('.js-purchase-prev').show();
                                                        $('#step1').hide();
                                                        $('#step3').show();
                                                        $('#step4').hide();
                                                        $('#step2').hide();
                                                        $('.js-purchase-confirm').hide();
                                                        $('.js-purchase-step').show();

                                                    }
                                                    else {
                                                        $('.error').html(response) ;
                                                    }

                                                    $('.js-purchase-step').html('<span>NEXT</span>') ;
												}
								});


        }
        else if(step == 3) {

            var formData =  new FormData( $('#purchase-form')[0]);
            $.ajax({
                type: "post",
                 url : "https://torindriveintl.com/spareparts/api/check-data",
                   data : formData,

                    enctype: 'multipart/form-data',
                    processData: false,
                    cache: false,
                     contentType: false,
                     success: function(response){

                             if(response.success ==true) {
                                                        step =4 ;
                                                        $('.js-step').html(step);
                                                        $('[name-step]').val(step);
			                                            $('#step2').hide();
                                                         $('.js-purchase-prev').show();
			                                            $('.js-purchase-step').hide();
			                                            $('#step1').hide();
                                                        $('#step3').hide();
			                                            $('#step4').show();
																									$('[name=cc_fees]').val(response.cc_fee);

																											var tt = $('[name=total_order]').val();
																											var fees = $('[name=cc_fees]').val();
																											var ff = tt * (fees /100);
																											var final_total = parseFloat(tt) + parseFloat(ff);
																												$('#new_total').html( '<span> ' + parseFloat(final_total).toFixed(2).toLocaleString()  + ' US Dollars</span>');
																											var html_fee = '<tr><th style="text-align:right;padding-right:10px;">CC Fee</th><td>'+parseFloat(ff).toFixed(2) +' US Dollars</td></tr>';
																											$('.cart-subtotal').after(html_fee);
                                                        $('.js-purchase-confirm').show();
                                                        $('.js-purchase-step').html('<span>NEXT</span>') ;
                                                    }
                                                    else {
                                                        $('.error').html(response) ;
                                                    }
                                                    $('.js-purchase-step').html('<span>NEXT</span>') ;
							}


					});

        }
        else if(step == 4 ){
            $('[name-step]').val(step);
			$('#step2').hide();
            $('.js-purchase-prev').show();
			$('.js-purchase-step').hide();
			$('#step1').hide();
            $('#step3').hide();
			$('#step4').show();
            $('.js-purchase-confirm').show();

        }
        $('.js-step').html(step);
    }) ;


    $('.js-purchase-confirm').click(function(){

        var formDatas =  new FormData( $('#purchase-form')[0]);
        $(this).html('<span><i class="fa fa-spin fa-spinner"></i></span>') ;
        $('.error').html('');
        $.ajax({
                type: "post",
                url : "https://torindriveintl.com/spareparts/api/checkout",
                 data : formDatas,
                enctype: 'multipart/form-data',
                processData: false,
                cache: false,
                contentType: false,
                success: function(response){


                     if(response.success ==true) {


                        $('#purchaseSuccess').modal('show')
                        $('#checkOutModal').modal('hide')  ;



                        step =1  ;
                        $('[name-step]').val(step);
                     }
                     else {
                        $('.error').html(response) ;
                    }

                     $('.js-purchase-confirm').html('<span>CONFIRM</span>') ;
            },
            error: function(response){
                var parent = $(response.responseText);
                $('.error').html('Server Error, please report to your Project Manager.<br><small>'+$('.trace-message', parent).text()+'</small>');
                $('.js-purchase-confirm').html('<span>CONFIRM</span>') ;
            }
 });

    });
var fee= 0 ;
    $('.js-purchase-prev').click(function() {
            step -- ;
            $('[name=step]').val(step);

        if(step == 1) {

            
			$('#step2').hide();
			$('.js-purchase-prev').hide();
			$('#step1').show();
			$('#step3').hide();
            $('#step4').hide();
            $('.js-purchase-confirm').hide();
            $('.js-purchase-step').show();

        }
        else if(step == 2) {
            $('[name-step]').val(step);
			$('#step2').show();
			$('.js-purchase-prev').show();
			$('#step1').hide();
			$('#step3').hide();
            $('#step4').hide();
            $('.js-purchase-confirm').hide();
            $('.js-purchase-step').show();
        }
        else if(step == 3 ){
            $('[name-step]').val(step);
			$('#step2').hide();
			$('.js-purchase-prev').show();
			$('#step1').hide();
			$('#step3').show();
             $('#step4').hide();
            $('.js-purchase-confirm').show();
            $('.js-purchase-step').hide();


        }
        $('.js-step').html(step);

    }) ;
    $('#payment_type').on('change',function () {
        var pay_type = $(this).val();
            if(pay_type == 'po') {
                $('.js-pt-cc').hide();
                $('.js-pt-po').show();
            }
            else if(pay_type =='creditcard') {
									fee = 3.4;
                $('.js-pt-cc').show();
                $('.js-pt-po').hide();
            }
    });

	$(function() {

										var dateToday = new Date();


                        var carrier = "" ;
                        var account_no = "" ;
					$('.add-to-cart-button').click(function() {

                        console.log('atay');

									$.ajax({
	   										type: "post",
	    			 							dataType: "json",
	    										url : "<?php echo admin_url('admin-ajax.php'); ?>",
	             		 						data : {action: "get_all_user_meta" },
	    										success: function(response){
	        									console.log(response);

                                                    $('[name=billing_address]').val(response.billing_address);
                                                    $('[name=billing_city]').val(response.billing_city);
                                                    $('[name=billing_state]').val(response.billing_state);
                                                    $('[name=billing_country]').val(response.billing_country);

                                                    if (typeof(response.billing_zip) !== 'undefined') {
                                                        $('[name=billing_zip]').val(response.billing_zip[0] ? response.billing_zip : response.profile_zip);
                                                    }
                                                  
                                                    $('[name=billing_zip]').val(response.billing_postcode);
                                                    $('[name=shipping_address]').val(response.shipping_address);
                                                    $('[name=shipping_city]').val(response.shipping_city);
                                                    $('[name=shipping_state]').val(response.shipping_state);
                                                    $('[name=shipping_country]').val(response.shipping_country);
                                                    if (typeof(response.shipping_zip) !== 'undefined') {
                                                         $('[name=shipping_zip]').val(response.shipping_zip[0] ? response.shipping_zip : response.profile_zip);
                                                    }
                                                    $('[name=shipping_contact_name]').val(response.shipping_contact_name);
                                                    $('[name=shipping_contact_phone]').val(response.shipping_contact_phone);
                                                    $('[name=shipping_contact_email]').val(response.shipping_contact_email);
                                                    formatPhone('shipphone');
                                                    account_no = response.customer_shipping_account;
                                                    carrier = response.customer_carrier;
                                                    $('[name=customer_carrier]').val(carrier);
                                                    $('[name=customer_shipping_account]').val(account_no);

						 $('[name=customer_name]').val(response.first_name + ' ' + response.last_name);
						 $('[name=customer_code]').val(response.customer_code);
						 $('[name=customer_phone]').val(response.contact_phone_number);
                         $('[name=customer_email]').val(response.user_email);
                         
                         console.log(response.cc_only +  'atay ba uy');
                                                                    if(response.cc_only == 1) {


																		
                                                                                                $('#payment_type').val('creditcard');
																								$('#payment_type option:selected').val('creditcard');
																								$('#payment_type').attr('readonly',true);
                                                                                                $('#payment_type').find('[value="po"]').remove();
                                                                            					$('#payment_type').find('[value="po"]').remove();
																								$('.js-pt-cc').show();
																								$('.js-pt-po').hide();
                                                                        $('[data-stype="tdi"]').remove();
                                                                    }

												}
											});

					});

                    $('[name=shipping_type]').on('change', function(){
                            var ship_type  = $(this).val();
                            console.log(ship_type);
                            if(ship_type !='Customer Account') {
                                $('[name=customer_carrier]').attr('disabled',true);
                                $('[name=customer_shipping_account]').attr('disabled',true);
                                $('[name=customer_carrier]').val('');
                                $('[name=customer_shipping_account]').val('');
                            }
                            else {
                                $('[name=customer_carrier]').attr('disabled',false);
                                $('[name=customer_shipping_account]').attr('disabled',false);
                                $('[name=customer_carrier]').val(carrier);
                                $('[name=customer_shipping_account]').val(account_no);
                            }
                            $('[name="shipping_type_code"]').val($('option:selected',this).data('code'));
                    }).change();
	});
</script>
<script>
	$(document).on('keypress',function(e) {
	    if(e.which == 13) {
	        $('form').submit();
	    }
	});
 var input_search = $('[name=search]').val();
	$(document).ready( function () {
	    $('#myTable').DataTable();

			oTable = $('#myTable').DataTable();
			if(input_search !="") {
					   oTable.search($('[name=search]').val()).draw() ;
			}
			$('#search').keyup(function(){
			      oTable.search($(this).val()).draw() ;
			});

	} );

	$(function() {
	$('[name=delivery_date]').on('change',function() {
			var fullDate = new Date();
			var twoDigitMonth = ((fullDate.getMonth().length+1) === 1) ? (fullDate.getMonth()+1) : '' + (fullDate.getMonth()+1);

			var present_date =  new Date(twoDigitMonth + "/" + fullDate.getDate() + "/" + fullDate.getFullYear());
			var nextDate = new Date($(this).val()) ;

				var diff =  nextDate.getTime() - present_date.getTime();
				var Difference_In_Days = diff / (1000 * 3600 * 24);
				if(Difference_In_Days < 7) {
								$('#dev').html('<label style="padding-top:10px;">Requested lead times shorter than one week may be delayed. Please contact TDI for more information.</label>') ;
				}
				else {
						$('#dev').html('') ;
				}

	});



	var current_total = 	$('[name=total_order]').val();
			$('.woocommerce-remove-coupon').click(function() {
					console.log('ok');
						var coupon_total = $('[name=coupon_amount]').val();
						var total_order = $('[name=total_order]').val();
						 var new_total =   $('[name=sub_total_order]').val();
						$('[name=total_order]').val(new_total);
						current_total =  	$('[name=total_order]').val();
							$('[name=coupon_amount]').val(0);
						$('.cart-discount').remove();
						<!-- $('#new_total').html( '<span>$ ' + parseFloat(current_total).toFixed(2) + '</span>'); -->

						var tt = $('[name=total_order]').val();
						var fees = $('[name=cc_fees]').val();
						var ff = tt * (fees /100);
						var final_total = parseFloat(current_total) + parseFloat(ff);
							$('#new_total').html( '<span>$ ' + parseFloat(final_total).toFixed().toLocaleString()  +  '</span>');
								var html_fee = '<tr><th style="text-align:right;padding-right:10px;">CC Fee</th><td>'+parseFloat(ff).toFixed(2) +' USD Dollars</td></tr>';
								$('.cart-subtotal').after(html_fee);

			});
					<!-- $('#new_total').html( '<span>$ ' + parseFloat(current_total).toFixed(2) + '</span>'); -->




});

$(function() {

	$.ajax({
		type: "post",
dataType: "json",
url : "<?php echo admin_url('admin-ajax.php'); ?>",
	data : {action: "check_backorder" },
success: function(response){
		console.log(response);
		if(response.back_order == true) {
			 var tt = new Date();
			var date = new Date(tt);
 			var newdate = new Date(date);

 			newdate.setDate(newdate.getDate() + 83);

 			var dd = newdate.getDate();
 			var mm = newdate.getMonth() + 1;
 		var y = newdate.getFullYear();

 				var someFormattedDate = mm + '/' + dd + '/' + y;
					console.log(someFormattedDate );
					var now = new Date();
			now.setDate(now.getDate()+84);

			 $('.datepicker').datepicker({ startDate: now });
                // $('#dev').html('<label style="padding-top:10px;">Some or all of your order is on backorder. The soonest available lead time is 12 weeks. Please contact the parts manager for more details.</label>')
				$('#dev').html('<label style="padding-top:10px;">Purchased quantity exceeds stock availability, please allow 12 weeks for delivery.</label>')
   }
		if(response.back_order == false) {
			 $('.datepicker').datepicker({ startDate: "today" });
}


	}
});
});


</script>

<script>
    var purchaseForm = $j3('#purchase-form'),
        masks = {
            'visa' : {
                limit: 19,
                cvv: 3,
                format: '4###-####-####-####'
            },
            'master-card' : {
                limit: 19,
                cvv: 3,
                format: '5###-####-####-####'
            },
            'american-express' : {
                limit: 17,
                cvv: 4,
                format: '3###-######-#####'
            },
            'other' : {
                limit: 19,
                cvv: '3',
                format: '####-####-####-####'
            }
        };
    function formatPhone(id){
        var phone_regex_sc10 = /\d{10}/g,
            phone_regex_sc7 = /\d{7}/g,
            cphone = $('#'+id).val();

        if ( cphone.length!=0 && !cphone.match(/\([\d]{3}\) [\d]{3}\-[\d]{4}/g) ) {
            cphone = cphone.replace(/\D/g,'');
        }
        if ( cphone.match(phone_regex_sc10) ) {
            $('#'+id).val('('+cphone.substring(0,3)+') '+cphone.substring(2,5)+'-'+cphone.substring(5,9));
        } else if ( cphone.length == 7 ) {
            $('#'+id).val('(901) '+cphone.substr(0,3)+'-'+cphone.substr(3,4));
        } else {
            // $('#'+id).val('');
        }
    }
    $j3(document).ready(function () {
        $j3('.js-card_type',purchaseForm).change(function(){
            var val = $j3(this).val();
            $j3('#card_number',purchaseForm)
                .mask(masks[val].format)
                .attr('maxlength',masks[val].limit)
                .attr('placeholder',masks[val].format)
                .val($j3('#card_number').val().substr(0,masks[val].limit))
                // .val('')
                .focus();
            $j3('#card_cvv',purchaseForm)
                .mask('####')
                .attr('maxlength',masks[val].cvv)
                .val($j3('#card_cvv').val().substr(0,masks[val].cvv));
                // .val('');
        }).change();
    });
    $(document).ready(function () {
        $('#shipphone')
        .keydown(function (e) {
            var key = e.which || e.charCode || e.keyCode || 0,
                charstr = '';
            $phone = $(this);

            // Don't let them remove the starting '('
            if ($phone.val().length === 1 && (key === 8 || key === 46)) {
                $phone.val('(');
                return false;
            }
            // Reset if they highlight and type over first char.
            else if ($phone.val().charAt(0) !== '(') {
                charstr = String.fromCharCode(e.keyCode);
                $phone.val('('+charstr.replace(/\D/g,'').charAt(0)+''); 
                if ( !isNaN(charstr) ) {
                    return false;
                }
            }

            // Auto-format- do not expose the mask as the user begins to type
            if (key !== 8 && key !== 9) {
                if ($phone.val().length === 4) {
                    $phone.val($phone.val() + ')');
                }
                if ($phone.val().length === 5) {
                    $phone.val($phone.val() + ' ');
                }           
                if ($phone.val().length === 9) {
                    $phone.val($phone.val() + '-');
                }
            }

            // Allow numeric (and tab, backspace, delete) keys only
            return (key == 8 || 
                    key == 9 ||
                    key == 46 ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105)); 
        })
        .keyup(function (e) {
            var key = e.which || e.charCode || e.keyCode || 0,
                charstr = '';
            $phone = $(this);
            
            if ($phone.val().charAt(0) !== '(') {
                $phone.val('('+$phone.val()); 
            }

            if ($phone.val().charAt(2) === '(') {
                $phone.val($phone.val().substring(0,($phone.val().length-1)));
            }
        })
        .bind('focus click', function () {
            $phone = $(this);
            
            /*if ($phone.val().length === 0) {
                $phone.val('(');
            }
            else {
                var val = $phone.val();
                $phone.val('').val(val); // Ensure cursor remains at the end
            }*/

            // var $phone = $('#fieldName');
            var fldLength= $phone.val().length;
            if ( fldLength ) {
                $phone[0].setSelectionRange(fldLength, fldLength);
            } else {
                $phone.val('(');
                $phone[0].setSelectionRange(1, 1);
            }

        })
        .blur(function () {
            $phone = $(this);
            
            if ($phone.val() === '(') {
                $phone.val('');
            } else {
                formatPhone('shipphone');
            }
        });
    });
</script>