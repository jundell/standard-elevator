<?php 
// Template Name: Elevator Component
get_header(); 
?>

<section class="header-image-new">

  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">
    <div class="row">
      <!-- <div class="col-sm-12 col-md-12"> -->
      <div class="col-xs-12">
        <div class="elevator-component">
        <?php the_content(); ?>
        <div class="clear"></div>
        <div class="menu-grid-wrapper">
          <?php if( have_rows('menu_group') ): ?>
          <div class="component__list component__list--elevator">
            <?php while( have_rows('menu_group') ): the_row(); 
              $image = get_sub_field('image');
              $title = get_sub_field('title');
              $link = get_sub_field('link');
              $tab = get_sub_field('tab');
            ?>
            <div class="component__item-list">
                <div class="component__item <?php echo empty($image) ? 'no-image' : ''; ?>">
                <?php if( $link ): ?>
                  <a target="<?php if($tab == 'true'){echo '_blank';} ?>" href="<?php echo $link; ?>">
                <?php endif; ?>
                  
                    <div class="component__item-image <?php echo empty($image) ? 'no-image' : ''; ?>"  data-mh="component__item-image">
                      <?php if(!empty($image)) { ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>">
                      <?php } else { ?>
                        <img src="<?php echo get_template_directory_uri();?>/images/default-no-image.jpg" alt="Standard Elevator">

                      <?php } ?>
                    </div>
                    <div class="component__item-header" data-mh="component__item-header">
                      <h4 class="component__item-title"><?php echo $title; ?></h4>
                    </div>
                  <?php if( $link ): ?>
                    </a>
                  <?php endif; ?>
                </div>
            </div>
            <?php endwhile; ?>
          <?php endif; ?>
          </div>
        </div>
        
        <?php if( get_field('below_content') ): ?>
          <?php echo the_field('below_content') ?>
        <?php endif; ?>

      </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>