<?php 
get_header(); 

$location_heading = get_field('location_heading');
$location_repeater = get_field('location_item');
$location_bg = get_field('location_background');

?>
<div class="main-content" id="location-select">
  <section id="landing-page-header">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" class="img-responsive" alt="Standard Elevator Systems">
  </section>

  <section class="section__map" style="background-image:url('<?php echo $location_bg['url']; ?>');">
	<div class="section__map-inner">
		<div class="container">
			<div class="section__map-header">
				<h1><?php echo $location_heading; ?></h1>
			</div>
			<div class="map__list">
				<?php 
					foreach($location_repeater as $location) { 
						$image = $location['location_map'];
						$name = $location['location_name'];
						$link = $location['location_link'];
						$target = $location['target'];
						$element_class = '';
						$link_attr = '';
						$link_class = '';

						if(!empty($link)) {
							$element_class = 'a';
							$link_attr = 'href="'.$link.'" target="'.$target.'"';
						} else {
							$element_class = 'div';
							$link_class = 'no-link';
						}
					?>
					<div class="map__list-item" data-mh="map__list-item">
						<<?php echo $element_class; ?> <?php echo $link_attr; ?> class="map__list-body <?php echo $link_class; ?>">
							<div class="map__list-image">
								<img src="<?php echo $image['sizes']['map-thumb']; ?>" alt="<?php echo $image['title']; ?>">
							</div>
							<div class="map__list-title">
								<?php echo $name; ?>
							</div>
						</<?php echo $element_class; ?>>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
  </section>

  <section class="footer__main">
	 	<div class="container">
			 <div class="row footer__main-row">
				 <div class="col-xs-12 col-md-6">
					<div class="footer__copyright">
						<?php dynamic_sidebar( 'Footer Copyright' ); ?>
					</div>
				 </div>
				 <div class="col-xs-12 col-md-6">
					 <div class="footer__logos">
						 <div class="footer__logos-title">Our Brands</div>
						 <ul>
							 <li>
								 <a href="">
								 	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tdi.png" alt="Torin Drive International">
								 </a>
							 </li>
							 <li>
								 <a href="">
								 	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/fieldx.png" alt="FieldX">
								 </a>
							 </li>
						 </ul>
					 </div>
				 </div>
			 </div>
		</div>
  </section>

</div>

<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.matchHeight.js"></script>
<?php //get_footer(); ?>