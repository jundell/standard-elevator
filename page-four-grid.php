<?php 

// Template Name: Page Four Grid

get_header(); 

?>
<section class="header-image-new">
  <div class="wOuter">
    <div class="wInner">
      <h1><?php the_title(); ?></h1>
    </div>
  </div>
</section>

<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        
        <div class="menu-grid-wrapper">
          <?php if( have_rows('menu_group') ): ?>
          <div class="row">
            <?php while( have_rows('menu_group') ): the_row(); 
              // vars
              $image = get_sub_field('image');
              $title = get_sub_field('title');
              $link = get_sub_field('link');
            ?>
            <div class="col-sm-6 col-md-3 item">

              <?php if( $link ): ?>
                <a href="<?php echo $link; ?>">
              <?php endif; ?>

                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>">
                  <h4><?php echo $title; ?></h4>

              <?php if( $link ): ?>
                </a>
              <?php endif; ?>

            </div>
            <?php endwhile; ?>
          <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>