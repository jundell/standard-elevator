<?php
// Template Name: Videos Page

get_header(); 
$video_cats = custom_repeater_data('video_category',array('category_name','video_gallery_shortcode'));

$content_tab_repeater = get_field('video_category');
	$num_tab_repeater = count($content_tab_repeater);
	$tab_repeater_count = '';

	if($num_tab_repeater >= 5) {
		$tab_repeater_count = 'multiple';
	} else {
		$tab_repeater_count = 'single';
	}

/*if( !empty($image) ):*/ ?>

<section class="header-image-new">



  <div class="wOuter">

    <div class="wInner">

      <h1><?php the_title(); ?></h1>

    </div>

  </div>

</section>



<section class="content">

  <div class="container">
  
	<?php //if( is_user_logged_in() ) : ?>
	
    <!-- <div class="bg-side"></div> -->

    <div class="row">

      <div class="col-md-3 sidebar_wrap welcome-left page--videos">       
        <?php 
			get_sidebar('menu'); 
		?> 
      </div>

      <div class="col-md-9 content_wrap" >
        <div class="the_content">
          <?php the_content(); ?>
        </div>

        <?php 
       
        if(is_array($content_tab_repeater)) { ?>

        <div class="section__tab" id="video_wrap">
          <ul class="nav nav-tabs <?php echo $tab_repeater_count; ?>">
            <?php $i_ctr = 0; foreach ($video_cats as $video_cat) {
                if($i_ctr == 0){
                  $active = 'active';
                }else{
                  $active = '';
                }
              ?>
              <li class="<?php echo $active; ?>"><a href="#vidcat<?php echo $i_ctr++; ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $video_cat['category_name'] ?></a></li>
            <?php $i_ctr++;  }  ?>
          </ul>
          <div id="video_cats">
            <div id="video_cat">
                <div class="tab-content" data-mh="tab-content">
                    <?php $i_ctr = 0; foreach ($video_cats as $video_cat) { 
                      if($i_ctr == 0){
                        $conactive = 'in active';
                        }else{
                        $conactive = '';
                        }
                    ?>
                    <div role="tabpanel" class="tab-pane fade <?php echo $conactive; ?>" id="vidcat<?php echo $i_ctr++; ?>" class="video_cat">
                      <h3><?php echo $video_cat['category_name'] ?></h3>
                      <div class="video_wrap">
                      <?php echo do_shortcode($video_cat['video_gallery_shortcode']); ?>
                      </div>
                    </div>
                  <?php $i_ctr++; } ?>
                </div>
              </div>
          </div>
        </div>
        <?php } ?>

        <div class="back-to-top"><a class="js-back-to-top">Back to Top</a></div>
      </div>

    </div>
	
	<?php //else : ?>
	
		<?php //get_template_part('restricted-error'); ?>
		
	<?php //endif; ?>
	
  </div>

</section>

<?php get_footer(); ?>