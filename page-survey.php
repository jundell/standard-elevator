<?php 
// Template Name: Survey Template
get_header(); 

global $TorinRegistration;

/*if( !empty($image) ):*/ ?>

<section class="header-image-new">



  <div class="wOuter">

    <div class="wInner">

      <h1><?php the_title(); ?></h1>

    </div>

  </div>

</section>

<section class="content">

  <div class="container">
	
	<?php if( is_user_logged_in() ) : ?>
    
		<div class="bg-side"></div>

		<div class="row">

		  <div class="col-md-3 sidebar_wrap welcome-left">			
				<?php 
					get_sidebar('menu'); 
				?>
		  </div>

		  <div class="col-sm-9 content_wrap" >
			
			<?php if( get_the_content() != '' ) : ?>
				<div class="the_content">
				  <?php the_content(); ?>
				</div>
			<?php endif; ?>
			
			<?php
				// Display Survey Form
				echo $TorinRegistration->MCR_display_survey_form();				
			?>

		  </div>

		</div>
		
		<?php else : ?>		
			
		<?php get_template_part('restricted-error'); ?>
		
		<?php endif; ?>
		
    </div>	

</section>

<?php get_footer(); ?>