<?php 

// Template Name: Export Users

function array2csv(array &$array)
{
   if (count($array) == 0) {
     return null;
   }
   ob_start();
   $df = fopen("php://output", 'w');
   fputcsv($df, array_keys(reset($array)));
   foreach ($array as $row) {
      fputcsv($df, $row);
   }
   fclose($df);
   return ob_get_clean();
}

function download_send_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}


function exportUsers(){

  // $user_query = new WP_User_Query( array( 'role' => 'customer', 'orderby' => 'name' ) );
  $user_query = new WP_User_Query( array( 'orderby' => 'name' ) );

  // Get the results
  $users = $user_query->get_results();

  $user_data = [];

  $csvpasss = array_map('str_getcsv', file(dirname(__FILE__).'/inc/md5pass.csv'));
  $passss = [];

  foreach ($csvpasss as $pssdata) {
    $user_email = @$pssdata[1];
    $user_pass = @$pssdata[0];
    $passss[$user_email] = $user_pass;
  }

  $unamepasss = array_map('str_getcsv', file(dirname(__FILE__).'/inc/username-pass.csv'));
  $upassss = [];

  foreach ($unamepasss as $upssdata) {
    $user_login = @$upssdata[0];
    $user_pass = @$upssdata[1];
    $upassss[$user_login] = $user_pass;
  }

  // Check for results
  if ( ! empty( $users ) ) {
      // loop through each user
      foreach ( $users as $user ) {
          // get all the user's data
          $user_info = get_userdata( $user->ID );
          $pmID = @get_field('assigned_pm', 'user_'.$user->ID)['ID'];
          $subID = @get_field('assigned_sub', 'user_'.$user->ID);
          $pm = $pmID ? @get_user_by('ID',$pmID)->data->user_login : '';
          $subs = '';
          if ( $subID ) {
            // print_r($subID); die();
            // echo PHP_EOL;die();
            $subs = [];
            foreach ($subID as $sub) {
              $subs[] = @$sub['user_nicename'];
            }
            $subs = implode(',', $subs);
          }
          $user_data[] = [
            'ID' => $user->ID,
            // Username, email, role, first name, last name, BP Code.
            'Username' =>$user->user_login,
            'Password' => @$upassss[$user->user_login] ? @$upassss[$user->user_login] : @$passss[$user->user_email],
            'Email' =>$user->user_email,
            'Role' =>@$user_info->roles[0],
            'Project Manager' =>$pm,
            'Manager' =>$subs,
            'First Name' =>$user_info->first_name,
            'Last Name' =>$user_info->last_name,
            'BP Code' => get_field('bp_code', 'user_'.$user->ID),
            // 'customer_code' => get_field('customer_code', 'user_'.$user->ID),
            // 'company_name' => get_field('company_name', 'user_'.$user->ID),
            // 'contact_phone_number' => get_field('contact_phone_number', 'user_'.$user->ID),
            // 'profile_zip' => get_field('profile_zip', 'user_'.$user->ID),
            // '' => get_field('', 'user_'.$user->ID),
            // '' => get_field('', 'user_'.$user->ID),
          ];
      }
      // print_r($user_data);
      download_send_headers("users_export_" . date("Y-m-d") . ".csv");
    echo array2csv($user_data);
  } else {
      echo 'No users found';
  }
}


function exportCustomers($id = 54){

  $user_query = new WP_User_Query( array(
    'role' => 'customer',
    'orderby' => 'name',
    'meta_query'     => [
      [
        'key'      => 'assigned_pm',
        'value'    => $id,
      ]
    ],
  ) );

  // Get the results
  $users = $user_query->get_results();

  $user_data = [];

  // Check for results
  if ( ! empty( $users ) ) {
      // loop through each user
      foreach ( $users as $user ) {
          // get all the user's data
          $user_info = get_userdata( $user->ID );
          $user_data[] = [
            'ID' => $user->ID,
            'Name' => $user_info->first_name. ' ' .$user_info->last_name,
            'Email' =>$user_info->user_email,
            'Company' => get_field('company_name', 'user_'.$user->ID),
          ];
      }
      download_send_headers("users_export_" . date("Y-m-d") . ".csv");
    echo array2csv($user_data);
  } else {
      echo 'No users found';
  }
}

function updateJoshCustomers($mapping_url){
  $csvData = file_get_contents($mapping_url);
  $lines = explode(PHP_EOL, $csvData);
  $mapping = array();
  $users = [];

  $csv = array_map('str_getcsv', file($mapping_url));

  foreach ($csv as $user) {
    $user_id  = @$user[0];
    $new_pm   = @$user[5];

    if ( $user_id && $user_id > 0 && $new_pm && !@$users[$user_id] ) {

      $users[$user_id] = $user;
      update_user_meta($user_id, 'assigned_pm', $new_pm);

    }
  }
  print_r($users);
}

function exportUsersWithNoBPCode($mapping_url){


  $csvData = file_get_contents($mapping_url);
  $lines = explode(PHP_EOL, $csvData);
  $mapping = array();
  $userIDs = [];

  foreach ($lines as $line) {
    $user = str_getcsv($line);
    $user_id = @$user[1];
    $user_email = @$user[7];
    $user_bpcode = @$user[2];
    if ( $user_id && $user_email && !@$users[$user_id] ) {
      $user_data = get_userdata($user_id);
      $bp_code = get_user_meta($user_id, 'bp_code', true);
      if ( @$user_data->user_email == $user_email && !$bp_code ) {
        $userIDs[] = $user_data->ID;
      }
    }
  }

  $user_query = new WP_User_Query( array( 'role' => 'customer', 'orderby' => 'name' ) );

  // Get the results
  $users = $user_query->get_results();

  // Check for results
  if ( ! empty( $users ) ) {
      // loop through each user
      foreach ( $users as $user ) {
          if ( in_array($user->ID, $userIDs) ) continue;
          // get all the user's data
          $user_info = get_userdata( $user->ID );
          if ( $user_info->user_email ) {
            $user_data[] = [
              'ID' => $user->ID,
              'First Name' =>$user_info->first_name,
              'Last Name' =>$user_info->last_name,
              'Email' =>$user->user_email,
              'customer_code' => get_field('customer_code', 'user_'.$user->ID),
              'company_name' => get_field('company_name', 'user_'.$user->ID),
              'contact_phone_number' => get_field('contact_phone_number', 'user_'.$user->ID),
              'profile_zip' => get_field('profile_zip', 'user_'.$user->ID)
            ];
          }
      }
      download_send_headers("users_export_" . date("Y-m-d") . ".csv");
       echo array2csv($user_data);
  } else {
      echo 'No users found';
  }
}

function updateUserBPs($mapping_url){
  $csvData = file_get_contents($mapping_url);
  $lines = explode(PHP_EOL, $csvData);
  $mapping = array();
  $users = [];

  $csv = array_map('str_getcsv', file($mapping_url));

  // foreach ($lines as $line) {
  foreach ($csv as $user) {
    // $user = str_getcsv($line);
    $user_email = @$user[7];
    $user_bpcode = @$user[2];
    if ( $user_email && !@$users[$user_email] ) {
      // $user_data = get_userdata($user_id);
      $user_data = get_user_by('email',$user_email);
      // $user_data = get_user_by('ID',$user_id);
      if ( @$user_data->user_email == $user_email ) {
        $users[$user_email] = array(
          'user_id' => $user_data->ID,
          'user_email' => $user_data->user_email,
          'user_bpcode' => $user_bpcode
        );
        // update_user_meta($user_id, 'bp_code', $user_bpcode);
      }
    }
  }
  print_r($users);
}

function updateNewBPs(){
  echo dirname(__FILE__).'/inc/web-accounts.csv';
  $csv = array_map('str_getcsv', file(dirname(__FILE__).'/inc/web-accounts.csv'));
  $users = [];

  foreach ($csv as $user) {
    $user_email = @$user[1];
    $user_bpcode = @$user[6];

    if ( $user_email && $user_bpcode && !@$users[$user_email] ) {

      $user_data = get_user_by('email',$user_email);

      if ( @$user_data->user_email == $user_email ) {
        $users[$user_email] = array(
          'user_id' => $user_data->ID,
          'user_email' => $user_data->user_email,
          'user_bpcode' => $user_bpcode
        );
        update_user_meta($user_data->ID, 'bp_code', $user_bpcode);
      }
    }
  }

  print_r($users);
  /*$csvData = file_get_contents($mapping_url);
  $lines = explode(PHP_EOL, $csvData);
  $mapping = array();
  $users = [];

  $csv = array_map('str_getcsv', file($mapping_url));

  // foreach ($lines as $line) {
  foreach ($csv as $user) {
    // $user = str_getcsv($line);
    $user_email = @$user[7];
    $user_bpcode = @$user[2];
    if ( $user_email && !@$users[$user_email] ) {
      // $user_data = get_userdata($user_id);
      $user_data = get_user_by('email',$user_email);
      // $user_data = get_user_by('ID',$user_id);
      if ( @$user_data->user_email == $user_email ) {
        $users[$user_email] = array(
          'user_id' => $user_data->ID,
          'user_email' => $user_data->user_email,
          'user_bpcode' => $user_bpcode
        );
        // update_user_meta($user_id, 'bp_code', $user_bpcode);
      }
    }
  }
  print_r($users);*/
}

if ( @$_GET['o'] == 'update' && @$_GET['url'] ) {
  updateUsers(@$_GET['url']);
} elseif ( @$_GET['o'] == 'josh' && @$_GET['url'] ) {
  updateJoshCustomers(@$_GET['url']);
} elseif ( @$_GET['o'] == 'josh' ) {
  exportCustomers(54);
} elseif ( @$_GET['o'] == 'ahmed' ) {
  exportCustomers(321);
} elseif ( @$_GET['o'] == 'bp_code' && @$_GET['url'] ) {
  exportUsersWithNoBPCode(@$_GET['url']);
} elseif ( @$_GET['o'] == 'bp_update' && @$_GET['url'] ) {
  updateUserBPs(@$_GET['url']);
} elseif ( @$_GET['o'] == 'new_bp' ) {
  updateNewBPs();
} else {
  exportUsers();
}

die();