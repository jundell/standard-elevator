<?php
add_filter( 'send_password_change_email', '__return_false' );
function remove_loop_button(){
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
    // add_filter( 'woocommerce_is_purchasable', false );
}
add_action('init','remove_loop_button');
add_action('woocommerce_after_shop_loop_item','replace_add_to_cart');
function replace_add_to_cart() {
    global $product;
    $link = $product->get_permalink();
    echo do_shortcode('<a href="'.$link.'" class="button view-details">View Details</a>');
}

// Simple, grouped and external products
add_filter('woocommerce_product_get_price', 'tdi_custom_price', 99, 2 );
add_filter('woocommerce_product_get_regular_price', 'tdi_custom_price', 99, 2 );
add_filter('woocommerce_get_price', 'tdi_custom_price', 99, 2 );
add_filter('woocommerce_get_regular_price', 'tdi_custom_price', 99, 2 );
// Variations
add_filter('woocommerce_product_variation_get_regular_price', 'tdi_custom_price', 99, 2 );
add_filter('woocommerce_product_variation_get_price', 'tdi_custom_price', 99, 2 );
function tdi_custom_price( $price, $product ) {
    global $wpdb;
    $user_id = get_current_user_id();
    $key = 'price_level';
    $single = true;
    $price_level = get_user_meta( $user_id, $key, $single );
    $name = @$product->get_name();
    $new_price = $wpdb->get_row("SELECT * FROM wp_pricing_table WHERE machines = '$name'") ;
    if ( $new_price ) {
        /*if($price_level == 'b4' || $price_level=='B4 Price') {
            $price = $new_price->b4_price;
        }
        else */if($price_level=='otis' || $price_level =='Otis' || $price_level == 'Otis Price') {
            $price = $new_price->otis_price;
        }
        else {
            $price = $new_price->general_price;
        }
    }
    return $price;
}

add_filter( 'gettext', 'woocommerce_rename_coupon_field_on_cart', 10, 3 );
add_filter( 'gettext', 'woocommerce_rename_coupon_field_on_cart', 10, 3 );
add_filter('woocommerce_coupon_error', 'rename_coupon_label', 10, 3);
add_filter('woocommerce_coupon_message', 'rename_coupon_label', 10, 3);
add_filter('woocommerce_cart_totals_coupon_label', 'rename_coupon_label',10, 1);
add_filter( 'woocommerce_checkout_coupon_message', 'woocommerce_rename_coupon_message_on_checkout' );


function woocommerce_rename_coupon_field_on_cart( $translated_text, $text, $text_domain ) {
    // bail if not modifying frontend woocommerce text
    if ( is_admin() || 'woocommerce' !== $text_domain ) {
        return $translated_text;
    }
    if ( 'Coupon:' === $text ) {
        $translated_text = 'Service Code:';
    }

    if ('Coupon has been removed.' === $text){
        $translated_text = 'Service code has been removed.';
    }

    if ( 'Apply coupon' === $text ) {
        $translated_text = 'Service Code';
    }

    if ( 'Coupon code' === $text ) {
        $translated_text = 'Service Code';
    
    } 

    return $translated_text;
}


// rename the "Have a Coupon?" message on the checkout page
function woocommerce_rename_coupon_message_on_checkout() {
    return 'Have an Service Code?' . ' ' . __( 'Click here to enter your code', 'woocommerce' ) . '';
}


function rename_coupon_label($err, $err_code=null, $something=null){

    $err = str_ireplace("Coupon","Service Code ",$err);

    return $err;
}

add_filter( 'woocommerce_product_get_stock_quantity' ,'custom_get_stock_quantity', 10, 2 );
add_filter( 'woocommerce_product_variation_get_stock_quantity' ,'custom_get_stock_quantity', 10, 2 );
function custom_get_stock_quantity( $value, $product ) {
    $qty = doSQL('select "ItemCode","OnHand"-"IsCommited" as "IsAvailable" from  "OITW" WHERE "WhsCode" = \'RAINES\' AND "ItemCode" = \''.@$product->name.'\'',
        false,
        'json'
    );
    if ( $qty && $qty[0] ) {
        $value = $qty[0]->IsAvailable;
        $value = $value > 0 ? $value : 0;
    }
    return $value;
}

function check_backorder() {
	global $product;


	$response = ['back_order' => false ] ;
	foreach ( WC()->cart->get_cart() as $cart_item ) {
        $_product =  wc_get_product( $cart_item['data']->get_id());

        $qty = doSQL('select "ItemCode","OnHand"-"IsCommited" as "IsAvailable" from  "OITW" WHERE "WhsCode" = \'RAINES\' AND "ItemCode" = \''.trim(@$_product->get_title()).'\'',
            false,
            'json'
        );
        $value = (int)@$qty[0]->IsAvailable;
        $q = (int)@$cart_item['quantity'];
        if( $q > $value ) {
            $response = ['back_order' => true ] ;
            break;
        } else {
            $response = ['back_order' => false ] ;
        }
	}
   	echo json_encode($response); //returning this value but still shows 0
   	die();

}
add_action( 'wp_ajax_check_backorder', 'check_backorder', 20);
add_action( 'wp_ajax_nopriv_check_backorder', 'check_backorder', 20);


function get_all_user_meta() {
    $all_meta_for_user = get_user_meta(get_current_user_id() );
    $all_meta_for_user['cc_only'] = isCCOnly(@$all_meta_for_user['bp_code'][0]);
    echo json_encode($all_meta_for_user); //returning this value but still shows 0
    die();

}
add_action( 'wp_ajax_get_all_user_meta', 'get_all_user_meta' );
add_action( 'wp_ajax_nopriv_get_all_user_meta', 'get_all_user_meta' );

function isCCOnly($bpcode) {
    $sapdata = doSQL(
        'SELECT "CardCode", "GroupNum" FROM "OCRD" where "CardCode" = \''.$bpcode.'\'',
        false,
        'json'
    );
    return @$sapdata[0]->GroupNum == '14' || !@$sapdata[0]->GroupNum ? 1 : 0;
}


function get_stock() {

  $qty = doSQL('select "ItemCode","OnHand"-"IsCommited" as "IsAvailable" from  "OITW" WHERE "WhsCode" = \'RAINES\' AND "ItemCode" = \''.@ $_POST['product_name'].'\'',
      false,
      'json'
  );
   	echo json_encode($qty[0]->IsAvailable); //returning this value but still shows 0
   	die();

}
add_action( 'wp_ajax_get_stock', 'get_stock' );
add_action( 'wp_ajax_nopriv_get_stock', 'get_stock' );


function tdi_login_hook($user_login, $user) {
	bpSync($user->ID);
}
add_action('wp_login', 'tdi_login_hook', 10, 2);

function bpSync($id){
    $user_id = $id;
    $bpcode  = get_user_meta($user_id, 'bp_code', true);
    $sapdata = getByBPCode($bpcode);
    $shipping = getAddress($bpcode,'S');
    $billing = getAddress($bpcode,'B');
    $info = getInfo($bpcode);
    $cc_only = 0;

    if ( $sapdata ) {
        $price_level = get_user_meta($user_id, 'price_level', true);
        
        if ( $price_level != 'no-price' ) {
            $price_level = 'general';
            $cc_only = $sapdata->GroupNum == '14' ? 1 : 0;
            if ( in_array($sapdata->ListNum, [11,12,14]) ) {
                $levels = [
                    11 => 'general',
                    12 => 'otis',
                    14 => 'b4',
                ];
                $price_level = $levels[$sapdata->ListNum];
            }
            // $sapdata->ListNum;
            update_user_meta( $user_id, 'price_level', $price_level );
        }
    }

    update_user_meta( $user_id, 'cc_only', $cc_only );

    if ( $shipping ) metaUpdate($user_id, $shipping);

    if ( $billing ) metaUpdate($user_id, $billing);

    if ( $info ) {
        $info['shipping_info'] = 'exist';
        metaUpdate($user_id, $info);
    }

	update_user_meta( $user->ID, 'sap_data', json_encode($sapdata) );
}

function metaUpdate($user_id, $meta){
    foreach ($meta as $meta_key => $meta_value) {
        update_user_meta( $user_id, $meta_key, $meta_value );
    }
}

function getByBPCode($bpcode) {
    $sapdata = doSQL(
        'SELECT "CardCode", "GroupNum", "ListNum" FROM "TEST5_TDI"."OCRD" where "CardCode" = \''.$bpcode.'\'',
        false,
        'json'
    );
    return @$sapdata[0] ? @$sapdata[0] : 0;
}

function getAddress($bpcode, $type = 'S') {
    $sapdata = doSQL(
        'select "CardCode",o1."Country",o3."Name" "CountryName","State",o2."Name" "StateName","City","Street","ZipCode","Address","County","Building"
        from "CRD1" o1
        left join "TDIPROD"."OCST" o2 on o1."State"=o2."Code"
        left join "TDIPROD"."OCRY" o3 on o1."Country"=o3."Code"
        where "CardCode"=\''.$bpcode.'\'
        and "AdresType"=\''.$type.'\'',
        false,
        'json'
    );
    $address = @$sapdata[0];
    if ( $address ) {
        $prefix = $type == 'S' ? 'shipping_' : 'billing_';
        $address = (array)$address;
        $new_address = '';
        $new_address .= @$address['Building'] ? @$address['Building'].', ' : '';
        $new_address .= @$address['Street'] ? @$address['Street'].', ' : '';
        $new_address .= @$address['Address'] ? @$address['Address'] : '';

        $address[$prefix.'address'] = $new_address;
        $address[$prefix.'city']    = @$address['City'];
        $address[$prefix.'state']   = @$address['StateName'];
        $address[$prefix.'zip']     = @$address['ZipCode'];
        $address[$prefix.'country'] = @$address['CountryName'];

        return $address;
    }
    return 0;
}

function userMeta($id){
    $meta = get_metadata('user', $id);
    $newm = [];
    foreach ($meta as $key => $value) {
        if ( !in_array($key,['session_tokens','wp_capabilities']) ) {
            $newm[$key] = @$value[0];
        }
    }
    return $newm;
}

function getInfo($bpcode) {
    $sapdata = doSQL(
        'select
            "ShipType"      as "shipping_type_id",
            o2."TrnspName"  as "shipping_type",
            "U_SCPhone"     as "shipping_contact_phone",
            "U_SCEmail"     as "shipping_contact_email",
            "U_ShipAcct"    as "customer_shipping_account",
            "U_CCarrier"    as "customer_carrier",
            "U_SCName"      as "shipping_contact_name"
        from "OCRD" o1
        left join "OSHP" o2 on o1."ShipType"=o2."TrnspCode"
        where "CardCode"=\''.$bpcode.'\'',
        false
    );

    $info = @$sapdata[0];
    return $info ? (array)$info : 0;
}


function doSQL($sql, $display = true, $type = 'json') {
    $server = 'http://3.222.225.217/';
    $db_name = 'sapjdbc-TEST5_TDI';

    if ( $_SERVER['SERVER_NAME'] == 'torindriveintl.com' ) {
        $db_name = 'sapjdbc';
        $server = 'http://172.31.25.27/';
    } elseif ( $_SERVER['SERVER_NAME'] == 'staging.torindriveintl.com' ) {
        $server = 'http://172.31.25.27/';
    }

    $url = $server.$db_name.'/sapjdbc.'.$type;

    //The data you want to send via POST
    $fields = [ 'sql' => $sql ];

    //url-ify the data for the POST
    $fields_string = http_build_query($fields);

    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

    //So that curl_exec returns the contents of the cURL; rather than echoing it
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

    //execute post
    $result = curl_exec($ch);
    if ( $display ) {
        return $result ? json_decode($result) : ['status'=>'fail', 'url' => $url, 'sql' => $sql];
    } else {
        return json_decode($result);
    }
}
